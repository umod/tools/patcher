using Mono.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using uMod.Patcher.Fields;
using uMod.Patcher.Hooks;
using uMod.Patcher.Modifiers;

namespace uMod.Patcher
{
    internal class Program
    {
        // Defines for command-line output
        [DllImport("kernel32.dll")]
        private static extern bool AttachConsole(int dwProcessId);

        private const int AttachParentProcess = -1;

        /// <summary>
        /// The main entry point for the application
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            AppDomain.CurrentDomain.AssemblyResolve += (_, args1) =>
            {
                string resourceName = $"uMod.Patcher.Dependencies.{new AssemblyName(args1.Name).Name}.dll";
                if (resourceName.Contains("resources.dll"))
                {
                    return null;
                }

                using Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
                if (stream != null)
                {
                    byte[] assemblyData = new byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }

                return null;
            };

            List<string> paths = new List<string>();
            bool unflagAll = false;
            string targetOverride = string.Empty;
            string error = string.Empty;
            string platform = "both";
            string excludeFile = string.Empty;
            HookOperatingSystem hookPlatform = HookOperatingSystem.All;

            OptionSet optionSet = new()
            {
                { "<>", "the .upj or .opj file or directory", value => { paths.Add(value); } },
                { "u|unflag", "whether to unflag all hooks", _ => { unflagAll = true; } },
                {
                    "d|dir=",
                    "the target directory",
                    value =>
                    {
                        if (!Directory.Exists(value))
                        {
                            throw new OptionException($"Directory ({value}) not found", "dir");
                        }
                        targetOverride = value;
                    }
                },
                { "p|platform=", "the platform to patch for", value => { platform = value; } },
                { "e|exclude=", "patch exclusion file path", value => { excludeFile = value; } },
            };

            try
            {
                optionSet.Parse(args);
            }
            catch (OptionException oEx)
            {
                Console.WriteLine($"{oEx.OptionName}: {oEx.Message}");
            }

            hookPlatform = platform.ToLower() switch
            {
                "both" => HookOperatingSystem.All,
                "windows" => HookOperatingSystem.Windows,
                "linux" => HookOperatingSystem.Linux,
                _ => hookPlatform
            };

            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    // Redirect console output to parent process; must be before any calls to Console.WriteLine()
                    AttachConsole(AttachParentProcess);
                    break;
            }

            if (error != "")
            {
                Console.WriteLine($"ERROR: {error}");
                return;
            }

            if (!Directory.Exists(targetOverride) && targetOverride != "")
            {
                Console.WriteLine($"{targetOverride} does not exist!");
                return;
            }

            foreach (string path in paths)
            {
                if (!File.Exists(path) && !Directory.Exists(path))
                {
                    Console.WriteLine($"{path} does not exist!");
                    return;
                }
            }

            Project patchProject = Project.Load(paths, targetOverride);
            Project.Current = patchProject;

            if (string.IsNullOrEmpty(patchProject.TargetDirectory))
            {
                patchProject.TargetDirectory = Environment.CurrentDirectory;
            }

            if (unflagAll)
            {
                UnflagPaths(paths, patchProject);
            }

            patchProject.Platform = hookPlatform;


            List<string> patchExclusions = null;
            if (!string.IsNullOrEmpty(excludeFile))
            {
                if (!File.Exists(excludeFile))
                {
                    Console.WriteLine($"Unable to find file: {excludeFile}");
                    Environment.Exit(1);
                    return;
                }
                string exclusionFileContents = File.ReadAllText(excludeFile);
                exclusionFileContents = exclusionFileContents.Replace("\r", string.Empty);
                patchExclusions = exclusionFileContents.Split("\n", StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            Patching.Patcher patcher = new(patchProject, true, delegate (string header, string message)
            {
                Console.WriteLine($"{header}: {message}");
            });

            patcher.Patch();
        }

        private static void UnflagPaths(IEnumerable<string> paths, Project patchProject)
        {
            foreach (string path in paths)
            {
                string extension = Path.GetExtension(path);
                if (extension == ".upj")
                {
                    string contents = File.ReadAllText(path);
                    string[] parts = contents.Replace("\r",string.Empty).Split("\n");
                    UnflagPaths(parts, patchProject);
                }
                else
                {
                    if (Unflag(patchProject, path, true))
                    {
                        patchProject.Save(path);
                    }
                }
            }
        }

        private static bool Unflag(Project project, string filename, bool console)
        {
            bool updated = false;
            foreach (Hook hook in project.Manifests.SelectMany(m => m.Hooks))
            {
                if (hook.Flagged)
                {
                    hook.Flagged = false;
                    updated = true;
                    if (console)
                    {
                        Console.WriteLine($"Hook {hook.HookName} has been unflagged");
                    }
                }
            }

            foreach (Modifier modifier in project.Manifests.SelectMany(m => m.Modifiers))
            {
                if (modifier.Flagged)
                {
                    modifier.Flagged = false;
                    updated = true;
                    if (console)
                    {
                        Console.WriteLine($"Modifier {modifier.Name} has been unflagged");
                    }
                }
            }

            foreach (Field field in project.Manifests.SelectMany(m => m.Fields))
            {
                if (field.Flagged)
                {
                    field.Flagged = false;
                    updated = true;
                    if (console)
                    {
                        Console.WriteLine($"Field {field.Name} has been unflagged");
                    }
                }
            }

            return updated;
        }
    }
}
