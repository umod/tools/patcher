﻿using Mono.Cecil;
using Mono.Cecil.Cil;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using uMod.Patcher.Exceptions;
using uMod.Patcher.Patching;

namespace uMod.Patcher.Hooks
{
    public enum CaptureGroupPosition { Before = 0, After = 1, Offset = 2 }

    public class CaptureGroup
    {
        public string Name { get; set; }

        public string[] Patterns { get; set; }
    }

    [HookType("Pattern")]
    public class Pattern : Simple
    {
        /// <summary>
        /// Not output to opj file
        /// </summary>
        [JsonIgnore]
        public override InjectionType InjectionType { get; set; } = InjectionType.Pattern;

        /// <summary>
        /// Not used for pattern hook
        /// </summary>
        [JsonIgnore]
        public override int InjectionIndex { get; set; }

        /// <summary>
        /// Gets or sets whether or not the injection is before or after capture group
        /// </summary>
        public CaptureGroupPosition CaptureGroupPosition { get; set; }

        /// <summary>
        /// Gets or sets the capture group name
        /// </summary>
        public string CaptureGroupName { get; set; }

        /// <summary>
        /// Gets or sets the capture groups
        /// </summary>
        public CaptureGroup[] CaptureGroups { get; set; }

        /// <summary>
        /// Gets or sets the instruction offset to inject the hook call at
        /// </summary>
        [DefaultValue(-1)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public int OffsetIndex { get; set; } = -1;

        private static readonly char[] SplitChar = { ':' };

        private static KeyValuePair<int, int>? MatchIlPatterns(IReadOnlyList<string> patterns, IlWeaver weaver, int startIndex = 0)
        {
            for (int instructionIndex = startIndex; instructionIndex < weaver.Instructions.Count; instructionIndex++)
            {
                if (instructionIndex + patterns.Count > weaver.Instructions.Count)
                {
                    return null;
                }

                bool found = true;
                for (int patternIndex = 0; patternIndex < patterns.Count; patternIndex++)
                {
                    Instruction instruction = weaver.Instructions[instructionIndex + patternIndex];
                    string pattern = patterns[patternIndex];
                    string instructionStr;
                    if (instruction.OpCode.FlowControl is FlowControl.Next or FlowControl.Call or FlowControl.Return)
                    {
                        int instructionOffsetLength = $"IL_{instruction.Offset:X4} :".Length;
                        instructionStr = instruction.ToString()[instructionOffsetLength..];
                    }
                    else
                    {
                        instructionStr = instruction.OpCode.Name;
                    }

                    if (!instructionStr.Equals(pattern))
                    {
                        found = false;
                        break;
                    }
                }

                if (found)
                {
                    return new KeyValuePair<int, int>(instructionIndex, instructionIndex + patterns.Count - 1);
                }
            }

            return null;
        }

        private int FindInjectionIndex(IlWeaver weaver)
        {
            if (CaptureGroups == null)
            {
                return 0;
            }

            Dictionary<string, KeyValuePair<int, int>> captureMatches = new();

            int startIndex = 0;

            foreach (CaptureGroup captureGroup in CaptureGroups)
            {
                KeyValuePair<int, int>? nullablePositions = MatchIlPatterns(captureGroup.Patterns, weaver, startIndex);
                if (nullablePositions == null)
                {
                    return -1;
                }

                KeyValuePair<int, int> positions = (KeyValuePair<int, int>)nullablePositions;
                captureMatches.Add(captureGroup.Name, positions);
                startIndex = positions.Value;
            }

            if (string.IsNullOrEmpty(CaptureGroupName))
            {
                throw new PatternInvalidException("Invalid Capture Group Name", "Capture group name not specified");
            }

            if (!captureMatches.ContainsKey(CaptureGroupName))
            {
                throw new PatternInvalidException("Invalid Capture Group Name", "Capture group name not found in capture group list");
            }

            KeyValuePair<int, int> matchedPosition = captureMatches[CaptureGroupName];

            switch (CaptureGroupPosition)
            {
                case CaptureGroupPosition.Before:
                    return matchedPosition.Key;

                case CaptureGroupPosition.After:
                    return matchedPosition.Value + 1;
            }

            if (OffsetIndex < 0)
            {
                return matchedPosition.Key - OffsetIndex;
            }
            if (OffsetIndex > 0)
            {
                return matchedPosition.Value + 1 + OffsetIndex;
            }

            return matchedPosition.Key;
        }

        /// <summary>
        /// Patch hook using the specified method into the target weaver
        /// </summary>
        /// <param name="original"></param>
        /// <param name="weaver"></param>
        /// <param name="moduleAssembly"></param>
        /// <param name="patcher"></param>
        /// <returns></returns>
        public override bool ApplyPatch(MethodDefinition original, IlWeaver weaver, AssemblyDefinition moduleAssembly, Patching.Patcher patcher = null)
        {
            InjectionIndex = FindInjectionIndex(weaver);

            return base.ApplyPatch(original, weaver, moduleAssembly, patcher);
        }
    }
}
