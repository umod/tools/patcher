﻿using Mono.Cecil;
using Mono.Cecil.Cil;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using uMod.Patcher.Patching;
using AssemblyDefinition = Mono.Cecil.AssemblyDefinition;
using TypeDefinition = Mono.Cecil.TypeDefinition;

namespace uMod.Patcher.Hooks
{
    public enum ReturnBehavior { Continue, ExitWhenValidType, ModifyRefArg, UseArgumentString, ExitWhenNonNull }

    public enum ArgumentBehavior { None, JustThis, JustParams, All, UseArgumentString }

    public enum InjectionType { Index = 0, Pre = 1, Post = 2, Pattern = 3 }

    /// <summary>
    /// A simple hook that injects at a certain point in the method, with a few options for handling arguments and return values
    /// </summary>
    [HookType("Simple", Default = true)]
    public class Simple : Hook
    {
        public class DeprecatedStatus
        {
            public string ReplacementHook { get; set; }
            public DateTime RemovalDate { get; set; }
        }

        /// <summary>
        /// Gets or sets the type of injection performed
        /// </summary>
        [DefaultValue(InjectionType.Index)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public virtual InjectionType InjectionType { get; set; } = InjectionType.Index;

        /// <summary>
        /// Gets or sets the instruction index to inject the hook call at
        /// </summary>
        public virtual int InjectionIndex { get; set; }

        /// <summary>
        /// Gets or sets the return behavior
        /// </summary>
        public ReturnBehavior ReturnBehavior { get; set; }

        /// <summary>
        /// Gets or sets the argument behavior
        /// </summary>
        public ArgumentBehavior ArgumentBehavior { get; set; }

        /// <summary>
        /// Gets or sets the argument string
        /// </summary>
        public string ArgumentString { get; set; }

        public DeprecatedStatus Deprecation { get; set; }

        [JsonIgnore]
        public bool IsDeprecated { get => Deprecation != null; }

        /// <summary>
        /// Find last valid position for "postfix" hooks
        /// </summary>
        /// <param name="weaver"></param>
        /// <returns></returns>
        public int FindPostPosition(IlWeaver weaver)
        {
            int lastReturn = -1;
            for (int i = 0; i < weaver.Instructions.Count; i++)
            {
                if (weaver.Instructions[i].OpCode == OpCodes.Ret)
                {
                    lastReturn = i;
                }
            }

            if (lastReturn == -1)
            {
                return weaver.Instructions.Count + 1;
            }

            return lastReturn;
        }

        private List<MethodDefinition> callhookMethods;
        private List<MethodDefinition> depcallhookMethods;

        private List<MethodDefinition> GetCallhookMethods(AssemblyDefinition moduleAssembly)
        {
            if (IsDeprecated)
            {
                if (depcallhookMethods != null)
                {
                    return depcallhookMethods;
                }

                // Get the call hook method (only grab object parameters: ignore the object[] hook)
                return depcallhookMethods = moduleAssembly.MainModule.Types
                    .Single(t => t.FullName == "Oxide.Core.Interface" || t.FullName == "uMod.Interface")
                    .Methods.Where(m => m.IsStatic && m.Name == "CallDeprecatedHook" && m.HasParameters && m.Parameters.Any(p => p.ParameterType.IsArray) == false)
                    .OrderBy(x => x.Parameters.Count)
                    .ToList();
            }
            else
            {
                if (callhookMethods != null)
                {
                    return callhookMethods;
                }

                // Get the call hook method (only grab object parameters: ignore the object[] hook)
                return callhookMethods = moduleAssembly.MainModule.Types
                    .Single(t => t.FullName == "Oxide.Core.Interface" || t.FullName == "uMod.Interface")
                    .Methods.Where(m => m.IsStatic && m.Name == "CallHook" && m.HasParameters && m.Parameters.Any(p => p.ParameterType.IsArray) == false)
                    .OrderBy(x => x.Parameters.Count)
                    .ToList();
            }
        }

        /// <summary>
        /// Patch hook using the specified method into the target weaver
        /// </summary>
        /// <param name="original"></param>
        /// <param name="weaver"></param>
        /// <param name="moduleAssembly"></param>
        /// <param name="patcher"></param>
        /// <returns></returns>
        public override bool ApplyPatch(MethodDefinition original, IlWeaver weaver, AssemblyDefinition moduleAssembly, Patching.Patcher patcher = null)
        {
            List<MethodDefinition> callhookmethods = GetCallhookMethods(moduleAssembly);

            int injectionIndex = -1;
            switch (InjectionType)
            {
                case InjectionType.Pre:
                    injectionIndex = 0;
                    break;

                case InjectionType.Post:
                    injectionIndex = FindPostPosition(weaver);
                    break;

                case InjectionType.Pattern:
                case InjectionType.Index:
                    injectionIndex = InjectionIndex;
                    break;
            }

            // Start injecting where requested
            weaver.Pointer = injectionIndex;
            weaver.OriginalPointer = injectionIndex;

            // Get the existing instruction to inject behind
            Instruction existing;
            try
            {
                existing = weaver.Instructions[weaver.Pointer];
            }
            catch (ArgumentOutOfRangeException)
            {
                ShowMsg($"The injection index specified for {Name} is invalid", "Invalid Index", patcher);
                return false;
            }

            // Introduce new locals from stack before injecting anything
            if (ArgumentBehavior == ArgumentBehavior.UseArgumentString)
                IntroduceLocals(original, weaver, patcher);

            VariableDefinition hookExpireDate = null;
            Instruction hookExpireDateAssignment = null;
            if (IsDeprecated)
            {
                hookExpireDate = weaver.AddVariable(original.Module.ImportReference(Deprecation.RemovalDate.GetType())); // hookExpireDate

                hookExpireDateAssignment = weaver.Add(Instruction.Create(OpCodes.Ldloca_S, hookExpireDate));
                weaver.Add(Instruction.Create(OpCodes.Ldc_I4, Deprecation.RemovalDate.Year));
                weaver.Add(Instruction.Create(OpCodes.Ldc_I4_S, (sbyte)Deprecation.RemovalDate.Month));
                weaver.Add(Instruction.Create(OpCodes.Ldc_I4_S, (sbyte)Deprecation.RemovalDate.Day));
                weaver.Add(Instruction.Create(OpCodes.Call, original.Module.ImportReference(typeof(DateTime).GetConstructor(new[] { typeof(int), typeof(int), typeof(int) }))));
            }

            // Load the hook name
            Instruction hookname = weaver.Add(Instruction.Create(OpCodes.Ldstr, HookName));

            // Push the arguments array to the stack and make the call
            //VariableDefinition argsvar; //This is the object array
            if (IsDeprecated)
            {
                weaver.Add(Instruction.Create(OpCodes.Ldstr, Deprecation.ReplacementHook));
                weaver.Add(Instruction.Create(OpCodes.Ldloc, hookExpireDate));
            }

            // Create an object array and load all arguments into it
            Instruction firstinjected = hookExpireDateAssignment ?? hookname;
            PushArgsArray(original, weaver, out int argCount, patcher);

            // Call the CallHook or CallDeprecatedHook method with the correct amount of arguments
            weaver.Add(Instruction.Create(OpCodes.Call, original.Module.ImportReference(callhookmethods[argCount])));

            // Deal with the return value
            //WeaveReturnValue(original, null, weaver);

            // Find all instructions which pointed to the existing and redirect them
            for (int i = 0; i < weaver.Instructions.Count; i++)
            {
                Instruction ins = weaver.Instructions[i];
                if (ins.Operand != null && ins.Operand.Equals(existing))
                {
                    // Check if the instruction lies within our injection range
                    // If it does, it is an instruction we just injected so we don't want to edit it
                    if (i < injectionIndex || i > weaver.Pointer)
                    {
                        ins.Operand = firstinjected;
                    }
                }
            }
            return true;
        }

        private void IntroduceLocals(MethodDefinition method, IlWeaver weaver, Patching.Patcher patcher)
        {
            string[] s = ParseArgumentString(out _);
            if (s == null)
                return;
            for (int i = 0; i < s.Length; i++)
            {
                string arg = s[i].ToLowerInvariant();
                if (string.IsNullOrEmpty(arg) || arg[0] != 'r')
                    continue;
                if (arg.Contains("."))
                    arg = arg.Split('.')[0];
                if (!int.TryParse(arg.Substring(1), out int index))
                    continue;
                if (index > 0)
                    index--; // A hidden trick. Because IL listing starts from 1
                if (index < 0 || index >= method.Body.Instructions.Count)
                {
                    ShowMsg($"Invalid callsite index {arg} supplied for {HookName}", "Invalid callsite supplied",
                        patcher);
                    continue;
                }
                Instruction ins = weaver.Instructions[index]; //method.Body.Instructions[index];
                TypeReference opVarType = ins.Operand is MethodDefinition mDef ? mDef.ReturnType :
                    ins.Operand is MethodReference mRef ? mRef.ReturnType : null;
                if (opVarType == null)
                {
                    ShowMsg($"Invalid callsite index {arg} supplied for {HookName}\nMethod call not found.",
                        "Invalid callsite supplied", patcher);
                    continue;
                }
                if (opVarType.MetadataType == MetadataType.Void)
                {
                    ShowMsg($"Invalid callsite index {arg} supplied for {HookName}\nReturn type cannot be void.",
                        "Invalid callsite supplied", patcher);
                    continue;
                }
                if (opVarType is GenericParameter gp &&
                    ((MethodReference)ins.Operand).DeclaringType is GenericInstanceType git)
                    opVarType = git.GenericArguments[gp.Position];

                VariableDefinition varDef = weaver.IntroducedLocals.ContainsKey(index)
                    ? weaver.Variables[weaver.IntroducedLocals[index]]
                    : weaver.AddVariable(opVarType);
                int nextIndex = index + (weaver.Pointer - weaver.OriginalPointer);
                weaver.IntroducedLocals[index] = weaver.Variables.Count - 1;
                weaver.AddAfter(nextIndex++, Instruction.Create(OpCodes.Stloc_S, varDef));
                weaver.AddAfter(nextIndex, Instruction.Create(OpCodes.Ldloc_S, varDef));
            }
        }

        private Instruction PushArgsArray(MethodDefinition method, IlWeaver weaver, /*out VariableDefinition argsvar*/ out int argCount, Patching.Patcher patcher)
        {
            argCount = 0;
            // Are we going to use arguments?
            if (ArgumentBehavior == ArgumentBehavior.None)
            {
                // Push null
                //argsvar = null;
                return null;
            }

            // Create array variable
            Instruction firstInstruction = null;
            // Are we using the argument string?
            if (ArgumentBehavior == ArgumentBehavior.UseArgumentString)
            {
                string[] args = ParseArgumentString(out string _);
                if (args == null)
                {
                    // Silently fail, but at least produce valid IL
                    //argsvar = null;
                    return null;
                }

                // Create the array
                /*argsvar = weaver.AddVariable(new ArrayType(method.Module.TypeSystem.Object), "args");
                firstInstruction = weaver.Add(ILWeaver.Ldc_I4_n(args.Length));
                weaver.Add(Instruction.Create(OpCodes.Newarr, method.Module.TypeSystem.Object));
                weaver.Stloc(argsvar);*/

                // Populate it
                for (int i = 0; i < args.Length; i++)
                {
                    argCount++;
                    string arg = args[i].ToLowerInvariant();
                    string[] target = null;
                    if (!string.IsNullOrEmpty(arg) && args[i].Contains("."))
                    {
                        string[] split = args[i].Split('.');
                        arg = split[0];
                        target = split.Skip(1).ToArray();
                    }

                    //weaver.Ldloc(argsvar);
                    //weaver.Add(ILWeaver.Ldc_I4_n(i));
                    if (string.IsNullOrEmpty(arg))
                    {
                        weaver.Add(Instruction.Create(OpCodes.Ldnull));
                    }
                    else if (arg == "this")
                    {
                        if (method.IsStatic)
                        {
                            weaver.Add(Instruction.Create(OpCodes.Ldnull));
                        }
                        else
                        {
                            weaver.Add(IlWeaver.Ldarg(null));
                        }

                        GetMember(weaver, method, method.DeclaringType.Resolve(), target, patcher);
                    }
                    else if (arg[0] == 'p' || arg[0] == 'a')
                    {
                        if (int.TryParse(arg.Substring(1), out int index))
                        {
                            /*if (method.IsStatic)
                                pdef = method.Parameters[index];
                            else
                                pdef = method.Parameters[index + 1];*/
                            if (index < method.Parameters.Count)
                            {
                                ParameterDefinition pdef = method.Parameters[index];

                                weaver.Add(IlWeaver.Ldarg(pdef));

                                if (!GetMember(weaver, method, pdef.ParameterType.Resolve(), target, patcher))
                                {
                                    //TypeReference pdefResolved = pdef.ParameterType.Module.ImportReference(pdef.ParameterType.Resolve());
                                    TypeReference typeRef = pdef.ParameterType is ByReferenceType byRefType
                                        ? byRefType.ElementType
                                        : pdef.ParameterType;
                                    if (pdef.ParameterType.IsByReference)
                                    {
                                        //weaver.Add(Instruction.Create(OpCodes.Ldind_Ref));
                                        weaver.Add(Instruction.Create(OpCodes.Ldobj, typeRef));
                                    }

                                    if (pdef.ParameterType.IsValueType)
                                    {
                                        weaver.Add(Instruction.Create(OpCodes.Box, typeRef));
                                    }
                                }
                            }
                            else
                            {
                                ShowMsg($"Invalid argument `{arg}` supplied for {HookName}", "Invalid argument supplied", patcher);
                            }
                        }
                        else
                        {
                            weaver.Add(Instruction.Create(OpCodes.Ldnull));
                        }
                    }
                    else if (arg[0] == 'l' || arg[0] == 'v')
                    {
                        if (int.TryParse(arg.Substring(1), out int index))
                        {
                            if (index < weaver.Variables.Count)
                            {
                                VariableDefinition vdef = weaver.Variables[index];

                                weaver.Ldloc(vdef);

                                if (!GetMember(weaver, method, vdef.VariableType.Resolve(), target, patcher))
                                {
                                    TypeReference typeRef = vdef.VariableType is ByReferenceType byRefType
                                        ? byRefType.ElementType
                                        : vdef.VariableType;
                                    if (vdef.VariableType.IsByReference)
                                    {
                                        weaver.Add(Instruction.Create(OpCodes.Ldobj, typeRef));
                                    }

                                    if (vdef.VariableType.IsValueType)
                                    {
                                        weaver.Add(Instruction.Create(OpCodes.Box, typeRef));
                                    }
                                }
                            }
                            else
                            {
                                ShowMsg($"Invalid variable `{arg}` supplied for {HookName}", "Invalid variable supplied", patcher);
                            }
                        }
                        else
                        {
                            weaver.Add(Instruction.Create(OpCodes.Ldnull));
                        }
                    }
                    else if (arg[0] == 'r')
                    {
                        if (int.TryParse(arg.Substring(1), out int index)
                            && weaver.IntroducedLocals.ContainsKey(--index)) // A hidden trick. Because IL listing starts from 1.
                        {
                            VariableDefinition vdef = weaver.Variables[weaver.IntroducedLocals[index]];

                            weaver.Ldloc(vdef);

                            if (!GetMember(weaver, method, vdef.VariableType.Resolve(), target, patcher))
                            {
                                TypeReference typeRef = vdef.VariableType is ByReferenceType byRefType
                                    ? byRefType.ElementType
                                    : vdef.VariableType;
                                if (vdef.VariableType.IsByReference)
                                    weaver.Add(Instruction.Create(OpCodes.Ldobj, typeRef));
                                if (vdef.VariableType.IsValueType)
                                    weaver.Add(Instruction.Create(OpCodes.Box, typeRef));
                            }
                        }
                        else
                        {
                            weaver.Add(Instruction.Create(OpCodes.Ldnull));
                        }
                    }
                    else
                    {
                        weaver.Add(Instruction.Create(OpCodes.Ldnull));
                    }

                    //weaver.Add(Instruction.Create(OpCodes.Stelem_Ref));
                }
            }
            else
            {
                // Determine what arguments to transmit
                bool includeargs = ArgumentBehavior == ArgumentBehavior.All || ArgumentBehavior == ArgumentBehavior.JustParams;
                bool includethis = ArgumentBehavior == ArgumentBehavior.All || ArgumentBehavior == ArgumentBehavior.JustThis;
                if (method.IsStatic)
                {
                    includethis = false;
                }

                List<ParameterDefinition> args = new List<ParameterDefinition>();
                if (includeargs)
                {
                    for (int i = 0; i < method.Parameters.Count; i++)
                    {
                        ParameterDefinition arg = method.Parameters[i];
                        if (!arg.IsOut)
                        {
                            args.Add(arg);
                        }
                    }
                }

                //argsvar = weaver.AddVariable(new ArrayType(method.Module.TypeSystem.Object), "args");

                // Load arg count, create array, store
                /*if (includethis)
                    firstInstruction = weaver.Add(ILWeaver.Ldc_I4_n(args.Count + 1));
                else
                    firstInstruction = weaver.Add(ILWeaver.Ldc_I4_n(args.Count));*/
                //weaver.Add(Instruction.Create(OpCodes.Newarr, method.Module.TypeSystem.Object));
                //weaver.Stloc(argsvar);

                // Include this
                if (includethis)
                {
                    //weaver.Ldloc(argsvar);
                    //weaver.Add(ILWeaver.Ldc_I4_n(0));
                    argCount++;
                    weaver.Add(IlWeaver.Ldarg(null));
                    //weaver.Add(Instruction.Create(OpCodes.Stelem_Ref));
                }

                // Loop each argument
                for (int i = 0; i < args.Count; i++)
                {
                    argCount++;
                    // Load array, load index load arg, store in array
                    ParameterDefinition arg = args[i];
                    //weaver.Ldloc(argsvar);
                    /*if (includethis)
                        weaver.Add(ILWeaver.Ldc_I4_n(i + 1));
                    else
                        weaver.Add(ILWeaver.Ldc_I4_n(i));*/
                    weaver.Add(IlWeaver.Ldarg(args[i]));
                    if (arg.ParameterType.IsByReference)
                    {
                        weaver.Add(Instruction.Create(OpCodes.Ldobj, arg.ParameterType));
                        weaver.Add(Instruction.Create(OpCodes.Box, arg.ParameterType));
                    }
                    else if (arg.ParameterType.IsValueType)
                    {
                        weaver.Add(Instruction.Create(OpCodes.Box, arg.ParameterType));
                    }
                    //weaver.Add(Instruction.Create(OpCodes.Stelem_Ref));
                }
            }
            return firstInstruction;
        }

        private void WeaveReturnValue(MethodDefinition method, VariableDefinition argsvar, IlWeaver weaver)
        {
            // What return behavior do we use?
            switch (ReturnBehavior)
            {
                case ReturnBehavior.Continue:
                    // Just discard the return value
                    weaver.Add(Instruction.Create(OpCodes.Pop));
                    break;

                case ReturnBehavior.ExitWhenNonNull:
                case ReturnBehavior.ExitWhenValidType:
                    // Is there a return value or not?
                    if (method.ReturnType.FullName == "System.Void")
                    {
                        // If the hook returned something that was non-null, return
                        Instruction i = weaver.Add(Instruction.Create(OpCodes.Ldnull));
                        weaver.Add(Instruction.Create(OpCodes.Beq_S, i.Next));
                        weaver.Add(Instruction.Create(OpCodes.Ret));
                    }
                    else if (ReturnBehavior == ReturnBehavior.ExitWhenNonNull && method.ReturnType == method.Module.TypeSystem.Boolean)
                    {
                        // Create variable
                        VariableDefinition returnvar = weaver.AddVariable(method.Module.TypeSystem.Object);

                        // Store the return value in it
                        weaver.Stloc(returnvar);
                        Instruction i = weaver.Ldloc(returnvar);

                        // Continue if null
                        weaver.Add(Instruction.Create(OpCodes.Brfalse_S, i.Next));

                        // If it's boolean, return it - else return false
                        weaver.Ldloc(returnvar);
                        weaver.Add(Instruction.Create(OpCodes.Isinst, method.Module.TypeSystem.Boolean));
                        Instruction i2 = Instruction.Create(OpCodes.Ldloc, returnvar);
                        weaver.Add(Instruction.Create(OpCodes.Brtrue_S, i2));
                        weaver.Add(Instruction.Create(OpCodes.Ldc_I4_0));
                        weaver.Add(Instruction.Create(OpCodes.Ret));
                        weaver.Add(i2);
                        weaver.Add(Instruction.Create(OpCodes.Unbox_Any, method.ReturnType));
                        weaver.Add(Instruction.Create(OpCodes.Ret));
                    }
                    else
                    {
                        // Create variable
                        VariableDefinition returnvar = weaver.AddVariable(method.Module.TypeSystem.Object);

                        // Store the return value in it
                        weaver.Stloc(returnvar);
                        weaver.Ldloc(returnvar);

                        // If it is non-null and matches the return type, return it - else continue
                        weaver.Add(Instruction.Create(OpCodes.Isinst, method.ReturnType));
                        Instruction i = weaver.Add(Instruction.Create(OpCodes.Ldnull));
                        weaver.Add(Instruction.Create(OpCodes.Beq_S, i.Next));
                        weaver.Ldloc(returnvar);
                        weaver.Add(Instruction.Create(OpCodes.Unbox_Any, method.ReturnType));
                        weaver.Add(Instruction.Create(OpCodes.Ret));
                    }
                    break;

                case ReturnBehavior.ModifyRefArg:
                    string[] args = ParseArgumentString(out _);
                    if (args == null)
                    {
                        break;
                    }

                    for (int i = 0; i < args.Length; i++)
                    {
                        string arg = args[i].ToLowerInvariant();
                        if (arg[0] == 'p' || arg[0] == 'a')
                        {
                            if (int.TryParse(arg.Substring(1), out int index))
                            {
                                ParameterDefinition pdef = method.Parameters[index];
                                if (pdef.ParameterType.IsValueType)
                                {
                                    //weaver.Ldloc(argsvar);
                                    //weaver.Add(ILWeaver.Ldc_I4_n(i));
                                    weaver.Add(Instruction.Create(OpCodes.Ldelem_Ref));
                                    weaver.Add(Instruction.Create(OpCodes.Unbox_Any, pdef.ParameterType));
                                    weaver.Starg(pdef);
                                }
                            }
                        }
                        else if (arg[0] == 'l' || arg[0] == 'v')
                        {
                            if (int.TryParse(arg.Substring(1), out int index))
                            {
                                VariableDefinition vdef = weaver.Variables[index];
                                if (vdef.VariableType.IsValueType)
                                {
                                    weaver.Ldloc(argsvar);
                                    weaver.Add(IlWeaver.Ldc_I4_n(i));
                                    weaver.Add(Instruction.Create(OpCodes.Ldelem_Ref));
                                    weaver.Add(Instruction.Create(OpCodes.Unbox_Any, vdef.VariableType));
                                    weaver.Stloc(vdef);
                                }
                            }
                        }
                    }
                    weaver.Add(Instruction.Create(OpCodes.Pop));
                    break;

                case ReturnBehavior.UseArgumentString:
                    // Deal with it according to the retvalue of the arg string
                    ParseArgumentString(out string retvalue);
                    if (!string.IsNullOrEmpty(retvalue))
                    {
                        if (retvalue[0] == 'l' && retvalue.Length > 1)
                        {
                            if (int.TryParse(retvalue.Substring(1), out int localindex))
                            {
                                // Create variable and get the target variable
                                VariableDefinition returnvar = weaver.AddVariable(method.Module.TypeSystem.Object);
                                VariableDefinition targetvar = weaver.Variables[localindex];

                                // Store the return value in it
                                weaver.Stloc(returnvar);
                                weaver.Ldloc(returnvar);

                                // If it is non-null and matches the variable type, store it in the target variable
                                weaver.Add(Instruction.Create(OpCodes.Isinst, targetvar.VariableType));
                                Instruction i = weaver.Add(Instruction.Create(OpCodes.Ldnull));
                                weaver.Add(Instruction.Create(OpCodes.Beq_S, i.Next));
                                weaver.Ldloc(returnvar);
                                weaver.Add(Instruction.Create(OpCodes.Unbox_Any, targetvar.VariableType));
                                weaver.Stloc(targetvar);

                                // Handled
                                return;
                            }
                        }
                        else if (retvalue[0] == 'a' && retvalue.Length > 1)
                        {
                            if (int.TryParse(retvalue.Substring(1), out int localindex))
                            {
                                // Create variable and get the target parameter
                                VariableDefinition returnvar = weaver.AddVariable(method.Module.TypeSystem.Object);
                                ParameterDefinition targetvar = method.Parameters[localindex];
                                TypeReference targettype = targetvar.ParameterType is ByReferenceType byReferenceType
                                    ? byReferenceType.ElementType
                                    : targetvar.ParameterType;

                                // Store the return value in it
                                weaver.Stloc(returnvar);
                                weaver.Ldloc(returnvar);

                                // If it is non-null and matches the variable type, store it in the target parameter variable
                                Instruction i = weaver.Add(Instruction.Create(OpCodes.Isinst, targettype));
                                weaver.Add(Instruction.Create(OpCodes.Brfalse_S, i.Next));
                                if (!targetvar.ParameterType.IsValueType)
                                {
                                    weaver.Add(IlWeaver.Ldarg(targetvar));
                                }

                                weaver.Ldloc(returnvar);
                                weaver.Add(Instruction.Create(OpCodes.Unbox_Any, targettype));
                                if (!targetvar.ParameterType.IsValueType)
                                {
                                    weaver.Add(Instruction.Create(OpCodes.Stobj, targettype));
                                }
                                else
                                {
                                    weaver.Starg(targetvar);
                                }

                                // Handled
                                return;
                            }
                        }
                        else if (retvalue == "ret" || retvalue == "return")
                        {
                            // Create variable
                            VariableDefinition returnvar = weaver.AddVariable(method.Module.TypeSystem.Object);

                            // Store the return value in it
                            weaver.Stloc(returnvar);
                            weaver.Ldloc(returnvar);

                            // If it is non-null and matches the return type, return it - else continue
                            weaver.Add(Instruction.Create(OpCodes.Isinst, method.ReturnType));
                            Instruction i = weaver.Add(Instruction.Create(OpCodes.Ldnull));
                            weaver.Add(Instruction.Create(OpCodes.Beq_S, i.Next));
                            weaver.Ldloc(returnvar);
                            weaver.Add(Instruction.Create(OpCodes.Unbox_Any, method.ReturnType));
                            weaver.Add(Instruction.Create(OpCodes.Ret));

                            // Handled
                            return;
                        }
                    }

                    // Not handled
                    weaver.Add(Instruction.Create(OpCodes.Pop));
                    break;
            }
        }

        private string[] ParseArgumentString(out string retvalue)
        {
            // Check arg string for null
            if (string.IsNullOrEmpty(ArgumentString))
            {
                retvalue = null;
                return null;
            }

            // Strip whitespace
            string argstr = new string(ArgumentString.Where(c => !char.IsWhiteSpace(c)).ToArray());

            // Split by return value indicator
            string[] leftright = argstr.Split(new[] { "=>" }, StringSplitOptions.RemoveEmptyEntries);
            if (leftright.Length == 0)
            {
                retvalue = null;
                return null;
            }

            // Split by comma
            string[] args = leftright[0].Split(',');

            // Set the return value
            retvalue = leftright.Length > 1 ? leftright[1] : null;

            // Return
            return args;
        }

        private bool GetMember(IlWeaver weaver, MethodDefinition originalMethod, TypeDefinition currentArg, string[] target, Patching.Patcher patcher)
        {
            if (currentArg == null || target == null || target.Length == 0)
            {
                return false;
            }

            int i;
            TypeDefinition arg = currentArg;
            for (i = 0; i < target.Length; i++)
            {
                if (GetMember(weaver, originalMethod, ref arg, target[i], patcher))
                {
                    continue;
                }

                ShowMsg($"Could not find the member `{target[i]}` in any of the base classes or interfaces of `{currentArg.Name}`.", "Invalid member", patcher);
                return false;
            }

            if (arg.IsValueType || arg.IsByReference)
            {
                weaver.Add(arg.Module == originalMethod.Module
                    ? Instruction.Create(OpCodes.Box, arg.Resolve())
                    : Instruction.Create(OpCodes.Box, originalMethod.Module.ImportReference(arg.Resolve())));
            }

            return i >= 1;
        }

        private bool GetMember(IlWeaver weaver, MethodDefinition originalMethod, ref TypeDefinition currentArg, string target, Patching.Patcher patcher)
        {
            Project project = patcher != null ? patcher.PatchProject : Project.Current;
            string targetDirectory = project.TargetDirectory;
            AssemblyResolver resolver = patcher != null ? patcher.PatchProject.GetAssemblyResolver() : Project.Current.GetAssemblyResolver();
            ReaderParameters readerParams = new ReaderParameters() { AssemblyResolver = resolver };

            if (currentArg == null || string.IsNullOrEmpty(target))
            {
                return false;
            }

            while (currentArg != null)
            {
                if (target.Contains('('))
                {
                    string[] methodname = target.Split('(');
                    string[] args = methodname[1].TrimEnd(')').Split(',').Where(x => !string.IsNullOrEmpty(x)).ToArray();

                    MethodDefinition method = currentArg.Methods.FirstOrDefault(m => m.Name == methodname[0] && m.Parameters.Count == args.Length);
                    if (method != null)
                    {
                        if (method.IsGenericInstance || method.HasGenericParameters || method.Parameters.Count > 0)
                        {
                            return false;
                        }

                        if (method.ReturnType.FullName != "System.Void")
                        {
                            if (method.ReturnType.IsByReference)
                            {
                                if (method.ReturnType.IsValueType)
                                {
                                    weaver.Add(Instruction.Create(OpCodes.Unbox_Any, method.ReturnType));
                                }
                            }
                            else
                            {
                                if (method.ReturnType.IsValueType)
                                {
                                    weaver.Add(Instruction.Create(OpCodes.Box, method.ReturnType));
                                }
                            }
                        }

                        weaver.Add(method.Module == originalMethod.Module
                            ? Instruction.Create(OpCodes.Callvirt, method)
                            : Instruction.Create(OpCodes.Callvirt, originalMethod.Module.ImportReference(method)));

                        currentArg = method.ReturnType.Resolve();

                        return true;
                    }
                }

                if (currentArg.IsClass)
                {
                    if (currentArg.HasFields)
                    {
                        foreach (FieldDefinition field in currentArg.Fields)
                        {
                            if (!string.Equals(field.Name, target, StringComparison.CurrentCultureIgnoreCase))
                            {
                                continue;
                            }

                            weaver.Add(field.Module == originalMethod.Module
                                ? Instruction.Create(OpCodes.Ldfld, field)
                                : Instruction.Create(OpCodes.Ldfld, originalMethod.Module.ImportReference(field)));

                            if (field.FieldType is GenericParameter genericParameter)
                            {
                                GenericParameterConstraint genericTypeConstraint = genericParameter.Constraints.FirstOrDefault(x => !((TypeDefinition)x.ConstraintType).IsInterface);
                                if (genericTypeConstraint != null)
                                {
                                    currentArg = genericTypeConstraint.ConstraintType.Resolve();
                                    return true;
                                }
                            }

                            currentArg = field.FieldType.Resolve();

                            return true;
                        }
                    }
                }

                if (currentArg.HasProperties)
                {
                    foreach (PropertyDefinition property in currentArg.Properties)
                    {
                        if (!string.Equals(property.Name, target, StringComparison.CurrentCultureIgnoreCase))
                        {
                            continue;
                        }

                        weaver.Add(property.GetMethod.Module == originalMethod.Module
                            ? Instruction.Create(OpCodes.Callvirt, property.GetMethod)
                            : Instruction.Create(OpCodes.Callvirt, originalMethod.Module.ImportReference(property.GetMethod)));
                        currentArg = property.PropertyType.Resolve();

                        return true;
                    }
                }

                if (currentArg.HasInterfaces)
                {
                    foreach (TypeReference intf in currentArg.Interfaces.Select(x => x.InterfaceType))
                    {
                        TypeDefinition previousArg = currentArg;
                        currentArg = intf.Resolve();
                        if (GetMember(weaver, originalMethod, ref currentArg, target, patcher))
                        {
                            return true;
                        }

                        currentArg = previousArg;
                    }
                }

                if (currentArg.BaseType != null && originalMethod.Module.Assembly != currentArg.BaseType.Module.Assembly)
                {
                    TypeReference baseType = currentArg.BaseType;
                    string assemblyPath =
                        $"{targetDirectory}\\{baseType.Scope.Name}{(baseType.Scope.Name.EndsWith(".dll") ? "" : ".dll")}";
                    AssemblyDefinition baseTypeAssembly = project.GetAssembly(assemblyPath, readerParams);
                    currentArg = baseTypeAssembly.MainModule.Types.SingleOrDefault(x => x.FullName == baseType.FullName);
                    baseTypeAssembly.Dispose();
                }
                else if (currentArg.BaseType != null)
                {
                    currentArg = currentArg.BaseType.Resolve();
                }
                else
                {
                    currentArg = null;
                }
            }

            return false;
        }
    }
}
