﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace uMod.Patcher
{
    public class Patch
    {
        /// <summary>
        /// Gets or sets the name of the assembly in which the target type resides
        /// </summary>
        public string AssemblyName { get; set; }

        /// <summary>
        /// Gets or sets a name for this patch
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the fully qualified name for the type in which the target resides
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets if this hook has been flagged
        /// </summary>
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool Flagged { get; set; }

        public Patch()
        {

        }
    }
}
