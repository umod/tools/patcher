using Mono.Cecil;
using System;
using System.IO;
using System.Linq;
using uMod.Patcher.Exceptions;
using uMod.Patcher.Patching;

namespace uMod.Patcher.Fields
{
    public enum Exposure { Private, Protected, Public, Internal, Static, Null }

    /// <summary>
    /// Represents a hook that is applied to single method and calls a single hook
    /// </summary>
    public class Field : Patch, IEquatable<Field>
    {
        /// <summary>
        /// Gets or sets the field to be added
        /// </summary>
        public string FieldType { get; set; }

        public Field()
        {
        }

        public Field(TypeDefinition type, string assembly)
        {
            Name = "NewField";
            TypeName = type.FullName;
            FieldType = string.Empty;
            AssemblyName = assembly;
        }

        public override string ToString()
        {
            return !string.IsNullOrEmpty(TypeName) ? $"{TypeName}::{Name} (Field)" : Name;
        }

        protected static void ShowMsg(string msg, string header, Patching.Patcher patcher = null)
        {
            if (patcher != null)
            {
                patcher.Log(msg);
            }
            else
            {
                throw new FieldInvalidException(header, msg);
            }
        }

        public bool IsValid(bool warn = false)
        {
            string[] fieldData = FieldType.Split('|');

            string targetAssemblyFile = Path.Combine(Project.Current.TargetDirectory, $"{AssemblyName.Replace(".dll", "")}_Original.dll");
            AssemblyDefinition targetAssembly = Project.Current.GetAssembly(targetAssemblyFile);
            TypeDefinition target = targetAssembly.MainModule.GetType(TypeName);
            if (target == null)
            {
                if (warn)
                {
                    ShowMsg($"The type '{TypeName}' in '{AssemblyName}' to add the field '{Name}' into could not be found!", "Target type missing");
                }

                return false;
            }

            if (target.Fields.Any(x => x.Name == Name))
            {
                if (warn)
                {
                    ShowMsg($"A field with the name '{Name}' already exists in the targeted class!", "Duplicate name");
                }

                return false;
            }

            if (Project.Current.Manifests.Find(x => x.AssemblyName == AssemblyName)!.Fields.Where(x => x != this).Any(field => field.Name == Name))
            {
                if (warn)
                {
                    ShowMsg($"A field with the name '{Name}' is already being added to the targeted class!", "Duplicate name");
                }

                return false;
            }

            if (string.IsNullOrEmpty(FieldType))
            {
                return true;
            }

            string newFieldAssemblyFile = Path.Combine(Project.Current.TargetDirectory, $"{fieldData[0].Replace(".dll", "")}.dll");
            AssemblyDefinition newFieldAssembly = Project.Current.GetAssembly(newFieldAssemblyFile);
            TypeDefinition newFieldType = newFieldAssembly?.MainModule?.GetType(fieldData[1]);
            if (!Definitions.TryParseType(FieldType, out _, out string error))
            {
                if (warn)
                {
                    ShowMsg($"The type '{fieldData[1]}' in '{fieldData[0]}' to add the field '{Name}' from could not be found!", "Error resolving field type");
                }

                return false;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Field);
        }

        public bool Equals(Field other)
        {
            return other != null &&
                   AssemblyName == other.AssemblyName &&
                   Name == other.Name &&
                   TypeName == other.TypeName &&
                   FieldType == other.FieldType;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(AssemblyName, Name, TypeName, FieldType);
        }
    }
}
