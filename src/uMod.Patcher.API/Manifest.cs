﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using uMod.Patcher.Fields;
using uMod.Patcher.Hooks;
using uMod.Patcher.Modifiers;

namespace uMod.Patcher
{
    /// <summary>
    /// A set of changes to make to an assembly
    /// </summary>
    [JsonConverter(typeof(Converter))]
    public class Manifest : IEquatable<Manifest>
    {
        [JsonIgnore]
        public string OriginalFile { get; set; }

        [JsonIgnore]
        public string Name => !string.IsNullOrEmpty(OriginalFile) ? Path.GetFileNameWithoutExtension(OriginalFile) : null;

        /// <summary>
        /// Gets or sets the name of the assembly in the target directory
        /// </summary>
        public string AssemblyName { get; set; }

        /// <summary>
        /// Gets or sets the hooks contained in this project
        /// </summary>
        public List<Hook> Hooks { get; set; }

        /// <summary>
        /// Gets or sets the changed modifiers in this project
        /// </summary>
        public List<Modifier> Modifiers { get; set; }

        /// <summary>
        /// Gets or sets the additional fields in this project
        /// </summary>
        public List<Field> Fields { get; set; }

        private static string[] ValidExtensions => new[] { ".dll", ".exe" };

        /// <summary>
        /// Initializes a new instance of the Manifest class
        /// </summary>
        public Manifest()
        {
            // Fill in defaults
            Hooks = new List<Hook>();
            Modifiers = new List<Modifier>();
            Fields = new List<Field>();
        }

        public int GetPatchIndex(Patch patch)
        {
            if (patch is Hook hook)
            {
                return Hooks.FindIndex(p => p.Equals(hook));
            }
            else if (patch is Modifier modifier)
            {
                return Modifiers.FindIndex(p => p.Equals(modifier));
            }
            else if (patch is Field field)
            {
                return Fields.FindIndex(p => p.Equals(field));
            }

            return -1;
        }

        public Patch GetMatchingPatch(Patch patch)
        {
            int index = -1;
            if (patch is Hook hook)
            {
                index = Hooks.FindIndex(p => p.Equals(hook));
                if (index > -1)
                {
                    return Hooks[index];
                }
            }
            else if (patch is Modifier modifier)
            {
                index = Modifiers.FindIndex(p => p.Equals(modifier));
                if (index > -1)
                {
                    return Modifiers[index];
                }
            }
            else if (patch is Field field)
            {
                index = Fields.FindIndex(p => p.Equals(field));
                if (index > -1)
                {
                    return Fields[index];
                }
            }

            return null;
        }

        public void ReplacePatch(Patch patch)
        {
            int index = -1;
            if (patch is Hook hook)
            {
                index = Hooks.FindIndex(p => p.Equals(hook));
                Hooks[index] = hook;
            }
            else if (patch is Modifier modifier)
            {
                index = Modifiers.FindIndex(p => p.Equals(modifier));
                Modifiers[index] = modifier;
            }
            else if (patch is Field field)
            {
                index = Fields.FindIndex(p => p.Equals(field));
                Fields[index] = field;
            }
        }

        public void AddPatch(Patch patch)
        {
            switch (patch)
            {
                case Hook hook:
                    Hooks.Add(hook);
                    break;
                case Modifier modifier:
                    Modifiers.Add(modifier);
                    break;
                case Field field:
                    Fields.Add(field);
                    break;
            }
        }

        public void RemovePatch(Patch patch)
        {
            switch (patch)
            {
                case Hook hook:
                    Hooks.Remove(hook);
                    break;
                case Modifier modifier:
                    Modifiers.Remove(modifier);
                    break;
                case Field field:
                    Fields.Remove(field);
                    break;
            }
        }

        public Manifest Clone()
        {
            Manifest manifest = new Manifest();
            manifest.OriginalFile = OriginalFile;
            return manifest;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Manifest);
        }

        public bool Equals(Manifest other)
        {
            return other != null &&
                   Name == other.Name &&
                   AssemblyName == other.AssemblyName;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, AssemblyName);
        }

        public class Converter : JsonConverter
        {
            private struct HookRef
            {
                public string Type;
                public Hook Hook;
            }

            public override bool CanConvert(Type objectType)
            {
                return objectType == typeof(Manifest);
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                Manifest manifest = existingValue != null ? existingValue as Manifest : new Manifest();
                while (reader.Read() && reader.TokenType != JsonToken.EndObject)
                {
                    if (reader.TokenType != JsonToken.PropertyName)
                    {
                        return null;
                    }

                    string propname = (string)reader.Value;
                    if (!reader.Read())
                    {
                        return null;
                    }

                    switch (propname)
                    {
                        case "AssemblyName":
                            manifest.AssemblyName = (string)reader.Value;
                            if (!string.IsNullOrEmpty(manifest.AssemblyName) && !IsExtensionValid(manifest.AssemblyName))
                            {
                                manifest.AssemblyName += ".dll";
                            }

                            break;

                        case "Hooks":
                            if (reader.TokenType != JsonToken.StartArray)
                            {
                                return null;
                            }

                            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                            {
                                if (reader.TokenType != JsonToken.StartObject)
                                {
                                    return null;
                                }

                                if (!reader.Read())
                                {
                                    return null;
                                }

                                if (reader.TokenType != JsonToken.PropertyName)
                                {
                                    return null;
                                }

                                if ((string)reader.Value != "Type")
                                {
                                    return null;
                                }

                                if (!reader.Read())
                                {
                                    return null;
                                }

                                string hooktype = (string)reader.Value;
                                Type t = Hook.GetHookType(hooktype);
                                if (t == null)
                                {
                                    throw new Exception("Unknown hook type");
                                }

                                Hook hook = Activator.CreateInstance(t) as Hook;
                                if (!reader.Read())
                                {
                                    return null;
                                }

                                if (reader.TokenType != JsonToken.PropertyName)
                                {
                                    return null;
                                }

                                if ((string)reader.Value != "Hook")
                                {
                                    return null;
                                }

                                if (!reader.Read())
                                {
                                    return null;
                                }

                                serializer.Populate(reader, hook);
                                if (!reader.Read())
                                {
                                    return null;
                                }

                                if (!Path.HasExtension(hook.AssemblyName))
                                {
                                    hook.AssemblyName += ".dll";
                                }

                                manifest.Hooks.Add(hook);
                            }
                            break;

                        case "Modifiers":
                            if (reader.TokenType != JsonToken.StartArray)
                            {
                                return null;
                            }

                            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                            {
                                if (reader.TokenType != JsonToken.StartObject)
                                {
                                    return null;
                                }

                                Modifier modifier = Activator.CreateInstance(typeof(Modifier)) as Modifier;
                                serializer.Populate(reader, modifier);

                                if (!Path.HasExtension(modifier.AssemblyName))
                                {
                                    modifier.AssemblyName += ".dll";
                                }

                                manifest.Modifiers.Add(modifier);
                            }
                            break;

                        case "Fields":
                            if (reader.TokenType != JsonToken.StartArray)
                            {
                                return null;
                            }

                            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                            {
                                if (reader.TokenType != JsonToken.StartObject)
                                {
                                    return null;
                                }

                                Field field = Activator.CreateInstance(typeof(Field)) as Field;
                                serializer.Populate(reader, field);

                                if (!Path.HasExtension(field.AssemblyName))
                                {
                                    field.AssemblyName += ".dll";
                                }

                                manifest.Fields.Add(field);
                            }
                            break;
                    }
                }
                foreach (Hook hook in manifest.Hooks)
                {
                    if (!string.IsNullOrWhiteSpace(hook.BaseHookName))
                    {
                        foreach (Hook baseHook in manifest.Hooks)
                        {
                            if (baseHook.Name.Equals(hook.BaseHookName))
                            {
                                hook.BaseHook = baseHook;
                                break;
                            }
                        }
                        if (hook.BaseHook == null)
                        {
                            throw new Exception("BaseHook missing: " + hook.BaseHookName);
                        }
                    }
                }
                return manifest;
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                Manifest manifest = value as Manifest;

                writer.WriteStartObject();

                writer.WritePropertyName("AssemblyName");
                writer.WriteValue(manifest.AssemblyName);

                HookRef[] refs = new HookRef[manifest.Hooks.Count];
                for (int i = 0; i < refs.Length; i++)
                {
                    refs[i].Hook = manifest.Hooks[i];
                    refs[i].Type = refs[i].Hook.GetType().Name;
                    refs[i].Hook.BaseHookName = refs[i].Hook.BaseHook?.Name;
                }

                writer.WritePropertyName("Hooks");
                serializer.Serialize(writer, refs);

                writer.WritePropertyName("Modifiers");
                serializer.Serialize(writer, manifest.Modifiers);

                writer.WritePropertyName("Fields");
                serializer.Serialize(writer, manifest.Fields);

                writer.WriteEndObject();
            }

            private bool IsExtensionValid(string assembly)
            {
                string ext = Path.GetExtension(assembly);
                foreach (string extension in ValidExtensions)
                {
                    if (extension == ext)
                    {
                        return true;
                    }
                }

                return false;
            }
        }
    }
}
