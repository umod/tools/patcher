﻿using Mono.Cecil;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using uMod.Patcher.Exceptions;
using uMod.Patcher.Hooks;
using uMod.Patcher.Patching;
using System;

namespace uMod.Patcher
{
    /// <summary>
    /// An uMod patcher project
    /// </summary>
    public class Project
    {
        public static Project Current;

        [JsonIgnore]
        public HookOperatingSystem Platform { get; set; } = HookOperatingSystem.All;

        /// <summary>
        /// Gets or sets the project name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the directory of the DLLs
        /// </summary>
        public string TargetDirectory { get; set; }

        /// <summary>
        /// Gets or sets all the manifests contained in this project
        /// </summary>
        public List<Manifest> Manifests { get; set; }

        /// <summary>
        /// Gets or sets the core path
        /// </summary>
        [JsonIgnore]
        public string CorePath { get; set; }

        /// <summary>
        /// Initializes a new instance of the Project class with sensible defaults
        /// </summary>
        public Project()
        {
            // Fill in defaults
            Name = "Untitled Project";
            TargetDirectory = "";
            Manifests = new List<Manifest>();
        }

        /// <summary>
        /// Saves this project to file
        /// </summary>
        public void Save(string path)
        {
            // save to file directly
            if (path.EndsWith(".opj", System.StringComparison.InvariantCultureIgnoreCase))
            {
                File.WriteAllText(path, JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
            }
            else if (Directory.Exists(path))
            {
                List<string> files = new List<string>();
                Project[] projects = GetPatchProjects();
                foreach (Project project in projects)
                {
                    string target = Path.Combine(path, $"{project.Name}.opj");
                    project.Save(target);
                    files.Add(Path.GetRelativePath(path, target));
                }

                if(files.Count > 0)
                {
                    string target = Path.Combine(path, $"{Current.Name}.upj");
                    File.WriteAllText(target, string.Join("\n", files));
                }
            }
            else
            {
                throw new IOException($"Path does not exist, cannot save project: {path}");
            }
        }

        public void RenameManifest(string selectedManifest, string newName)
        {
            foreach (Manifest manifest in Manifests)
            {
                if (!string.IsNullOrEmpty(manifest.OriginalFile))
                {
                    string manifestName = Path.GetFileNameWithoutExtension(manifest.OriginalFile);
                    if (manifestName.Equals(selectedManifest, StringComparison.InvariantCultureIgnoreCase))
                    {
                        string originalFile = manifest.OriginalFile;
                        manifest.OriginalFile = manifest.OriginalFile.Replace(selectedManifest, newName);
                        File.Delete(originalFile);
                    }
                }
            }
        }

        /// <summary>
        /// Loads this project from file
        /// </summary>
        /// <returns></returns>
        public static Project Load(string filename, string overrideTarget = null)
        {
            if (!File.Exists(filename))
            {
                return new Project();
            }

            string ext = Path.GetExtension(filename);
            string file = Path.GetFileName(filename);
            string directory = Path.GetDirectoryName(filename);

            string text = File.ReadAllText(filename);

            if (ext == ".upj")
            {
                string[] paths = text.Replace("\r", string.Empty).Split("\n").Select(x => Path.Combine(directory, x)).ToArray();
                return Load(paths, overrideTarget);
            }
            else // opj
            {
                try
                {
                    Project project = JsonConvert.DeserializeObject<Project>(text);

                    if (string.IsNullOrEmpty(project.Name) || project.Name == "Untitled Project")
                    {
                        project.Name = Path.GetFileNameWithoutExtension(filename);
                    }

                    foreach (Manifest manifest in project.Manifests)
                    {
                        manifest.OriginalFile = filename; // save this to ensure file is saved back
                    }

                    if (project != null && !string.IsNullOrEmpty(overrideTarget))
                    {
                        project.TargetDirectory = overrideTarget;
                    }

                    return project;
                }
                catch (JsonReaderException ex)
                {
                    throw new ProjectInvalidException($"There was a problem loading the project file: {file}", "Are all file paths properly escaped?", ex);
                }
            }
        }

        public void DeleteManifest(Manifest manifest)
        {
            if (manifest != null)
            {
                Manifests.Remove(manifest);
                //File.Delete(manifest.OriginalFile);
                //TODO: only delete on save
            }
        }

        public void DeleteManifest(string name)
        {
            Manifest removeManifest = GetManifestByName(name);

            if (removeManifest != null)
            {
                Manifests.Remove(removeManifest);
                //File.Delete(removeManifest.OriginalFile);
                //TODO: only delete on save
            }
        }

        private Manifest GetManifestByName(string name)
        {
            name = Path.GetFileNameWithoutExtension(name);
            foreach (Manifest manifest in Manifests)
            {
                if (!string.IsNullOrEmpty(manifest.OriginalFile))
                {
                    string manifestName = Path.GetFileNameWithoutExtension(manifest.OriginalFile);
                    if (manifestName.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return manifest;
                    }
                }
            }

            return null;
        }

        public static Project Load(IEnumerable<string> paths, string overrideTarget = null)
        {
            Project mainProject = new Project();

            foreach (string path in paths)
            {
                Project subProject = null;

                if (File.Exists(path))
                {
                    subProject = Load(path, overrideTarget);
                }
                else if (Directory.Exists(path))
                {
                    subProject = LoadDirectory(path, overrideTarget);
                }

                if (subProject != null)
                {
                    if (string.IsNullOrEmpty(mainProject.Name) || mainProject.Name == "Untitled Project")
                    {
                        mainProject.Name = subProject.Name;
                    }

                    mainProject.Merge(subProject);
                }
            }

            return mainProject;
        }

        public static Project LoadDirectory(string directory, string overrideTarget = null)
        {
            if (!Directory.Exists(directory))
            {
                return new Project();
            }

            Project mainProject = new Project();

            string[] paths = Directory.GetFiles(directory, "*.opj", SearchOption.TopDirectoryOnly);

            foreach (string path in paths)
            {
                Project subProject = Load(path, overrideTarget);
                if (subProject != null)
                {
                    mainProject.Merge(subProject);
                }
            }

            return mainProject;
        }

        public void Merge(Project project)
        {
            // obtain property data assuming at least one OPJ file contains it
            if (string.IsNullOrEmpty(TargetDirectory) && !string.IsNullOrEmpty(project.TargetDirectory))
            {
                TargetDirectory = project.TargetDirectory;
            }

            if (string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(project.Name))
            {
                Name = project.Name;
            }

            foreach (Manifest manifest in project.Manifests)
            {
                Manifests.Add(manifest);
            }
        }

        public string[] GetManifestNames()
        {
            return Manifests.Select(x => Path.GetFileNameWithoutExtension(x.OriginalFile)).Distinct().ToArray();
        }

        public Manifest GetPrimaryManifest()
        {
            return Manifests.FirstOrDefault(x => x.Name == Name);
        }

        public Project[] GetPatchProjects()
        {
            List<Project> projects = new List<Project>();

            Dictionary<string, List<Manifest>> manifestsByFile = new Dictionary<string, List<Manifest>>();

            string mainProjectFile = $"{Name}.opj";
            foreach (Manifest manifest in Manifests)
            {
                List<Manifest> manifests;
                if (string.IsNullOrEmpty(manifest.OriginalFile) && !manifestsByFile.TryGetValue(mainProjectFile, out manifests))
                {
                    manifestsByFile.Add(mainProjectFile, manifests = new List<Manifest>());
                }
                else if (!manifestsByFile.TryGetValue(manifest.OriginalFile, out manifests))
                {
                    manifestsByFile.Add(manifest.OriginalFile, manifests = new List<Manifest>());
                }

                manifests.Add(manifest);
            }

            foreach (var kvp in manifestsByFile)
            {
                Project project = new Project()
                {
                    TargetDirectory = TargetDirectory,
                    CorePath = CorePath
                };
                foreach (Manifest manifest in kvp.Value)
                {
                    project.Manifests.Add(manifest);
                    if (string.IsNullOrEmpty(project.Name) || project.Name == "Untitled Project")
                    {
                        project.Name = manifest.Name;
                    }
                }

                projects.Add(project);
            }

            return projects.ToArray();
        }

        /// <summary>
        /// Adds an empty manifest with the given assembly name to the project
        /// </summary>
        /// <param name="assemblyName"></param>
        public void AddManifestByAssembly(string assemblyName)
        {
            Manifest manifest = new() { AssemblyName = assemblyName };
            Manifests.Add(manifest);
        }

        /// <summary>
        /// Removes the manifest that references the specified assembly name from the project
        /// </summary>
        /// <param name="assemblyName"></param>
        public void RemoveManifestByAssembly(string assemblyName)
        {
            Manifest manifest = GetManifestByAssembly(assemblyName);
            if (manifest != null)
            {
                Manifests.Remove(manifest);
            }
        }

        /// <summary>
        /// Gets the manifest with the specified assembly name
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public Manifest GetManifestByAssembly(string assemblyName)
        {
            return Manifests.FirstOrDefault(manifest => manifest.AssemblyName == assemblyName);
        }

        public Manifest GetManifest(string assemblyName, string name)
        {
            return Manifests.FirstOrDefault(manifest => manifest.AssemblyName == assemblyName && manifest.Name == name);
        }

        public bool HasMultipleAssemblyManifests(string assemblyName)
        {
            return Manifests.Count(x => x.AssemblyName == assemblyName) > 1;
        }

        /// <summary>
        /// Gets the assembly definition from the specified assembly path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public AssemblyDefinition GetAssembly(string path, ReaderParameters parameters = null)
        {
            string fileName = Path.GetFileName(path);

            if (!File.Exists(path))
            {
                if (fileName == "Oxide.Core.dll" || fileName == "uMod.Core.dll")
                {
                    path = CorePath;
                }
                else if (!string.IsNullOrEmpty(CorePath) && !string.IsNullOrEmpty(fileName))
                {
                    string corePathDirectory = Path.GetDirectoryName(CorePath);
                    if (corePathDirectory != null)
                    {
                        string possiblePath = Path.Combine(corePathDirectory, fileName);
                        if (File.Exists(possiblePath))
                        {
                            path = possiblePath;
                        }
                    }
                }
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"Failed to locate {fileName} assembly");
            }

            return parameters == null ? AssemblyDefinition.ReadAssembly(path) : AssemblyDefinition.ReadAssembly(path, parameters);
        }

        public AssemblyResolver GetAssemblyResolver(string target = null)
        {
            target ??= TargetDirectory;

            string[] directories = {
                target
            };

            if (!string.IsNullOrEmpty(CorePath))
            {
                string corePathDir = Path.GetDirectoryName(CorePath);
                if (corePathDir != target)
                {
                    directories = new[]
                    {
                        target,
                        corePathDir
                    };
                }
            }

            return new AssemblyResolver
            {
                Directories = directories
            };
        }

        public Manifest GetManifest(string name)
        {
            return Manifests.FirstOrDefault(m => m.Name == name);
        }

        public bool HasManifest(string name)
        {
            return Manifests.Any(m => m.Name == name);
        }
    }
}
