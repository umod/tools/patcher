﻿using System;

namespace uMod.Patcher.Exceptions
{
    public class ModifierInvalidException : PatchInvalidException
    {
        public ModifierInvalidException(string header, string message, Exception innerException = null) : base(header, message, innerException)
        {
        }
    }
}
