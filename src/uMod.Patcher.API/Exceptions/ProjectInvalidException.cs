﻿using System;

namespace uMod.Patcher.Exceptions
{
    public class ProjectInvalidException : PatchInvalidException
    {
        public ProjectInvalidException(string header, string message, Exception innerException = null) : base(header, message, innerException)
        {
        }
    }
}
