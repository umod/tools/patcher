﻿using System;

namespace uMod.Patcher.Exceptions
{
    public class PatchInvalidException : Exception
    {
        public string Header { get; }
        public string Text { get; }

        public PatchInvalidException(string header, string message, Exception innerException = null) : base($"{header}: {message}", innerException)
        {
            Header = header;
            Text = message;
        }
    }
}
