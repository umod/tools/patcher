﻿using ICSharpCode.Decompiler;
using ICSharpCode.Decompiler.CSharp;
using ICSharpCode.Decompiler.CSharp.OutputVisitor;
using ICSharpCode.Decompiler.Metadata;
using ICSharpCode.Decompiler.TypeSystem;
using Mono.Cecil.Cil;
using Mono.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uMod.Patcher.Patching;
using MethodBody = Mono.Cecil.Cil.MethodBody;
using MethodDefinition = Mono.Cecil.MethodDefinition;
using ParameterDefinition = Mono.Cecil.ParameterDefinition;

namespace uMod.Patcher
{
    /// <summary>
    /// Contains code decompiling utility methods
    /// </summary>
    public static class Decompiler
    {
        /// <summary>
        /// Decompiles the specified method body to MSIL
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public static string DecompileToIl(MethodBody body)
        {
            StringBuilder sb = new();
            if (body?.Instructions == null)
            {
                return null;
            }

            Collection<Instruction> instructions = body.Instructions;
            for (int i = 0; i < instructions.Count; i++)
            {
                Instruction inst = instructions[i];
                sb.AppendLine(inst.ToString().Replace("\n", "\\n"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Decompiles the specified method definition to source code
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="weaver"></param>
        /// <returns></returns>
        public static async Task<string> GetSourceCode(MethodDefinition definition, IlWeaver weaver = null)
        {
            return await Task.Run(() =>
            {
                try
                {
                    weaver?.Apply(definition.Body);

                    using (MemoryStream moduleStream = new())
                    {
                        definition.Module.Assembly.Write(moduleStream);
                        moduleStream.Position = 0;

                        PEFile peFile = new("patched.dll", moduleStream);
                        IAssemblyResolver resolver = new UniversalAssemblyResolver(definition.Module.FileName, false, peFile.DetectTargetFrameworkId());

                        CSharpFormattingOptions formattingOptions = FormattingOptionsFactory.CreateAllman();
                        formattingOptions.IndentationString = "    ";
                        formattingOptions.RemoveEndOfLineWhiteSpace = false;

                        DecompilerSettings settings = new();
                        settings.SetLanguageVersion(LanguageVersion.Latest);
                        settings.NamedArguments = false;
                        settings.CSharpFormattingOptions = formattingOptions;

                        CSharpDecompiler decompiler = new(peFile, resolver, settings);
                        ITypeDefinition typeInfo = decompiler.TypeSystem.MainModule.Compilation.FindType(new FullTypeName(definition.DeclaringType.FullName.Replace("/", "+"))).GetDefinition();
                        if (typeInfo is null)
                        {
                            throw new Exception();
                        }

                        IMethod methodInfo = typeInfo.Methods.First(x =>
                            x.ReturnType.ReflectionName.Equals(definition.ReturnType.FullName.Replace("<", "[[").Replace(">", "]]").Replace("/", "+"), StringComparison.InvariantCultureIgnoreCase) &&
                            x.Name.Equals(definition.Name, StringComparison.OrdinalIgnoreCase) &&
                            HasMatchingParameters(x, definition)
                        );

                        return decompiler.DecompileAsString(methodInfo.MetadataToken);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        return $"Error in creating source code from IL: {ex.Message} {Environment.NewLine}{ex.StackTrace} {Environment.NewLine}{Environment.NewLine}{ex.InnerException.Message} {Environment.NewLine}{ex.InnerException.StackTrace}";
                    }

                    return $"Error in creating source code from IL: {ex.Message}{Environment.NewLine}{ex.StackTrace}";
                }
                finally
                {
                    if (weaver != null)
                    {
                        definition.Body = null;
                    }
                }
            });
        }

        private static bool HasMatchingParameters(IMethod method, MethodDefinition definition)
        {
            if (method.Parameters.Count != definition.Parameters.Count) return false;

            for (int i = 0; i < definition.Parameters.Count; i++)
            {
                IParameter methodParameter = method.Parameters[i];
                ParameterDefinition definitionParameter = definition.Parameters[i];

                if (!methodParameter.Name.Equals(definitionParameter.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }

                if (methodParameter.Type is ParameterizedType genericMethodType && definitionParameter.ParameterType is Mono.Cecil.GenericInstanceType genericDefinitionType)
                {
                    if (!genericMethodType.GenericType.ReflectionName.Equals(genericDefinitionType.ElementType.FullName, StringComparison.OrdinalIgnoreCase))
                    {
                        return false;
                    }

                    if (genericMethodType.TypeArguments.Count != genericDefinitionType.GenericArguments.Count)
                    {
                        return false;
                    }

                    for (int j = 0; j < methodParameter.Type.TypeArguments.Count; j++)
                    {
                        IType genericMethodArgument = genericMethodType.TypeArguments[j];
                        Mono.Cecil.TypeReference genericDefinitionArgument = genericDefinitionType.GenericArguments[j];

                        if (!genericMethodArgument.FullName.Equals(genericDefinitionArgument.FullName.Replace("/", "."), StringComparison.OrdinalIgnoreCase))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if (!methodParameter.Type.FullName.Equals(definitionParameter.ParameterType.FullName.Replace("/", "."), StringComparison.OrdinalIgnoreCase))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
