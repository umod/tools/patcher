﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uMod.Patcher.Fields;
using uMod.Patcher.Hooks;
using uMod.Patcher.Modifiers;

namespace uMod.Patcher
{
    public class Notification
    {

    }

    public class AssemblyMissingNotification : Notification
    {
        public AssemblyMissingNotification(string assemblyName)
        {
            AssemblyName = assemblyName;
        }

        public string AssemblyName { get; }
    }

    public class MethodChangedNotification : Notification
    {
        public Hook Hook { get; }

        public MethodChangedNotification(Hook hook)
        {
            this.Hook = hook;
        }
    }

    public class MethodMissingNotification : Notification
    {
        public Hook Hook  { get; }

        public MethodMissingNotification(Hook hook)
        {
            Hook = hook;
        }
    }

    public class FieldModificationChangedNotification : Notification
    {
        public Modifier Modifier  { get; }

        public FieldModificationChangedNotification(Modifier modifier)
        {
            Modifier = modifier;
        }
    }

    public class NewFieldChangedNotification : Notification
    {
        public Field Field { get; }

        public NewFieldChangedNotification(Field field)
        {
            Field = field;
        }
    }

    public class MethodModificationChangedNotification : Notification
    {
        public Modifier Modifier;

        public MethodModificationChangedNotification(Modifier modifier)
        {
            Modifier = modifier;
        }
    }

    public class PropertyChangedNotification : Notification
    {
        public Modifier Modifier;

        public PropertyChangedNotification(Modifier modifier)
        {
            Modifier = modifier;
        }
    }
}
