﻿using System.IO;
using System.Windows.Forms;

namespace uMod.Patcher
{
    public partial class ProjectSettingsControl : UserControl
    {
        public Project ProjectObject { get; set; }

        public string ProjectFilename { get; set; }

        public ProjectSettingsControl()
        {
            InitializeComponent();
        }

        private void ProjectSettingsControl_Load(object sender, System.EventArgs e)
        {
            nametextbox.Text = ProjectObject.Name;
            directorytextbox.Text = ProjectObject.TargetDirectory;
            filenametextbox.Text = ProjectFilename;
            foreach (string name in ProjectObject.GetManifestNames())
            {
                manifestlist.Items.Add(name);
            }
        }

        private void savebutton_Click(object sender, System.EventArgs e)
        {
            // Verify
            if (!Directory.Exists(directorytextbox.Text))
            {
                MessageBox.Show(this, "The target directory is invalid.", "uMod Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Directory.Exists(Path.GetDirectoryName(filenametextbox.Text)))
            {
                MessageBox.Show(this, "The filename is invalid.", "uMod Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (nametextbox.TextLength == 0)
            {
                MessageBox.Show(this, "The project name is invalid.", "uMod Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string oldname = ProjectObject.Name;
            string oldtarget = ProjectObject.TargetDirectory;
            // Save
            ProjectObject.Name = nametextbox.Text;
            ProjectObject.TargetDirectory = directorytextbox.Text;
            PatcherForm.MainForm.AddHistory(new ProjectChanged(oldname, oldtarget, ProjectObject));
            //ProjectObject.Save(ProjectFilename);
        }

        private void renameToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if(manifestlist.SelectedItems.Count == 0)
            {
                return;
            }

            string selectedManifest = manifestlist.SelectedItems[0].Text;
            string newName = string.Empty;
            var result = FormUtility.ShowInputDialog($"Rename {selectedManifest}",ref newName);
            if(result == DialogResult.OK)
            {
                ProjectObject.RenameManifest(selectedManifest, newName);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if(manifestlist.SelectedItems.Count == 0)
            {
                return;
            }

            string selectedManifest = manifestlist.SelectedItems[0].Text;
            ProjectObject.DeleteManifest(selectedManifest);
        }

        
    }
}
