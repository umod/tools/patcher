﻿
namespace uMod.Patcher.Views
{
    partial class NotificationsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.notificationtree = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // notificationtree
            // 
            this.notificationtree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notificationtree.HideSelection = false;
            this.notificationtree.Location = new System.Drawing.Point(0, 0);
            this.notificationtree.Name = "notificationtree";
            this.notificationtree.Size = new System.Drawing.Size(551, 337);
            this.notificationtree.TabIndex = 0;
            this.notificationtree.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.notificationtree_NodeMouseDoubleClick);
            // 
            // NotificationsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.notificationtree);
            this.Name = "NotificationsControl";
            this.Size = new System.Drawing.Size(551, 337);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView notificationtree;
    }
}
