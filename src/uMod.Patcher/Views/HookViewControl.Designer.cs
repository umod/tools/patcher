﻿namespace uMod.Patcher.Views
{
    partial class HookViewControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.detailsgroup = new System.Windows.Forms.GroupBox();
            this.detailstable = new System.Windows.Forms.TableLayoutPanel();
            this.typenametextbox = new System.Windows.Forms.TextBox();
            this.assemblytextbox = new System.Windows.Forms.TextBox();
            this.typenamelabel = new System.Windows.Forms.Label();
            this.assemblylabel = new System.Windows.Forms.Label();
            this.methodnamelabel = new System.Windows.Forms.Label();
            this.methodnametextbox = new System.Windows.Forms.TextBox();
            this.buttonholder = new System.Windows.Forms.FlowLayoutPanel();
            this.flagbutton = new System.Windows.Forms.Button();
            this.unflagbutton = new System.Windows.Forms.Button();
            this.applybutton = new System.Windows.Forms.Button();
            this.deletebutton = new System.Windows.Forms.Button();
            this.clonebutton = new System.Windows.Forms.Button();
            this.namelabel = new System.Windows.Forms.Label();
            this.nametextbox = new System.Windows.Forms.TextBox();
            this.hooknamelabel = new System.Windows.Forms.Label();
            this.hooktypelabel = new System.Windows.Forms.Label();
            this.basehooklabel = new System.Windows.Forms.Label();
            this.hooknametextbox = new System.Windows.Forms.TextBox();
            this.hooktypedropdown = new System.Windows.Forms.ComboBox();
            this.BaseHookDropdown = new System.Windows.Forms.ComboBox();
            this.typeSearchButton = new System.Windows.Forms.Button();
            this.BaseHookSearchButton = new System.Windows.Forms.Button();
            this.MethodSelectButton = new System.Windows.Forms.Button();
            this.tabview = new System.Windows.Forms.TabControl();
            this.hooksettingstab = new System.Windows.Forms.TabPage();
            this.beforetab = new System.Windows.Forms.TabPage();
            this.aftertab = new System.Windows.Forms.TabPage();
            this.codebeforetab = new System.Windows.Forms.TabPage();
            this.codeaftertab = new System.Windows.Forms.TabPage();
            this.codedifftab = new System.Windows.Forms.TabPage();
            this.capturegroupstab = new System.Windows.Forms.TabPage();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToCaptureGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailsgroup.SuspendLayout();
            this.detailstable.SuspendLayout();
            this.buttonholder.SuspendLayout();
            this.tabview.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // detailsgroup
            // 
            this.detailsgroup.Controls.Add(this.detailstable);
            this.detailsgroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.detailsgroup.Location = new System.Drawing.Point(6, 6);
            this.detailsgroup.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.detailsgroup.Name = "detailsgroup";
            this.detailsgroup.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.detailsgroup.Size = new System.Drawing.Size(726, 286);
            this.detailsgroup.TabIndex = 0;
            this.detailsgroup.TabStop = false;
            this.detailsgroup.Text = "Hook Details";
            // 
            // detailstable
            // 
            this.detailstable.ColumnCount = 3;
            this.detailstable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.detailstable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81F));
            this.detailstable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.detailstable.Controls.Add(this.typenametextbox, 1, 1);
            this.detailstable.Controls.Add(this.assemblytextbox, 1, 0);
            this.detailstable.Controls.Add(this.typenamelabel, 0, 1);
            this.detailstable.Controls.Add(this.assemblylabel, 0, 0);
            this.detailstable.Controls.Add(this.methodnamelabel, 0, 2);
            this.detailstable.Controls.Add(this.methodnametextbox, 1, 2);
            this.detailstable.Controls.Add(this.buttonholder, 0, 7);
            this.detailstable.Controls.Add(this.namelabel, 0, 3);
            this.detailstable.Controls.Add(this.nametextbox, 1, 3);
            this.detailstable.Controls.Add(this.hooknamelabel, 0, 4);
            this.detailstable.Controls.Add(this.hooktypelabel, 0, 5);
            this.detailstable.Controls.Add(this.basehooklabel, 0, 6);
            this.detailstable.Controls.Add(this.hooknametextbox, 1, 4);
            this.detailstable.Controls.Add(this.hooktypedropdown, 1, 5);
            this.detailstable.Controls.Add(this.BaseHookDropdown, 1, 6);
            this.detailstable.Controls.Add(this.typeSearchButton, 2, 1);
            this.detailstable.Controls.Add(this.BaseHookSearchButton, 2, 6);
            this.detailstable.Controls.Add(this.MethodSelectButton, 2, 2);
            this.detailstable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailstable.Location = new System.Drawing.Point(4, 19);
            this.detailstable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.detailstable.Name = "detailstable";
            this.detailstable.RowCount = 8;
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.detailstable.Size = new System.Drawing.Size(718, 264);
            this.detailstable.TabIndex = 0;
            // 
            // typenametextbox
            // 
            this.typenametextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typenametextbox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.typenametextbox.Location = new System.Drawing.Point(111, 36);
            this.typenametextbox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.typenametextbox.Name = "typenametextbox";
            this.typenametextbox.ReadOnly = true;
            this.typenametextbox.Size = new System.Drawing.Size(573, 23);
            this.typenametextbox.TabIndex = 2;
            // 
            // assemblytextbox
            // 
            this.assemblytextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assemblytextbox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.assemblytextbox.Location = new System.Drawing.Point(111, 3);
            this.assemblytextbox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.assemblytextbox.Name = "assemblytextbox";
            this.assemblytextbox.ReadOnly = true;
            this.assemblytextbox.Size = new System.Drawing.Size(573, 23);
            this.assemblytextbox.TabIndex = 1;
            // 
            // typenamelabel
            // 
            this.typenamelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typenamelabel.Location = new System.Drawing.Point(4, 33);
            this.typenamelabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.typenamelabel.Name = "typenamelabel";
            this.typenamelabel.Size = new System.Drawing.Size(99, 33);
            this.typenamelabel.TabIndex = 1;
            this.typenamelabel.Text = "Type Name:";
            this.typenamelabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // assemblylabel
            // 
            this.assemblylabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assemblylabel.Location = new System.Drawing.Point(4, 0);
            this.assemblylabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.assemblylabel.Name = "assemblylabel";
            this.assemblylabel.Size = new System.Drawing.Size(99, 33);
            this.assemblylabel.TabIndex = 1;
            this.assemblylabel.Text = "Assembly:";
            this.assemblylabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // methodnamelabel
            // 
            this.methodnamelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.methodnamelabel.Location = new System.Drawing.Point(4, 66);
            this.methodnamelabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.methodnamelabel.Name = "methodnamelabel";
            this.methodnamelabel.Size = new System.Drawing.Size(99, 33);
            this.methodnamelabel.TabIndex = 0;
            this.methodnamelabel.Text = "Method Name:";
            this.methodnamelabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // methodnametextbox
            // 
            this.methodnametextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.methodnametextbox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.methodnametextbox.Location = new System.Drawing.Point(111, 69);
            this.methodnametextbox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.methodnametextbox.Name = "methodnametextbox";
            this.methodnametextbox.ReadOnly = true;
            this.methodnametextbox.Size = new System.Drawing.Size(573, 23);
            this.methodnametextbox.TabIndex = 4;
            // 
            // buttonholder
            // 
            this.detailstable.SetColumnSpan(this.buttonholder, 2);
            this.buttonholder.Controls.Add(this.flagbutton);
            this.buttonholder.Controls.Add(this.unflagbutton);
            this.buttonholder.Controls.Add(this.applybutton);
            this.buttonholder.Controls.Add(this.deletebutton);
            this.buttonholder.Controls.Add(this.clonebutton);
            this.buttonholder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonholder.Location = new System.Drawing.Point(4, 234);
            this.buttonholder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonholder.Name = "buttonholder";
            this.buttonholder.Size = new System.Drawing.Size(680, 35);
            this.buttonholder.TabIndex = 11;
            // 
            // flagbutton
            // 
            this.flagbutton.Location = new System.Drawing.Point(4, 3);
            this.flagbutton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flagbutton.Name = "flagbutton";
            this.flagbutton.Size = new System.Drawing.Size(64, 27);
            this.flagbutton.TabIndex = 0;
            this.flagbutton.Text = "Flag";
            this.flagbutton.UseVisualStyleBackColor = true;
            this.flagbutton.Click += new System.EventHandler(this.flagbutton_Click);
            // 
            // unflagbutton
            // 
            this.unflagbutton.Location = new System.Drawing.Point(76, 3);
            this.unflagbutton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.unflagbutton.Name = "unflagbutton";
            this.unflagbutton.Size = new System.Drawing.Size(72, 27);
            this.unflagbutton.TabIndex = 1;
            this.unflagbutton.Text = "Unflag";
            this.unflagbutton.UseVisualStyleBackColor = true;
            this.unflagbutton.Click += new System.EventHandler(this.unflagbutton_Click);
            // 
            // applybutton
            // 
            this.applybutton.Location = new System.Drawing.Point(156, 3);
            this.applybutton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.applybutton.Name = "applybutton";
            this.applybutton.Size = new System.Drawing.Size(112, 27);
            this.applybutton.TabIndex = 2;
            this.applybutton.Text = "Apply Changes";
            this.applybutton.UseVisualStyleBackColor = true;
            this.applybutton.Click += new System.EventHandler(this.applybutton_Click);
            // 
            // deletebutton
            // 
            this.deletebutton.Location = new System.Drawing.Point(276, 3);
            this.deletebutton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.deletebutton.Name = "deletebutton";
            this.deletebutton.Size = new System.Drawing.Size(102, 27);
            this.deletebutton.TabIndex = 3;
            this.deletebutton.Text = "Remove";
            this.deletebutton.UseVisualStyleBackColor = true;
            this.deletebutton.Click += new System.EventHandler(this.deletebutton_Click);
            // 
            // clonebutton
            // 
            this.clonebutton.Location = new System.Drawing.Point(386, 3);
            this.clonebutton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.clonebutton.Name = "clonebutton";
            this.clonebutton.Size = new System.Drawing.Size(88, 27);
            this.clonebutton.TabIndex = 4;
            this.clonebutton.Text = "Clone";
            this.clonebutton.UseVisualStyleBackColor = true;
            this.clonebutton.Click += new System.EventHandler(this.clonebutton_Click);
            // 
            // namelabel
            // 
            this.namelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.namelabel.Location = new System.Drawing.Point(4, 99);
            this.namelabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.namelabel.Name = "namelabel";
            this.namelabel.Size = new System.Drawing.Size(99, 33);
            this.namelabel.TabIndex = 6;
            this.namelabel.Text = "Name:";
            this.namelabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nametextbox
            // 
            this.nametextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nametextbox.Location = new System.Drawing.Point(111, 102);
            this.nametextbox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.nametextbox.MaxLength = 40;
            this.nametextbox.Name = "nametextbox";
            this.nametextbox.Size = new System.Drawing.Size(573, 23);
            this.nametextbox.TabIndex = 6;
            this.nametextbox.TextChanged += new System.EventHandler(this.nametextbox_TextChanged);
            // 
            // hooknamelabel
            // 
            this.hooknamelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hooknamelabel.Location = new System.Drawing.Point(4, 132);
            this.hooknamelabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hooknamelabel.Name = "hooknamelabel";
            this.hooknamelabel.Size = new System.Drawing.Size(99, 33);
            this.hooknamelabel.TabIndex = 8;
            this.hooknamelabel.Text = "Hook Name:";
            this.hooknamelabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // hooktypelabel
            // 
            this.hooktypelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hooktypelabel.Location = new System.Drawing.Point(4, 165);
            this.hooktypelabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hooktypelabel.Name = "hooktypelabel";
            this.hooktypelabel.Size = new System.Drawing.Size(99, 33);
            this.hooktypelabel.TabIndex = 9;
            this.hooktypelabel.Text = "Hook Type:";
            this.hooktypelabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // basehooklabel
            // 
            this.basehooklabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.basehooklabel.Location = new System.Drawing.Point(4, 198);
            this.basehooklabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.basehooklabel.Name = "basehooklabel";
            this.basehooklabel.Size = new System.Drawing.Size(99, 33);
            this.basehooklabel.TabIndex = 9;
            this.basehooklabel.Text = "Base Hook:";
            this.basehooklabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // hooknametextbox
            // 
            this.hooknametextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hooknametextbox.Location = new System.Drawing.Point(111, 135);
            this.hooknametextbox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.hooknametextbox.MaxLength = 40;
            this.hooknametextbox.Name = "hooknametextbox";
            this.hooknametextbox.Size = new System.Drawing.Size(573, 23);
            this.hooknametextbox.TabIndex = 7;
            this.hooknametextbox.TextChanged += new System.EventHandler(this.hooknametextbox_TextChanged);
            // 
            // hooktypedropdown
            // 
            this.hooktypedropdown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hooktypedropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hooktypedropdown.FormattingEnabled = true;
            this.hooktypedropdown.Location = new System.Drawing.Point(111, 168);
            this.hooktypedropdown.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.hooktypedropdown.Name = "hooktypedropdown";
            this.hooktypedropdown.Size = new System.Drawing.Size(573, 23);
            this.hooktypedropdown.TabIndex = 8;
            this.hooktypedropdown.SelectedIndexChanged += new System.EventHandler(this.hooktypedropdown_SelectedIndexChanged);
            // 
            // BaseHookDropdown
            // 
            this.BaseHookDropdown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BaseHookDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BaseHookDropdown.Enabled = false;
            this.BaseHookDropdown.FormattingEnabled = true;
            this.BaseHookDropdown.Location = new System.Drawing.Point(111, 201);
            this.BaseHookDropdown.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BaseHookDropdown.Name = "BaseHookDropdown";
            this.BaseHookDropdown.Size = new System.Drawing.Size(573, 23);
            this.BaseHookDropdown.TabIndex = 9;
            this.BaseHookDropdown.SelectedIndexChanged += new System.EventHandler(this.basehookdropdown_SelectedIndexChanged);
            // 
            // typeSearchButton
            // 
            this.typeSearchButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typeSearchButton.Image = global::uMod.Patcher.Properties.Resources.magnifying_glass_small;
            this.typeSearchButton.Location = new System.Drawing.Point(692, 36);
            this.typeSearchButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.typeSearchButton.Name = "typeSearchButton";
            this.typeSearchButton.Size = new System.Drawing.Size(22, 27);
            this.typeSearchButton.TabIndex = 3;
            this.typeSearchButton.UseVisualStyleBackColor = true;
            this.typeSearchButton.Click += new System.EventHandler(this.typeSearchButton_Click);
            // 
            // BaseHookSearchButton
            // 
            this.BaseHookSearchButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BaseHookSearchButton.Enabled = false;
            this.BaseHookSearchButton.Image = global::uMod.Patcher.Properties.Resources.magnifying_glass_small;
            this.BaseHookSearchButton.Location = new System.Drawing.Point(692, 201);
            this.BaseHookSearchButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BaseHookSearchButton.Name = "BaseHookSearchButton";
            this.BaseHookSearchButton.Size = new System.Drawing.Size(22, 27);
            this.BaseHookSearchButton.TabIndex = 10;
            this.BaseHookSearchButton.UseVisualStyleBackColor = true;
            this.BaseHookSearchButton.Click += new System.EventHandler(this.baseHookSearchButton_Click);
            // 
            // MethodSelectButton
            // 
            this.MethodSelectButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MethodSelectButton.Image = global::uMod.Patcher.Properties.Resources.pencil;
            this.MethodSelectButton.Location = new System.Drawing.Point(692, 69);
            this.MethodSelectButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MethodSelectButton.Name = "MethodSelectButton";
            this.MethodSelectButton.Size = new System.Drawing.Size(22, 27);
            this.MethodSelectButton.TabIndex = 5;
            this.MethodSelectButton.UseVisualStyleBackColor = true;
            this.MethodSelectButton.Click += new System.EventHandler(this.MethodSelectButton_Click);
            // 
            // tabview
            // 
            this.tabview.Controls.Add(this.hooksettingstab);
            this.tabview.Controls.Add(this.beforetab);
            this.tabview.Controls.Add(this.aftertab);
            this.tabview.Controls.Add(this.codebeforetab);
            this.tabview.Controls.Add(this.codeaftertab);
            this.tabview.Controls.Add(this.codedifftab);
            this.tabview.Controls.Add(this.capturegroupstab);
            this.tabview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabview.Location = new System.Drawing.Point(6, 292);
            this.tabview.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabview.Name = "tabview";
            this.tabview.SelectedIndex = 0;
            this.tabview.Size = new System.Drawing.Size(726, 255);
            this.tabview.TabIndex = 12;
            this.tabview.SelectedIndexChanged += new System.EventHandler(this.tabview_SelectedIndexChanged);
            // 
            // hooksettingstab
            // 
            this.hooksettingstab.Location = new System.Drawing.Point(4, 24);
            this.hooksettingstab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.hooksettingstab.Name = "hooksettingstab";
            this.hooksettingstab.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.hooksettingstab.Size = new System.Drawing.Size(718, 227);
            this.hooksettingstab.TabIndex = 0;
            this.hooksettingstab.Text = "Hook Settings";
            this.hooksettingstab.UseVisualStyleBackColor = true;
            // 
            // beforetab
            // 
            this.beforetab.Location = new System.Drawing.Point(4, 24);
            this.beforetab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.beforetab.Name = "beforetab";
            this.beforetab.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.beforetab.Size = new System.Drawing.Size(718, 227);
            this.beforetab.TabIndex = 1;
            this.beforetab.Text = "MSIL Before";
            this.beforetab.UseVisualStyleBackColor = true;
            // 
            // aftertab
            // 
            this.aftertab.Location = new System.Drawing.Point(4, 24);
            this.aftertab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.aftertab.Name = "aftertab";
            this.aftertab.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.aftertab.Size = new System.Drawing.Size(718, 227);
            this.aftertab.TabIndex = 2;
            this.aftertab.Text = "MSIL After";
            this.aftertab.UseVisualStyleBackColor = true;
            // 
            // codebeforetab
            // 
            this.codebeforetab.Location = new System.Drawing.Point(4, 24);
            this.codebeforetab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.codebeforetab.Name = "codebeforetab";
            this.codebeforetab.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.codebeforetab.Size = new System.Drawing.Size(718, 227);
            this.codebeforetab.TabIndex = 4;
            this.codebeforetab.Text = "Code Before";
            this.codebeforetab.UseVisualStyleBackColor = true;
            // 
            // codeaftertab
            // 
            this.codeaftertab.Location = new System.Drawing.Point(4, 24);
            this.codeaftertab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.codeaftertab.Name = "codeaftertab";
            this.codeaftertab.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.codeaftertab.Size = new System.Drawing.Size(718, 227);
            this.codeaftertab.TabIndex = 3;
            this.codeaftertab.Text = "Code After";
            this.codeaftertab.UseVisualStyleBackColor = true;
            // 
            // codedifftab
            // 
            this.codedifftab.Location = new System.Drawing.Point(4, 24);
            this.codedifftab.Name = "codedifftab";
            this.codedifftab.Size = new System.Drawing.Size(718, 227);
            this.codedifftab.TabIndex = 6;
            this.codedifftab.Text = "Code Diff";
            this.codedifftab.UseVisualStyleBackColor = true;
            // 
            // capturegroupstab
            // 
            this.capturegroupstab.Location = new System.Drawing.Point(4, 24);
            this.capturegroupstab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.capturegroupstab.Name = "capturegroupstab";
            this.capturegroupstab.Size = new System.Drawing.Size(718, 227);
            this.capturegroupstab.TabIndex = 5;
            this.capturegroupstab.Text = "Capture Groups";
            this.capturegroupstab.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToCaptureGroupToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(192, 26);
            // 
            // addToCaptureGroupToolStripMenuItem
            // 
            this.addToCaptureGroupToolStripMenuItem.Name = "addToCaptureGroupToolStripMenuItem";
            this.addToCaptureGroupToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.addToCaptureGroupToolStripMenuItem.Text = "Add to Capture Group";
            // 
            // HookViewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabview);
            this.Controls.Add(this.detailsgroup);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "HookViewControl";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.Size = new System.Drawing.Size(738, 553);
            this.detailsgroup.ResumeLayout(false);
            this.detailstable.ResumeLayout(false);
            this.detailstable.PerformLayout();
            this.buttonholder.ResumeLayout(false);
            this.tabview.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox detailsgroup;
        private System.Windows.Forms.TableLayoutPanel detailstable;
        private System.Windows.Forms.Label methodnamelabel;
        private System.Windows.Forms.TextBox methodnametextbox;
        private System.Windows.Forms.FlowLayoutPanel buttonholder;
        private System.Windows.Forms.Button deletebutton;
        private System.Windows.Forms.Label namelabel;
        private System.Windows.Forms.TextBox nametextbox;
        private System.Windows.Forms.Label hooknamelabel;
        private System.Windows.Forms.Label hooktypelabel;
        private System.Windows.Forms.Label basehooklabel;
        private System.Windows.Forms.TextBox hooknametextbox;
        private System.Windows.Forms.ComboBox hooktypedropdown;
        public System.Windows.Forms.ComboBox BaseHookDropdown;
        private System.Windows.Forms.Button flagbutton;
        private System.Windows.Forms.Button unflagbutton;
        private System.Windows.Forms.Button applybutton;
        private System.Windows.Forms.TabControl tabview;
        private System.Windows.Forms.TabPage hooksettingstab;
        private System.Windows.Forms.TabPage beforetab;
        private System.Windows.Forms.TabPage aftertab;
        private System.Windows.Forms.Label typenamelabel;
        private System.Windows.Forms.Label assemblylabel;
        private System.Windows.Forms.TextBox typenametextbox;
        private System.Windows.Forms.TextBox assemblytextbox;
        private System.Windows.Forms.TabPage codeaftertab;
        private System.Windows.Forms.TabPage codebeforetab;
        private System.Windows.Forms.Button clonebutton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addToCaptureGroupToolStripMenuItem;
        private System.Windows.Forms.TabPage capturegroupstab;
        private System.Windows.Forms.Button typeSearchButton;
        public System.Windows.Forms.Button BaseHookSearchButton;
        private System.Windows.Forms.Button MethodSelectButton;
        private System.Windows.Forms.TabPage codedifftab;
    }
}
