﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace uMod.Patcher.Views
{
    public partial class NotificationsControl : UserControl
    {
        /// <summary>
        /// Gets or sets the main patcher form
        /// </summary>
        public PatcherForm MainForm { get; set; }

        public NotificationsControl()
        {
            InitializeComponent();
        }

        public List<Notification> Notifications { get; internal set; }

        public string GetIssueTitle(Notification notification)
        {
            switch (notification)
            {
                case AssemblyMissingNotification:
                    return "Assemblies are missing from the target directory";
                case MethodMissingNotification:
                    return "Methods referenced by hooks no longer exist";
                case MethodChangedNotification:
                    return "Methods referenced by hooks have changed";
                case FieldModificationChangedNotification:
                    return "Fields with altered modifiers have changed";
                case MethodModificationChangedNotification:
                    return "Methods with altered modifiers have changed";
                case PropertyChangedNotification:
                    return "Properties with altered modifiers have changed";
                case NewFieldChangedNotification:
                    return "New fields were flagged";
            }

            return string.Empty;
        }

        public void PopulateNotifications()
        {
            Dictionary<Type, TreeNode> topNodes = new Dictionary<Type, TreeNode>();

            foreach (Notification notification in Notifications)
            {
                if (!topNodes.TryGetValue(notification.GetType(), out TreeNode topNode))
                {
                    topNodes.Add(notification.GetType(), topNode = new TreeNode(GetIssueTitle(notification)));
                }
                switch (notification)
                {
                    case AssemblyMissingNotification assemblyMissingNotification:
                        topNode.Nodes.Add(new TreeNode(assemblyMissingNotification.AssemblyName)
                        {
                            Tag = notification
                        });
                        break;
                    case MethodMissingNotification methodMissingNotification:
                        topNode.Nodes.Add(new TreeNode($"{methodMissingNotification.Hook.TypeName}.{methodMissingNotification.Hook.Name}")
                        {
                            Tag = notification
                        });
                        break;
                    case MethodChangedNotification methodChangedNotification:
                        topNode.Nodes.Add(new TreeNode($"{methodChangedNotification.Hook.TypeName}.{methodChangedNotification.Hook.Name}")
                        {
                            Tag = notification
                        });
                        break;
                    case FieldModificationChangedNotification fieldChangedNotification:
                        topNode.Nodes.Add(new TreeNode($"{fieldChangedNotification.Modifier.TypeName}.{fieldChangedNotification.Modifier.Name}")
                        {
                            Tag = notification
                        });
                        break;
                    case MethodModificationChangedNotification methodModificationChangedNotification:
                        topNode.Nodes.Add(new TreeNode($"{methodModificationChangedNotification.Modifier.TypeName}.{methodModificationChangedNotification.Modifier.Name}")
                        {
                            Tag = notification
                        });
                        break;
                    case PropertyChangedNotification propertyChangedNotification:
                        topNode.Nodes.Add(new TreeNode($"{propertyChangedNotification.Modifier.TypeName}.{propertyChangedNotification.Modifier.Name}")
                        {
                            Tag = notification
                        });
                        break;
                    case NewFieldChangedNotification newFieldChangedNotification:
                        topNode.Nodes.Add(new TreeNode($"{newFieldChangedNotification.Field.TypeName}.{newFieldChangedNotification.Field.Name}")
                        {
                            Tag = notification
                        });
                        break;
                }
            }

            foreach (var kvp in topNodes)
            {
                notificationtree.Nodes.Add(kvp.Value);
            }
        }

        private void notificationtree_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (notificationtree.SelectedNode != null && notificationtree.SelectedNode.Tag is Notification notification)
            {
                switch (notification)
                {
                    case AssemblyMissingNotification assemblyMissingNotification:
                        break;
                    case MethodMissingNotification methodMissingNotification:
                        MainForm.GotoHook(methodMissingNotification.Hook);
                        break;
                    case MethodChangedNotification methodChangedNotification:
                        MainForm.GotoHook(methodChangedNotification.Hook);
                        break;
                    case FieldModificationChangedNotification fieldChangedNotification:
                        MainForm.GotoModifier(fieldChangedNotification.Modifier);
                        break;
                    case MethodModificationChangedNotification methodModificationChangedNotification:
                        MainForm.GotoModifier(methodModificationChangedNotification.Modifier);
                        break;
                    case PropertyChangedNotification propertyChangedNotification:
                        MainForm.GotoModifier(propertyChangedNotification.Modifier);
                        break;
                    case NewFieldChangedNotification newFieldChangedNotification:
                        MainForm.GotoField(newFieldChangedNotification.Field);
                        break;
                }
            }
        }
    }
}
