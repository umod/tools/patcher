﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using uMod.Patcher.Hooks;

namespace uMod.Patcher.Views
{
    public partial class SimpleHookSettingsControl : HookSettingsControl
    {
        private bool ignorechanges;

        public SimpleHookSettingsControl()
        {
            InitializeComponent();
        }

        private int InjectionTypeToIndex(InjectionType type)
        {
            switch (type)
            {
                case InjectionType.Index:
                    return 2;

                case InjectionType.Pre:
                    return 0;

                case InjectionType.Post:
                    return 1;
            }

            return -1;
        }

        private InjectionType IndexToInjectionType(int selectedIndex)
        {
            switch (selectedIndex)
            {
                case 2:
                    return InjectionType.Index;

                case 0:
                    return InjectionType.Pre;

                case 1:
                    return InjectionType.Post;
            }

            return InjectionType.Index;
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            ReturnBehavior[] allreturnbehaviors = Enum.GetValues(typeof(ReturnBehavior)) as ReturnBehavior[];
            Array.Sort(allreturnbehaviors, (a, b) => Comparer<int>.Default.Compare((int)a, (int)b));
            for (int i = 0; i < allreturnbehaviors.Length; i++)
            {
                returnbehavior.Items.Add(allreturnbehaviors[i]);
            }

            ArgumentBehavior[] allargumentbehaviors = Enum.GetValues(typeof(ArgumentBehavior)) as ArgumentBehavior[];
            Array.Sort(allargumentbehaviors, (a, b) => Comparer<int>.Default.Compare((int)a, (int)b));
            for (int i = 0; i < allargumentbehaviors.Length; i++)
            {
                argumentbehavior.Items.Add(allargumentbehaviors[i]);
            }

            Simple hook = Hook as Simple;

            ignorechanges = true;
            if (hook.InjectionIndex > -1)
            {
                hook.InjectionType = InjectionType.Index;
                injectionindex.Value = hook.InjectionIndex;
                injectiontype.SelectedIndex = 2;
            }
            else
            {
                injectionindex.Value = -1;
                injectiontype.SelectedIndex = InjectionTypeToIndex(hook.InjectionType);
            }

            ToggleInjectionIndex();
            returnbehavior.SelectedIndex = (int)hook.ReturnBehavior;
            argumentbehavior.SelectedIndex = (int)hook.ArgumentBehavior;
            argumentstring.Text = string.IsNullOrEmpty(hook.ArgumentString) ? string.Empty : hook.ArgumentString;
            cmbOperatingSystem.SelectedIndex = (int)Hook.OperatingSystem;
            if (hook.IsDeprecated)
            {
                chkIsDeprecated.Checked = true;
                txtTargetHook.Text = hook.Deprecation.ReplacementHook;
                dtpRemovalDate.Value = hook.Deprecation.RemovalDate;
                HandleDeprecationFieldsVisibility(true);
            }
            ignorechanges = false;
        }

        private void ToggleInjectionIndex()
        {
            if (Hook is Simple simpleHook)
            {
                if (simpleHook.InjectionType == InjectionType.Index)
                {
                    injectionindex.Enabled = true;
                    injectionindex.Minimum = -1;
                }
                else
                {
                    injectionindex.Enabled = false;
                    injectionindex.Minimum = -1;
                    injectionindex.Value = -1;
                }
            }
        }

        private void injectionindex_ValueChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Simple hook = Hook as Simple;
            hook.InjectionIndex = (int)injectionindex.Value;
            NotifyChanges();
        }

        private void returnbehavior_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Simple hook = Hook as Simple;
            hook.ReturnBehavior = (ReturnBehavior)returnbehavior.SelectedIndex;
            NotifyChanges();
        }

        private void argumentbehavior_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Simple hook = Hook as Simple;
            hook.ArgumentBehavior = (ArgumentBehavior)argumentbehavior.SelectedIndex;
            NotifyChanges();
        }

        private void argumentstring_TextChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Simple hook = Hook as Simple;
            hook.ArgumentString = argumentstring.Text;
            NotifyChanges();
        }

        private void injectiontype_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Simple hook = Hook as Simple;

            hook.InjectionType = IndexToInjectionType(injectiontype.SelectedIndex);

            switch (hook.InjectionType)
            {
                case InjectionType.Index:
                    injectionindex.Enabled = true;
                    break;

                default:
                    injectionindex.Value = -1;
                    injectionindex.Enabled = false;
                    break;
            }

            NotifyChanges();
        }
        
        private void cmbOperatingSystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Hook.OperatingSystem = (HookOperatingSystem) cmbOperatingSystem.SelectedIndex;
            NotifyChanges();
        }

        private void chkIsDeprecated_CheckedChanged(object sender, EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            ignorechanges = true;

            CheckBox checkbox = sender as CheckBox;
            Simple hook = Hook as Simple;

            DateTime initialDeprecationDate = DateTime.Now.AddDays(60);
            dtpRemovalDate.Value = initialDeprecationDate;

            hook.Deprecation = checkbox.Checked ? new Simple.DeprecatedStatus
            {
                RemovalDate = initialDeprecationDate,
                ReplacementHook = txtTargetHook.Text
            } : null;
            HandleDeprecationFieldsVisibility(checkbox.Checked);

            ignorechanges = false;

            NotifyChanges();
        }

        private void HandleDeprecationFieldsVisibility(bool shouldShow)
        {
            lblTargetHook.Visible = shouldShow;
            txtTargetHook.Visible = shouldShow;
            lblRemovalDate.Visible = shouldShow;
            dtpRemovalDate.Visible = shouldShow;
        }

        private void txtTargetHook_TextChanged(object sender, EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Simple hook = Hook as Simple;
            hook.Deprecation.ReplacementHook = txtTargetHook.Text.Trim();

            NotifyChanges();
        }

        private void dtpRemovalDate_ValueChanged(object sender, EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Simple hook = Hook as Simple;
            hook.Deprecation.RemovalDate = dtpRemovalDate.Value;

            NotifyChanges();
        }
    }
}
