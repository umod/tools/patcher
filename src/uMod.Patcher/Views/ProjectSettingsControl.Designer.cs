﻿namespace uMod.Patcher
{
    partial class ProjectSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.settingsgroup = new System.Windows.Forms.GroupBox();
            this.savebutton = new System.Windows.Forms.Button();
            this.tablepanel = new System.Windows.Forms.TableLayoutPanel();
            this.selectfilenamebutton = new System.Windows.Forms.Button();
            this.selectdirectorybutton = new System.Windows.Forms.Button();
            this.directorytextbox = new System.Windows.Forms.TextBox();
            this.nametextbox = new System.Windows.Forms.TextBox();
            this.directorylabel = new System.Windows.Forms.Label();
            this.namelabel = new System.Windows.Forms.Label();
            this.manifestlabel = new System.Windows.Forms.Label();
            this.manifestlist = new System.Windows.Forms.ListView();
            this.filenamelabel = new System.Windows.Forms.Label();
            this.filenametextbox = new System.Windows.Forms.TextBox();
            this.manifestcontextmenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsgroup.SuspendLayout();
            this.tablepanel.SuspendLayout();
            this.manifestcontextmenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsgroup
            // 
            this.settingsgroup.Controls.Add(this.tablepanel);
            this.settingsgroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.settingsgroup.Location = new System.Drawing.Point(0, 0);
            this.settingsgroup.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.settingsgroup.Name = "settingsgroup";
            this.settingsgroup.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.settingsgroup.Size = new System.Drawing.Size(589, 227);
            this.settingsgroup.TabIndex = 1;
            this.settingsgroup.TabStop = false;
            this.settingsgroup.Text = "Project Settings";
            // 
            // savebutton
            // 
            this.savebutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.savebutton.Location = new System.Drawing.Point(10, 233);
            this.savebutton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.savebutton.Name = "savebutton";
            this.savebutton.Size = new System.Drawing.Size(124, 27);
            this.savebutton.TabIndex = 2;
            this.savebutton.Text = "Save Changes";
            this.savebutton.UseVisualStyleBackColor = true;
            this.savebutton.Click += new System.EventHandler(this.savebutton_Click);
            // 
            // tablepanel
            // 
            this.tablepanel.ColumnCount = 3;
            this.tablepanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.tablepanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablepanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tablepanel.Controls.Add(this.filenametextbox, 0, 2);
            this.tablepanel.Controls.Add(this.filenamelabel, 0, 2);
            this.tablepanel.Controls.Add(this.manifestlabel, 0, 3);
            this.tablepanel.Controls.Add(this.selectfilenamebutton, 2, 2);
            this.tablepanel.Controls.Add(this.directorylabel, 0, 1);
            this.tablepanel.Controls.Add(this.namelabel, 0, 0);
            this.tablepanel.Controls.Add(this.nametextbox, 1, 0);
            this.tablepanel.Controls.Add(this.selectdirectorybutton, 2, 1);
            this.tablepanel.Controls.Add(this.directorytextbox, 1, 1);
            this.tablepanel.Controls.Add(this.manifestlist, 1, 3);
            this.tablepanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablepanel.Location = new System.Drawing.Point(4, 19);
            this.tablepanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tablepanel.Name = "tablepanel";
            this.tablepanel.RowCount = 4;
            this.tablepanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tablepanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tablepanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tablepanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablepanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tablepanel.Size = new System.Drawing.Size(581, 205);
            this.tablepanel.TabIndex = 0;
            // 
            // selectfilenamebutton
            // 
            this.selectfilenamebutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectfilenamebutton.Enabled = false;
            this.selectfilenamebutton.Location = new System.Drawing.Point(547, 61);
            this.selectfilenamebutton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.selectfilenamebutton.Name = "selectfilenamebutton";
            this.selectfilenamebutton.Size = new System.Drawing.Size(30, 23);
            this.selectfilenamebutton.TabIndex = 8;
            this.selectfilenamebutton.Text = "...";
            this.selectfilenamebutton.UseVisualStyleBackColor = true;
            // 
            // selectdirectorybutton
            // 
            this.selectdirectorybutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectdirectorybutton.Enabled = false;
            this.selectdirectorybutton.Location = new System.Drawing.Point(547, 32);
            this.selectdirectorybutton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.selectdirectorybutton.Name = "selectdirectorybutton";
            this.selectdirectorybutton.Size = new System.Drawing.Size(30, 23);
            this.selectdirectorybutton.TabIndex = 4;
            this.selectdirectorybutton.Text = "...";
            this.selectdirectorybutton.UseVisualStyleBackColor = true;
            // 
            // directorytextbox
            // 
            this.directorytextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.directorytextbox.Location = new System.Drawing.Point(121, 32);
            this.directorytextbox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.directorytextbox.Name = "directorytextbox";
            this.directorytextbox.Size = new System.Drawing.Size(418, 23);
            this.directorytextbox.TabIndex = 5;
            // 
            // nametextbox
            // 
            this.tablepanel.SetColumnSpan(this.nametextbox, 2);
            this.nametextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nametextbox.Location = new System.Drawing.Point(121, 3);
            this.nametextbox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.nametextbox.MaxLength = 100;
            this.nametextbox.Name = "nametextbox";
            this.nametextbox.Size = new System.Drawing.Size(456, 23);
            this.nametextbox.TabIndex = 3;
            this.nametextbox.Text = "Untitled uMod Game";
            // 
            // directorylabel
            // 
            this.directorylabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.directorylabel.Location = new System.Drawing.Point(4, 29);
            this.directorylabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.directorylabel.Name = "directorylabel";
            this.directorylabel.Size = new System.Drawing.Size(109, 29);
            this.directorylabel.TabIndex = 2;
            this.directorylabel.Text = "Target Directory:";
            this.directorylabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // namelabel
            // 
            this.namelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.namelabel.Location = new System.Drawing.Point(4, 0);
            this.namelabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.namelabel.Name = "namelabel";
            this.namelabel.Size = new System.Drawing.Size(109, 29);
            this.namelabel.TabIndex = 0;
            this.namelabel.Text = "Name:";
            this.namelabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // manifestlabel
            // 
            this.manifestlabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manifestlabel.Location = new System.Drawing.Point(4, 88);
            this.manifestlabel.Margin = new System.Windows.Forms.Padding(4, 1, 4, 0);
            this.manifestlabel.Name = "manifestlabel";
            this.manifestlabel.Size = new System.Drawing.Size(109, 117);
            this.manifestlabel.TabIndex = 9;
            this.manifestlabel.Text = "Manifests:";
            // 
            // manifestlist
            // 
            this.tablepanel.SetColumnSpan(this.manifestlist, 2);
            this.manifestlist.ContextMenuStrip = this.manifestcontextmenu;
            this.manifestlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manifestlist.Location = new System.Drawing.Point(120, 90);
            this.manifestlist.MultiSelect = false;
            this.manifestlist.Name = "manifestlist";
            this.manifestlist.Size = new System.Drawing.Size(458, 112);
            this.manifestlist.TabIndex = 10;
            this.manifestlist.UseCompatibleStateImageBehavior = false;
            // 
            // filenamelabel
            // 
            this.filenamelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filenamelabel.Location = new System.Drawing.Point(4, 58);
            this.filenamelabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.filenamelabel.Name = "filenamelabel";
            this.filenamelabel.Size = new System.Drawing.Size(109, 29);
            this.filenamelabel.TabIndex = 11;
            this.filenamelabel.Text = "Location:";
            this.filenamelabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // filenametextbox
            // 
            this.filenametextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filenametextbox.Enabled = false;
            this.filenametextbox.Location = new System.Drawing.Point(121, 61);
            this.filenametextbox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.filenametextbox.Name = "filenametextbox";
            this.filenametextbox.Size = new System.Drawing.Size(418, 23);
            this.filenametextbox.TabIndex = 13;
            // 
            // manifestcontextmenu
            // 
            this.manifestcontextmenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.manifestcontextmenu.Name = "manifestcontextmenu";
            this.manifestcontextmenu.Size = new System.Drawing.Size(118, 48);
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.renameToolStripMenuItem.Text = "Rename";
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // ProjectSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.savebutton);
            this.Controls.Add(this.settingsgroup);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ProjectSettingsControl";
            this.Size = new System.Drawing.Size(589, 263);
            this.Load += new System.EventHandler(this.ProjectSettingsControl_Load);
            this.settingsgroup.ResumeLayout(false);
            this.tablepanel.ResumeLayout(false);
            this.tablepanel.PerformLayout();
            this.manifestcontextmenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox settingsgroup;
        private System.Windows.Forms.Button savebutton;
        private System.Windows.Forms.TableLayoutPanel tablepanel;
        private System.Windows.Forms.TextBox filenametextbox;
        private System.Windows.Forms.Label filenamelabel;
        private System.Windows.Forms.Label manifestlabel;
        private System.Windows.Forms.Button selectfilenamebutton;
        private System.Windows.Forms.Label directorylabel;
        private System.Windows.Forms.Label namelabel;
        private System.Windows.Forms.TextBox nametextbox;
        private System.Windows.Forms.Button selectdirectorybutton;
        private System.Windows.Forms.TextBox directorytextbox;
        private System.Windows.Forms.ListView manifestlist;
        private System.Windows.Forms.ContextMenuStrip manifestcontextmenu;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    }
}
