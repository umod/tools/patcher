﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using uMod.Patcher.Hooks;

namespace uMod.Patcher.Views
{
    public class CaptureGroupsControl : HookSettingsControl
    {
        public static class RenamePrompt
        {
            public static string ShowDialog(string text, string caption)
            {
                Form prompt = new Form()
                {
                    Width = 500,
                    Height = 170,
                    FormBorderStyle = FormBorderStyle.FixedDialog,
                    Text = caption,
                    StartPosition = FormStartPosition.CenterScreen
                };
                Label textLabel = new Label() { Left = 50, Top = 20, Text = text };
                TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 400 };
                Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 85, DialogResult = DialogResult.OK };
                confirmation.Click += (sender, e) => { prompt.Close(); };
                prompt.Controls.Add(textBox);
                prompt.Controls.Add(confirmation);
                prompt.Controls.Add(textLabel);
                prompt.AcceptButton = confirmation;

                return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
            }
        }

        public ListBox Capturegrouplist;
        private ContextMenuStrip captureGroupListContextMenu;
        private System.ComponentModel.IContainer components;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private ContextMenuStrip captureGroupItemsContextMenu;
        private ToolStripMenuItem moveUpToolStripMenuItem;
        private ToolStripMenuItem moveDownToolStripMenuItem;
        private ToolStripMenuItem deleteToolStripMenuItem1;
        private ToolStripMenuItem moveUpToolStripMenuItem1;
        private ToolStripMenuItem moveDownToolStripMenuItem1;
        public ListView Capturegroupitems;
        private ToolStripMenuItem renameToolStripMenuItem;
        private string selectedCaptureGroupName;

        public override void UpdateCaptureGroups()
        {
            if (Hook is Pattern patternHook)
            {
                Capturegrouplist.Items.Clear();

                if (patternHook.CaptureGroups != null)
                {
                    foreach (CaptureGroup captureGroup in patternHook.CaptureGroups)
                    {
                        Capturegrouplist.Items.Add(captureGroup.Name);
                    }
                }

                PopulateCaptureGroupItems();

                if (Capturegrouplist.Items.Count > 0 && Capturegrouplist.SelectedIndex < 0)
                    Capturegrouplist.SelectedIndex = 0;
            }
        }

        private void PopulateCaptureGroupItems()
        {
            if (!string.IsNullOrEmpty(selectedCaptureGroupName) && Hook is Pattern patternHook)
            {
                CaptureGroup captureGroup = patternHook.CaptureGroups.FirstOrDefault(x => x.Name.Equals(selectedCaptureGroupName, StringComparison.InvariantCultureIgnoreCase));

                Capturegroupitems.Items.Clear();
                if (captureGroup != null)
                {
                    foreach (string capturePattern in captureGroup.Patterns)
                    {
                        Capturegroupitems.Items.Add(capturePattern);
                    }
                }
            }
        }

        private void Capturegrouplist_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            selectedCaptureGroupName = Capturegrouplist.SelectedItem?.ToString();
            if (string.IsNullOrEmpty(selectedCaptureGroupName))
            {
                return;
            }

            PopulateCaptureGroupItems();
        }

        public CaptureGroupsControl()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Capturegrouplist = new System.Windows.Forms.ListBox();
            this.captureGroupListContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.moveUpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Capturegroupitems = new System.Windows.Forms.ListView();
            this.captureGroupItemsContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.captureGroupListContextMenu.SuspendLayout();
            this.captureGroupItemsContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // Capturegrouplist
            // 
            this.Capturegrouplist.ContextMenuStrip = this.captureGroupListContextMenu;
            this.Capturegrouplist.Dock = System.Windows.Forms.DockStyle.Left;
            this.Capturegrouplist.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Capturegrouplist.FormattingEnabled = true;
            this.Capturegrouplist.IntegralHeight = false;
            this.Capturegrouplist.ItemHeight = 16;
            this.Capturegrouplist.Location = new System.Drawing.Point(0, 0);
            this.Capturegrouplist.Name = "Capturegrouplist";
            this.Capturegrouplist.Size = new System.Drawing.Size(120, 227);
            this.Capturegrouplist.TabIndex = 0;
            this.Capturegrouplist.SelectedIndexChanged += new System.EventHandler(this.Capturegrouplist_SelectedIndexChanged);
            this.Capturegrouplist.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Capturegrouplist_KeyUp);
            this.Capturegrouplist.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Capturegrouplist_MouseDown);
            // 
            // captureGroupListContextMenu
            // 
            this.captureGroupListContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveUpToolStripMenuItem1,
            this.moveDownToolStripMenuItem1,
            this.renameToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.captureGroupListContextMenu.Name = "captureGroupListContextMenu";
            this.captureGroupListContextMenu.Size = new System.Drawing.Size(139, 92);
            this.captureGroupListContextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.captureGroupListContextMenu_ItemClicked);
            // 
            // moveUpToolStripMenuItem1
            // 
            this.moveUpToolStripMenuItem1.Name = "moveUpToolStripMenuItem1";
            this.moveUpToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.moveUpToolStripMenuItem1.Text = "Move Up";
            // 
            // moveDownToolStripMenuItem1
            // 
            this.moveDownToolStripMenuItem1.Name = "moveDownToolStripMenuItem1";
            this.moveDownToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.moveDownToolStripMenuItem1.Text = "Move Down";
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.renameToolStripMenuItem.Text = "Rename";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            // 
            // Capturegroupitems
            // 
            this.Capturegroupitems.ContextMenuStrip = this.captureGroupItemsContextMenu;
            this.Capturegroupitems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Capturegroupitems.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Capturegroupitems.GridLines = true;
            this.Capturegroupitems.Location = new System.Drawing.Point(120, 0);
            this.Capturegroupitems.Name = "Capturegroupitems";
            this.Capturegroupitems.Size = new System.Drawing.Size(358, 227);
            this.Capturegroupitems.TabIndex = 1;
            this.Capturegroupitems.UseCompatibleStateImageBehavior = false;
            this.Capturegroupitems.View = System.Windows.Forms.View.List;
            this.Capturegroupitems.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Capturegroupitems_KeyUp);
            // 
            // captureGroupItemsContextMenu
            // 
            this.captureGroupItemsContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveUpToolStripMenuItem,
            this.moveDownToolStripMenuItem,
            this.deleteToolStripMenuItem1});
            this.captureGroupItemsContextMenu.Name = "captureGroupItemsContextMenu";
            this.captureGroupItemsContextMenu.Size = new System.Drawing.Size(139, 70);
            this.captureGroupItemsContextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.captureGroupItemsContextMenu_ItemClicked);
            // 
            // moveUpToolStripMenuItem
            // 
            this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
            this.moveUpToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.moveUpToolStripMenuItem.Text = "Move Up";
            // 
            // moveDownToolStripMenuItem
            // 
            this.moveDownToolStripMenuItem.Name = "moveDownToolStripMenuItem";
            this.moveDownToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.moveDownToolStripMenuItem.Text = "Move Down";
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.deleteToolStripMenuItem1.Text = "Delete";
            // 
            // CaptureGroupsControl
            // 
            this.Controls.Add(this.Capturegroupitems);
            this.Controls.Add(this.Capturegrouplist);
            this.Name = "CaptureGroupsControl";
            this.Size = new System.Drawing.Size(478, 227);
            this.captureGroupListContextMenu.ResumeLayout(false);
            this.captureGroupItemsContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void captureGroupListContextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (string.IsNullOrEmpty(selectedCaptureGroupName))
            {
                return;
            }

            if (Hook is Pattern patternHook)
            {
                CaptureGroup selectedCaptureGroup;
                switch (e.ClickedItem.Text.ToLower())
                {
                    case "move up":
                        MoveListBoxItems(Capturegrouplist, MoveDirection.Up);
                        UpdateCaptureGroupOrder(Capturegrouplist);
                        NotifyChanges();
                        break;

                    case "move down":
                        MoveListBoxItems(Capturegrouplist, MoveDirection.Down);
                        UpdateCaptureGroupOrder(Capturegrouplist);
                        NotifyChanges();
                        break;

                    case "rename":
                        string newName = ShowRenamePrompt(Capturegrouplist);
                        if (!string.IsNullOrEmpty(newName) && Capturegrouplist.SelectedItem != null)
                        {
                            int index = Capturegrouplist.SelectedIndex;
                            string selected = Capturegrouplist.SelectedItem.ToString();

                            selectedCaptureGroup =
                                patternHook.CaptureGroups.FirstOrDefault(x => x.Name.Equals(selected, StringComparison.InvariantCultureIgnoreCase));

                            if (selectedCaptureGroup != null)
                            {
                                selectedCaptureGroup.Name = newName;
                                if (!string.IsNullOrEmpty(patternHook.CaptureGroupName) && patternHook.CaptureGroupName.Equals(selected,
                                    StringComparison.InvariantCultureIgnoreCase))
                                {
                                    patternHook.CaptureGroupName = newName;
                                }
                                Capturegrouplist.Items.Remove(Capturegrouplist.SelectedItem);
                                Capturegrouplist.Items.Insert(index, newName);
                                Capturegrouplist.SetSelected(index, true);
                                NotifyChanges();
                            }

                        }
                        break;

                    case "delete":
                        RemoveCaptureGroup(selectedCaptureGroupName);

                        break;
                }
            }
        }

        private void RemoveCaptureGroup(string group)
        {
            Pattern patternHook = Hook as Pattern;

            List<CaptureGroup> captureGroups = new List<CaptureGroup>(patternHook.CaptureGroups);
            CaptureGroup selectedCaptureGroup = captureGroups.FirstOrDefault(x =>
                x.Name.Equals(group, StringComparison.InvariantCultureIgnoreCase));

            if (selectedCaptureGroup != null)
            {
                captureGroups.Remove(selectedCaptureGroup);
                Capturegrouplist.Items.Remove(Capturegrouplist.SelectedItem);
                patternHook.CaptureGroups = captureGroups.ToArray();
                if (patternHook.CaptureGroupName == selectedCaptureGroup.Name)
                {
                    patternHook.CaptureGroupName = string.Empty;
                }

                if (Capturegrouplist.Items.Count > 0)
                {
                    Capturegrouplist.SelectedIndex = 0;
                    selectedCaptureGroupName = Capturegrouplist.Text;
                }

                Capturegroupitems.Items.Clear();
                NotifyChanges();
            }
        }

        private void captureGroupItemsContextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (Capturegroupitems.SelectedItems.Count == 0)
            {
                return;
            }

            if (Hook is Pattern patternHook)
            {
                switch (e.ClickedItem.Text.ToLower())
                {
                    case "move up":
                        MoveListViewItems(Capturegroupitems, MoveDirection.Up);
                        UpdateSelectedCaptureGroupPatternOrder(Capturegroupitems);
                        break;

                    case "move down":
                        MoveListViewItems(Capturegroupitems, MoveDirection.Down);
                        UpdateSelectedCaptureGroupPatternOrder(Capturegroupitems);
                        break;

                    case "delete":
                        if (string.IsNullOrEmpty(selectedCaptureGroupName))
                        {
                            return;
                        }
                        RemoveSelectedCaptureGroupItems();
                        break;
                }
            }
        }

        private void RemoveSelectedCaptureGroupItems()
        {
            Pattern patternHook = Hook as Pattern;

            CaptureGroup captureGroup = patternHook.CaptureGroups.FirstOrDefault(x =>
                x.Name.Equals(selectedCaptureGroupName, StringComparison.InvariantCultureIgnoreCase));
            if (captureGroup == null) return;

            foreach (ListViewItem selectedPatternItem in Capturegroupitems.SelectedItems)
            {
                List<string> patterns = new List<string>(captureGroup.Patterns);
                patterns.Remove(selectedPatternItem.Text);
                captureGroup.Patterns = patterns.ToArray();
                Capturegroupitems.Items.Remove(selectedPatternItem);
            }

            if (Capturegroupitems.Items.Count == 0)
            {
                RemoveCaptureGroup(captureGroup.Name);
            }

            NotifyChanges();
        }

        private enum MoveDirection { Up = -1, Down = 1 };

        private void MoveListViewItems(ListView sender, MoveDirection direction)
        {
            int dir = (int)direction;

            bool valid = sender.SelectedItems.Count > 0 &&
                         ((direction == MoveDirection.Down && (sender.SelectedItems[sender.SelectedItems.Count - 1].Index < sender.Items.Count - 1))
                          || (direction == MoveDirection.Up && (sender.SelectedItems[0].Index > 0)));

            if (!valid) return;

            foreach (ListViewItem item in sender.SelectedItems)
            {
                int index = item.Index + dir;
                sender.Items.RemoveAt(item.Index);
                sender.Items.Insert(index, item.Text);
            }
        }

        private string ShowRenamePrompt(ListBox sender)
        {
            if (sender.SelectedItem == null || sender.SelectedIndex < 0)
            {
                return string.Empty;
            }

            object selected = sender.SelectedItem;

            return RenamePrompt.ShowDialog("Specify new name:", $"Rename Capture Group: {selected.ToString()}");
        }

        private void MoveListBoxItems(ListBox sender, MoveDirection direction)
        {
            if (sender.SelectedItem == null || sender.SelectedIndex < 0)
            {
                return;
            }

            int newIndex = sender.SelectedIndex + (int)direction;

            if (newIndex < 0 || newIndex >= sender.Items.Count)
            {
                return;
            }

            object selected = sender.SelectedItem;

            sender.Items.Remove(selected);
            sender.Items.Insert(newIndex, selected);
            sender.SetSelected(newIndex, true);
        }

        private void UpdateCaptureGroupOrder(ListBox sender)
        {
            if (!(Hook is Pattern patternHook)) return;

            List<CaptureGroup> newCaptureGroups = new List<CaptureGroup>();

            foreach (var item in Capturegrouplist.Items)
            {
                CaptureGroup captureGroup = patternHook.CaptureGroups.FirstOrDefault(x =>
                    x.Name.Equals(item.ToString(), StringComparison.InvariantCultureIgnoreCase));
                if (captureGroup != null)
                {
                    newCaptureGroups.Add(captureGroup);
                }
            }

            patternHook.CaptureGroups = newCaptureGroups.ToArray();
            NotifyChanges();
        }

        private void UpdateSelectedCaptureGroupPatternOrder(ListView sender)
        {
            if (string.IsNullOrEmpty(selectedCaptureGroupName))
            {
                return;
            }

            if (!(Hook is Pattern patternHook)) return;

            CaptureGroup captureGroup = patternHook.CaptureGroups.FirstOrDefault(x =>
                x.Name.Equals(selectedCaptureGroupName, StringComparison.InvariantCultureIgnoreCase));
            if (captureGroup == null) return;
            List<string> patterns = new List<string>();
            foreach (ListViewItem item in sender.Items)
            {
                patterns.Add(item.Text);
            }

            captureGroup.Patterns = patterns.ToArray();
            NotifyChanges();
        }

        private void Capturegrouplist_MouseDown(object sender, MouseEventArgs e)
        {
            var clickedIndex = Capturegrouplist.IndexFromPoint(e.X, e.Y);
            if (Capturegrouplist.SelectedIndex == clickedIndex) return;
            Capturegrouplist.SelectedIndex = clickedIndex;
            selectedCaptureGroupName = Capturegrouplist.Text;
        }

        private void Capturegrouplist_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveCaptureGroup(selectedCaptureGroupName);
            }
        }

        private void Capturegroupitems_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveSelectedCaptureGroupItems();
            }
        }
    }
}
