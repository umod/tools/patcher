﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using uMod.Patcher.Hooks;

namespace uMod.Patcher.Views
{
    public partial class PatternHookSettingsControl : HookSettingsControl
    {
        private bool ignorechanges;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblOffsetIndex;
        private System.Windows.Forms.NumericUpDown offsetindex;
        private System.Windows.Forms.ComboBox returnbehavior;
        private System.Windows.Forms.ComboBox argumentbehavior;
        private System.Windows.Forms.TextBox argumentstring;
        private System.Windows.Forms.Label label5;
        private Label label1;
        private TableLayoutPanel tableLayoutPanel2;
        private ComboBox capturegroup;
        private ComboBox capturegroupprecedence;
        private ComboBox cmbOperatingSystem;

        public PatternHookSettingsControl()
        {
            InitializeComponent();
        }

        public override void UpdateCaptureGroups()
        {
            if (Hook is Pattern patternHook)
            {
                capturegroup.Items.Clear();
                if (patternHook.CaptureGroups != null)
                {
                    foreach (CaptureGroup captureGroup in patternHook.CaptureGroups)
                    {
                        capturegroup.Items.Add(captureGroup.Name);
                    }

                    if (capturegroup.Items.IndexOf(patternHook.CaptureGroupName) == -1)
                    {
                        capturegroup.SelectedIndex = -1;
                        capturegroup.SelectedItem = null;
                        capturegroup.Text = "";
                    }
                }
                NotifyChanges();
            }
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            ReturnBehavior[] allreturnbehaviors = Enum.GetValues(typeof(ReturnBehavior)) as ReturnBehavior[];
            Array.Sort(allreturnbehaviors, (a, b) => Comparer<int>.Default.Compare((int)a, (int)b));
            for (int i = 0; i < allreturnbehaviors.Length; i++)
            {
                returnbehavior.Items.Add(allreturnbehaviors[i]);
            }

            ArgumentBehavior[] allargumentbehaviors = Enum.GetValues(typeof(ArgumentBehavior)) as ArgumentBehavior[];
            Array.Sort(allargumentbehaviors, (a, b) => Comparer<int>.Default.Compare((int)a, (int)b));
            for (int i = 0; i < allargumentbehaviors.Length; i++)
            {
                argumentbehavior.Items.Add(allargumentbehaviors[i]);
            }

            Pattern hook = Hook as Pattern;

            ignorechanges = true;
            capturegroup.Enabled = true;
            capturegroupprecedence.Enabled = true;
            if (hook.OffsetIndex > -1)
            {
                offsetindex.Value = hook.OffsetIndex;
            }
            else
            {
                offsetindex.Value = -1;
            }

            if (!string.IsNullOrEmpty(hook.CaptureGroupName))
            {
                UpdateCaptureGroups();
                capturegroup.SelectedItem = hook.CaptureGroupName;

                switch (hook.CaptureGroupPosition)
                {
                    case CaptureGroupPosition.Before:
                        capturegroup.Show();
                        offsetindex.Hide();
                        lblOffsetIndex.Hide();
                        capturegroupprecedence.SelectedIndex = 0;
                        break;

                    case CaptureGroupPosition.After:
                        capturegroup.Show();
                        offsetindex.Hide();
                        lblOffsetIndex.Hide();
                        capturegroupprecedence.SelectedIndex = 1;
                        break;

                    case CaptureGroupPosition.Offset:
                        capturegroup.Show();
                        offsetindex.Show();
                        lblOffsetIndex.Show();
                        capturegroupprecedence.SelectedIndex = 2;
                        break;
                }
            }
            else
            {
                capturegroup.Hide();
                offsetindex.Hide();
                lblOffsetIndex.Hide();
            }

            returnbehavior.SelectedIndex = (int)hook.ReturnBehavior;
            argumentbehavior.SelectedIndex = (int)hook.ArgumentBehavior;
            argumentstring.Text = string.IsNullOrEmpty(hook.ArgumentString) ? string.Empty : hook.ArgumentString;
            cmbOperatingSystem.SelectedIndex = (int)Hook.OperatingSystem;
            ignorechanges = false;
        }

        private void offsetindex_ValueChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Pattern hook = Hook as Pattern;
            hook.OffsetIndex = (int)offsetindex.Value;
            NotifyChanges();
        }

        private void returnbehavior_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Pattern hook = Hook as Pattern;
            hook.ReturnBehavior = (ReturnBehavior)returnbehavior.SelectedIndex;
            NotifyChanges();
        }

        private void argumentbehavior_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Pattern hook = Hook as Pattern;
            hook.ArgumentBehavior = (ArgumentBehavior)argumentbehavior.SelectedIndex;
            NotifyChanges();
        }

        private void argumentstring_TextChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Pattern hook = Hook as Pattern;
            hook.ArgumentString = argumentstring.Text;
            NotifyChanges();
        }

        private void capturegroupprecedence_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Pattern hook = Hook as Pattern;
            switch (capturegroupprecedence.SelectedIndex)
            {
                case 0:
                    hook.CaptureGroupPosition = CaptureGroupPosition.Before;
                    capturegroup.Show();
                    offsetindex.Hide();
                    lblOffsetIndex.Hide();
                    break;

                case 1:
                    hook.CaptureGroupPosition = CaptureGroupPosition.After;
                    capturegroup.Show();
                    offsetindex.Hide();
                    lblOffsetIndex.Hide();
                    break;

                case 2:
                    hook.CaptureGroupPosition = CaptureGroupPosition.Offset;
                    capturegroup.Show();
                    offsetindex.Show();
                    lblOffsetIndex.Show();
                    break;
            }

            NotifyChanges();
        }

        private void capturegroup_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Pattern hook = Hook as Pattern;

            string captureGroupName = capturegroup.Items[capturegroup.SelectedIndex].ToString();
            hook.CaptureGroupName = captureGroupName;
            NotifyChanges();
        }

        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOffsetIndex = new System.Windows.Forms.Label();
            this.offsetindex = new System.Windows.Forms.NumericUpDown();
            this.returnbehavior = new System.Windows.Forms.ComboBox();
            this.argumentbehavior = new System.Windows.Forms.ComboBox();
            this.argumentstring = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.capturegroup = new System.Windows.Forms.ComboBox();
            this.capturegroupprecedence = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbOperatingSystem = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offsetindex)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblOffsetIndex, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.offsetindex, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.returnbehavior, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.argumentbehavior, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.argumentstring, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.cmbOperatingSystem, 1, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(344, 156);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 26);
            this.label2.TabIndex = 9;
            this.label2.Text = "Argument String:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 26);
            this.label6.TabIndex = 5;
            this.label6.Text = "Argument Behavior:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Return Behavior:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOffsetIndex
            // 
            this.lblOffsetIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOffsetIndex.Location = new System.Drawing.Point(3, 26);
            this.lblOffsetIndex.Name = "lblOffsetIndex";
            this.lblOffsetIndex.Size = new System.Drawing.Size(114, 26);
            this.lblOffsetIndex.TabIndex = 0;
            this.lblOffsetIndex.Text = "Offset Index:";
            this.lblOffsetIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // offsetindex
            // 
            this.offsetindex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.offsetindex.Location = new System.Drawing.Point(123, 29);
            this.offsetindex.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.offsetindex.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.offsetindex.Name = "offsetindex";
            this.offsetindex.Size = new System.Drawing.Size(218, 23);
            this.offsetindex.TabIndex = 2;
            this.offsetindex.ValueChanged += new System.EventHandler(this.offsetindex_ValueChanged);
            // 
            // returnbehavior
            // 
            this.returnbehavior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.returnbehavior.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.returnbehavior.FormattingEnabled = true;
            this.returnbehavior.Location = new System.Drawing.Point(123, 55);
            this.returnbehavior.Name = "returnbehavior";
            this.returnbehavior.Size = new System.Drawing.Size(218, 23);
            this.returnbehavior.TabIndex = 3;
            this.returnbehavior.SelectedIndexChanged += new System.EventHandler(this.returnbehavior_SelectedIndexChanged);
            // 
            // argumentbehavior
            // 
            this.argumentbehavior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.argumentbehavior.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.argumentbehavior.FormattingEnabled = true;
            this.argumentbehavior.Location = new System.Drawing.Point(123, 81);
            this.argumentbehavior.Name = "argumentbehavior";
            this.argumentbehavior.Size = new System.Drawing.Size(218, 23);
            this.argumentbehavior.TabIndex = 4;
            this.argumentbehavior.SelectedIndexChanged += new System.EventHandler(this.argumentbehavior_SelectedIndexChanged);
            // 
            // argumentstring
            // 
            this.argumentstring.Dock = System.Windows.Forms.DockStyle.Fill;
            this.argumentstring.Location = new System.Drawing.Point(123, 107);
            this.argumentstring.Name = "argumentstring";
            this.argumentstring.Size = new System.Drawing.Size(218, 23);
            this.argumentstring.TabIndex = 5;
            this.argumentstring.TextChanged += new System.EventHandler(this.argumentstring_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 26);
            this.label5.TabIndex = 13;
            this.label5.Text = "Injection Position:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel2.Controls.Add(this.capturegroup, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.capturegroupprecedence, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(120, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(224, 26);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // capturegroup
            // 
            this.capturegroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.capturegroup.Location = new System.Drawing.Point(77, 3);
            this.capturegroup.Name = "capturegroup";
            this.capturegroup.Size = new System.Drawing.Size(144, 23);
            this.capturegroup.TabIndex = 1;
            this.capturegroup.SelectedIndexChanged += new System.EventHandler(this.capturegroup_SelectedIndexChanged);
            // 
            // capturegroupprecedence
            // 
            this.capturegroupprecedence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.capturegroupprecedence.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.capturegroupprecedence.FormattingEnabled = true;
            this.capturegroupprecedence.Items.AddRange(new object[] {
            "Before",
            "After",
            "Offset"});
            this.capturegroupprecedence.Location = new System.Drawing.Point(3, 3);
            this.capturegroupprecedence.Name = "capturegroupprecedence";
            this.capturegroupprecedence.Size = new System.Drawing.Size(68, 23);
            this.capturegroupprecedence.TabIndex = 0;
            this.capturegroupprecedence.SelectedIndexChanged += new System.EventHandler(this.capturegroupprecedence_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 26);
            this.label1.TabIndex = 15;
            this.label1.Text = "Operating System:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbOperatingSystem
            // 
            this.cmbOperatingSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbOperatingSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOperatingSystem.FormattingEnabled = true;
            this.cmbOperatingSystem.Items.AddRange(new object[] {
            "Both",
            "Windows-Only",
            "Linux-Only"});
            this.cmbOperatingSystem.Location = new System.Drawing.Point(123, 133);
            this.cmbOperatingSystem.Name = "cmbOperatingSystem";
            this.cmbOperatingSystem.Size = new System.Drawing.Size(218, 23);
            this.cmbOperatingSystem.TabIndex = 6;
            this.cmbOperatingSystem.SelectedIndexChanged += new System.EventHandler(this.cmbOperatingSystem_SelectedIndexChanged);
            // 
            // PatternHookSettingsControl
            // 
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PatternHookSettingsControl";
            this.Size = new System.Drawing.Size(415, 198);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offsetindex)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void cmbOperatingSystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Hook.OperatingSystem = (HookOperatingSystem) cmbOperatingSystem.SelectedIndex;
            NotifyChanges();
        }
    }
}
