﻿using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using Menees.Diffs;
using Menees.Diffs.Windows.Forms;
using Menees.Windows.Forms;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using uMod.Patcher.Exceptions;
using uMod.Patcher.Hooks;
using uMod.Patcher.Patching;

namespace uMod.Patcher.Views
{
    public partial class HookViewControl : UserControl, IPatchView
    {
        /// <summary>
        /// Gets or sets the hook to use
        /// </summary>
        public Hook Hook { get; set; }

        /// <summary>
        /// Gets or sets the main patcher form
        /// </summary>
        public PatcherForm MainForm { get; set; }

        public Button FlagButton { get; set; }

        public Button UnflagButton { get; set; }

        public bool IsChanged => applybutton?.Enabled ?? false;

        public TabPage Tab { get; set; }

        private List<Type> hooktypes;

        private TextEditorControl msilbefore, msilafter, codebefore, codeafter;

        private DiffControl codediff;

        private MethodDefinition methoddef;

        private bool ignoretypechange;

        private CaptureGroupsControl capturegroups;

        private HookSettingsControl settingsview;

        public HookViewControl()
        {
            InitializeComponent();
            FlagButton = flagbutton;
            UnflagButton = unflagbutton;
        }

        protected override async void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            methoddef = MainForm.GetMethod(Hook.AssemblyName, Hook.TypeName, Hook.Signature);

            hooktypes = new List<Type>();
            int selindex = 0;
            int i = 0;
            foreach (Type hooktype in Hook.GetHookTypes())
            {
                string typename = hooktype.GetCustomAttribute<HookType>().Name;
                hooktypedropdown.Items.Add(typename);
                hooktypes.Add(hooktype);
                if (typename == Hook.HookTypeName)
                {
                    selindex = i;
                }

                i++;
            }

            List<Hook> hooks = Project.Current.GetManifestByAssembly(Hook.AssemblyName).Hooks;
            List<Hook> baseHooks = (from hook in hooks where hook.BaseHook != null select hook.BaseHook).ToList();
            BaseHookDropdown.Items.Add("");
            int selindex2 = 0;
            i = 1;
            foreach (Hook hook in hooks)
            {
                if (hook.BaseHook == Hook)
                {
                    clonebutton.Enabled = false;
                }

                if (hook != Hook.BaseHook && baseHooks.Contains(hook))
                {
                    continue;
                }

                BaseHookDropdown.Items.Add(hook.Name);
                if (hook == Hook.BaseHook)
                {
                    selindex2 = i;
                }

                i++;
            }

            if (!string.IsNullOrEmpty(Hook.BaseHookName))
            {
                BaseHookSearchButton.Enabled = true;
            }
            else
            {
                BaseHookSearchButton.Enabled = false;
            }

            assemblytextbox.Text = Hook.AssemblyName;
            assemblytextbox.Select(0, 0);
            typenametextbox.Text = Hook.TypeName;

            if (methoddef != null)
            {
                methodnametextbox.Text = Hook.Signature.ToString();
            }
            else
            {
                methodnametextbox.Text = Hook.Signature + " (METHOD MISSING)";
            }

            nametextbox.Text = Hook.Name;
            hooknametextbox.Text = Hook.HookName;
            ignoretypechange = true;
            hooktypedropdown.SelectedIndex = selindex;
            BaseHookDropdown.SelectedIndex = selindex2;
            ignoretypechange = false;

            if (Hook.Flagged)
            {
                flagbutton.Enabled = false;
                unflagbutton.Enabled = true;
                unflagbutton.Focus();
            }
            else
            {
                flagbutton.Enabled = true;
                unflagbutton.Enabled = false;
                flagbutton.Focus();
            }

            switch (Hook)
            {
                case Initialize initialize:
                    settingsview = new InitializeHookSettingsControl { Hook = initialize };
                    tabview.TabPages.Remove(capturegroupstab);
                    break;

                case Pattern patternHook:
                    settingsview = new PatternHookSettingsControl { Hook = patternHook };
                    break;

                case Simple simpleHook:
                    settingsview = new SimpleHookSettingsControl { Hook = simpleHook };
                    tabview.TabPages.Remove(capturegroupstab);
                    break;

                case Modify modifyHook:
                    settingsview = new ModifyHookSettingsControl { Hook = modifyHook };
                    tabview.TabPages.Remove(capturegroupstab);
                    break;
            }

            if (settingsview == null)
            {
                Label tmp = new Label
                {
                    TextAlign = ContentAlignment.MiddleCenter,
                    AutoSize = false,
                    Text = "No settings.",
                    Dock = DockStyle.Fill
                };
                hooksettingstab.Controls.Add(tmp);
            }
            else
            {
                settingsview.Dock = DockStyle.Fill;
                settingsview.OnSettingsChanged += settingsview_OnSettingsChanged;
                hooksettingstab.Controls.Add(settingsview);
            }

            capturegroups = new CaptureGroupsControl()
            {
                Dock = DockStyle.Fill,
                Hook = Hook
            };

            capturegroups.OnSettingsChanged += capturegroups_OnSettingsChanged;

            capturegroupstab.Controls.Add(capturegroups);
            UpdateCaptureGroups();

            if (methoddef == null)
            {
                Label missinglabel1 = new Label();
                missinglabel1.Dock = DockStyle.Fill;
                missinglabel1.AutoSize = false;
                missinglabel1.Text = "METHOD MISSING";
                missinglabel1.TextAlign = ContentAlignment.MiddleCenter;
                beforetab.Controls.Add(missinglabel1);

                Label missinglabel2 = new Label();
                missinglabel2.Dock = DockStyle.Fill;
                missinglabel2.AutoSize = false;
                missinglabel2.Text = "METHOD MISSING";
                missinglabel2.TextAlign = ContentAlignment.MiddleCenter;
                aftertab.Controls.Add(missinglabel2);

                return;
            }

            HandlePatch();

            MarkApplied();
        }

        public async void HandlePatch()
        {
            IlWeaver weaver = new IlWeaver(methoddef.Body) { Module = methoddef.Module };
            Font font = new Font(FontFamily.GenericMonospace, 10.0f);

            Hook.PreparePatch(methoddef, weaver, MainForm.CoreAssembly);

            string msilbeforeText = weaver.ToString();
            string srcbeforeText = await Decompiler.GetSourceCode(methoddef, weaver);

            codebefore = new TextEditorControl
            {
                Font = font,
                Dock = DockStyle.Fill,
                Text = srcbeforeText,
                Document = { HighlightingStrategy = HighlightingManager.Manager.FindHighlighter("C#") },
                IsReadOnly = true
            };

            try
            {
                Hook.ApplyPatch(methoddef, weaver, MainForm.CoreAssembly);
                MainForm?.ClearStatus();
            }
            catch (PatchInvalidException ex)
            {
                MainForm?.SetError($"{ex.Header}: {ex.Text}");
                //MessageBox.Show(ex.Text, ex.Header, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            msilafter = new TextEditorControl
            {
                Font = font,
                Dock = DockStyle.Fill,
                Text = weaver.ToString(),
                IsReadOnly = true
            };
            string srcafterText = await Decompiler.GetSourceCode(methoddef, weaver);
            codeafter = new TextEditorControl
            {
                Font = font,
                Dock = DockStyle.Fill,
                Text = srcafterText,
                Document = { HighlightingStrategy = HighlightingManager.Manager.FindHighlighter("C#") },
                IsReadOnly = true
            };

            msilbefore = new TextEditorControl
            {
                Font = font,
                Dock = DockStyle.Fill,
                Text = msilbeforeText,
                IsReadOnly = true
            };

            codediff = new DiffControl()
            {
                Font = font, Dock = DockStyle.Fill,
            };

            var beforeList = srcbeforeText.Split("\n");
            var afterList = srcafterText.Split("\n");

            TextDiff diff = new TextDiff(HashType.Crc32, false, true, 0, false);
			EditScript script = diff.Execute(beforeList, afterList);
            codediff.SetData(beforeList, afterList, script);

            codediff.UseTranslucentOverview = true;
            codediff.ShowColorLegend = false;
            codediff.LineDiffHeight = 0;
            codediff.Margin = Padding.Empty;
            codediff.ShowToolBar = false;

            msilbefore.ActiveTextAreaControl.TextArea.MouseClick += TextAreaOnMouseClick;

            beforetab.Controls.Clear();
            beforetab.Controls.Add(msilbefore);

            aftertab.Controls.Clear();
            aftertab.Controls.Add(msilafter);

            codebeforetab.Controls.Clear();
            codebeforetab.Controls.Add(codebefore);

            codeaftertab.Controls.Clear();
            codeaftertab.Controls.Add(codeafter);

            codedifftab.Controls.Clear();
            codedifftab.Controls.Add(codediff);
        }

        private void ContextMenuStripOnItemClicked(Pattern hook, CaptureGroup captureGroup = null)
        {
            if (hook.CaptureGroups == null)
            {
                hook.CaptureGroups = new CaptureGroup[0];
            }
            List<CaptureGroup> captureGroupList = new List<CaptureGroup>(hook.CaptureGroups);

            if (captureGroup == null)
            {
                captureGroupList.Add(captureGroup = new CaptureGroup()
                {
                    Name = "Group" + (captureGroupList.Count + 1),
                });
                hook.CaptureGroups = captureGroupList.ToArray();
            }

            if (!msilbefore.ActiveTextAreaControl.SelectionManager.HasSomethingSelected)
            {
                return;
            }

            ISelection selection = msilbefore.ActiveTextAreaControl.SelectionManager.SelectionCollection.FirstOrDefault();
            if (selection == null)
            {
                return;
            }

            IlWeaver weaver = new IlWeaver(methoddef.Body) { Module = methoddef.Module };
            Hook.PreparePatch(methoddef, weaver, MainForm.CoreAssembly);

            if (captureGroup.Patterns == null)
            {
                captureGroup.Patterns = new string[0];
            }

            int start = selection.StartPosition.Line;
            int end = selection.EndPosition.Line;

            List<string> patterns = new List<string>(captureGroup.Patterns);

            for (int line = start; line <= end; line++)
            {
                Instruction instruction = weaver.Instructions[line];
                string instructionStr;
                if (instruction.OpCode.FlowControl is FlowControl.Next or FlowControl.Call or FlowControl.Return)
                {
                    int instructionOffsetLength = $"IL_{instruction.Offset:X4} :".Length;
                    instructionStr = instruction.ToString()[instructionOffsetLength..];
                }
                else
                {
                    instructionStr = instruction.OpCode.Name;
                }
                patterns.Add(instructionStr);
            }

            captureGroup.Patterns = patterns.ToArray();
            UpdateCaptureGroups();
        }

        public void UpdateCaptureGroups()
        {
            capturegroups.UpdateCaptureGroups();
            settingsview.UpdateCaptureGroups();
        }

        private void TextAreaOnMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && Hook is Pattern patternHook)
            {
                InitializeContextMenuStrip();
                contextMenuStrip1.Show(msilbefore.ActiveTextAreaControl.TextArea, e.Location);
            }
        }

        private void InitializeContextMenuStrip()
        {
            if (Hook is Pattern patternHook)
            {
                ToolStripItem toolItem;
                contextMenuStrip1.Items.Clear();
                if (patternHook.CaptureGroups != null)
                {
                    foreach (CaptureGroup group in patternHook.CaptureGroups)
                    {
                        toolItem = contextMenuStrip1.Items.Add("Add to " + group.Name);
                        toolItem.Click += delegate { ContextMenuStripOnItemClicked(patternHook, group); };
                    }
                }

                toolItem = contextMenuStrip1.Items.Add("Add to new group");
                toolItem.Click += delegate
                {
                    ContextMenuStripOnItemClicked(patternHook);
                };
            }
        }

        private void capturegroups_OnSettingsChanged(HookSettingsControl obj)
        {
            UpdateCaptureGroups();
            MarkChanged();
        }

        private void settingsview_OnSettingsChanged(HookSettingsControl obj)
        {
            MarkChanged();
        }

        private void deletebutton_Click(object sender, System.EventArgs e)
        {
            MainForm?.TryRemovePatch(Hook);
        }

        private void flagbutton_Click(object sender, System.EventArgs e)
        {
            Hook.Flagged = true;
            MainForm.UpdateHook(Hook, false);
            flagbutton.Enabled = false;
            unflagbutton.Enabled = true;
        }

        private void unflagbutton_Click(object sender, System.EventArgs e)
        {
            Hook.Flagged = false;
            MainForm.UpdateHook(Hook, false);
            if (Hook.Flagged)
            {
                return;
            }

            flagbutton.Enabled = true;
            unflagbutton.Enabled = false;
        }

        private void hooktypedropdown_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (hooktypedropdown.SelectedIndex < 0)
            {
                return;
            }

            if (ignoretypechange)
            {
                return;
            }

            Type t = hooktypes[hooktypedropdown.SelectedIndex];
            if (t == null || t == Hook.GetType())
            {
                return;
            }

            DialogResult result = MessageBox.Show(MainForm, "Are you sure you want to change the type of this hook? Any hook settings will be lost.", "uMod Patcher", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                IEnumerable<Hook> clonedHooks = MainForm.GetHooksWithBaseHook(Hook);

                Hook newhook = Activator.CreateInstance(t) as Hook;
                newhook.Name = Hook.Name;
                newhook.HookName = Hook.HookName;
                newhook.AssemblyName = Hook.AssemblyName;
                newhook.TypeName = Hook.TypeName;
                newhook.Signature = Hook.Signature;
                newhook.Flagged = Hook.Flagged;
                newhook.MSILHash = Hook.MSILHash;
                newhook.BaseHook = Hook.BaseHook;
                newhook.BaseHookName = Hook.BaseHookName;
                newhook.HookCategory = Hook.HookCategory;
                foreach (Hook clone in clonedHooks)
                    clone.BaseHook = newhook;

                MainForm.AddHook(newhook);
                MainForm.RemoveHook(Hook);
                MainForm.GotoHook(newhook);
            }
            else
            {
                hooktypedropdown.SelectedIndex = hooktypes.IndexOf(Hook.GetType());
            }
        }

        private void nametextbox_TextChanged(object sender, System.EventArgs e)
        {
            MarkChanged();
        }

        private void hooknametextbox_TextChanged(object sender, System.EventArgs e)
        {
            MarkChanged();
        }

        private void typeSearchButton_Click(object sender, EventArgs e)
        {
            Mono.Cecil.TypeDefinition typeDef = MainForm?.GetTypeDefinition(Hook.TypeName);
            if (typeDef != null)
            {
                MainForm?.GotoType(typeDef);
                return;
            }

            MainForm?.Search(Hook.TypeName);
        }

        private void baseHookSearchButton_Click(object sender, EventArgs e)
        {
            Hook hookTarget = MainForm?.GetHook(Hook.BaseHookName);
            if (hookTarget != null)
            {
                MainForm?.GotoHook(Hook.BaseHook);
                return;
            }

            MainForm?.Search(Hook.BaseHookName);
        }

        private void MethodSelectButton_Click(object sender, EventArgs e)
        {
            Mono.Cecil.TypeDefinition typeDef = MainForm?.GetTypeDefinition(Hook.TypeName);

            if (typeDef == null)
            {
                MainForm?.SetError($"No type found: {Hook.TypeName}");
                return;
            }

            TypeMethodPrompt typeMethodPrompt = new TypeMethodPrompt
            {
                StartPosition = FormStartPosition.CenterParent,
                MainForm = MainForm,
                TypeDef = typeDef,
                OnMemberSelected = MethodSelected,
                MemberFilter = FormUtility.MemberFilter.Methods
            };
            typeMethodPrompt.ShowDialog(this);
        }

        private void MethodSelected(TypeMethodPrompt prompt)
        {
            Hook.Signature = Utility.GetMethodSignature(prompt.SelectedMethodDefinition);
            methodnametextbox.Text = Hook.Signature.ToString();
            Apply();
        }

        private void applybutton_Click(object sender, System.EventArgs e)
        {
            Apply();
        }

        private void tabview_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Hook is Pattern patternHook)
            {
                if (!string.IsNullOrWhiteSpace(patternHook.CaptureGroupName) && capturegroups.Capturegrouplist.Items.IndexOf(patternHook.CaptureGroupName) == -1)
                {
                    patternHook.CaptureGroupName = string.Empty;
                    UpdateCaptureGroups();
                }
            }
        }

        public async void Apply()
        {
            Hook.Name = nametextbox.Text.Trim();
            Hook.HookName = Regex.Replace(hooknametextbox.Text.Trim(), " ", "");

            MainForm.UpdateHook(Hook, false);

            if (msilbefore != null && msilafter != null)
            {
                IlWeaver weaver = new IlWeaver(methoddef.Body) { Module = methoddef.Module };

                Hook.PreparePatch(methoddef, weaver, MainForm.CoreAssembly);
                msilbefore.Text = weaver.ToString();
                codebefore.Text = await Decompiler.GetSourceCode(methoddef, weaver);

                try
                {
                    Hook.ApplyPatch(methoddef, weaver, MainForm.CoreAssembly);
                }
                catch (PatchInvalidException ex)
                {
                    MainForm?.SetError($"{ex.Header}: {ex.Text}");
                    //MessageBox.Show(ex.Text, ex.Header, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                msilafter.Text = weaver.ToString();
                codeafter.Text = await Decompiler.GetSourceCode(methoddef, weaver);
            }

            MarkApplied();
        }

        private void MarkApplied()
        {
            applybutton.Enabled = false;
            if ((Tab?.Text?.EndsWith("*") ?? false))
            {
                Tab.Text = Tab.Text.TrimEnd('*').Trim();
            }
        }

        private void basehookdropdown_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (BaseHookDropdown.SelectedIndex < 0)
            {
                return;
            }

            if (ignoretypechange)
            {
                return;
            }

            string hookName = (string)BaseHookDropdown.SelectedItem;
            if (string.IsNullOrWhiteSpace(hookName))
            {
                Hook.BaseHook = null;
                return;
            }
            List<Hook> hooks = Project.Current.GetManifestByAssembly(Hook.AssemblyName).Hooks;
            foreach (Hook hook in hooks)
            {
                if (hook.Name.Equals(hookName))
                {
                    Hook.BaseHook = hook;
                    break;
                }
            }
            if (!Hook.BaseHook.Name.Equals(hookName))
            {
                MessageBox.Show(MainForm, "Base Hook not found!", "uMod Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clonebutton_Click(object sender, System.EventArgs e)
        {
            MainForm?.CloneHook(Hook);
            clonebutton.Enabled = false;
        }

        public void MarkChanged()
        {
            applybutton.Enabled = true;
            if(!(Tab?.Text?.EndsWith("*") ?? true))
            {
                Tab.Text += " *";
            }
        }
    }
}
