﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Mono.Cecil;
using uMod.Patcher.Fields;
using uMod.Patcher.Hooks;
using uMod.Patcher.Modifiers;

namespace uMod.Patcher.Views
{
    public partial class SearchViewControl : UserControl
    {
        public PatcherForm MainForm;

        public SearchViewControl()
        {
            InitializeComponent();
        }

        private readonly Dictionary<int, object> searchResults = new Dictionary<int, object>();

        ListViewItem CreateSearchItem(string type, string name, string category)
        {
            ListViewItem listViewItem = new ListViewItem()
            {
                Text = type
            };

            listViewItem.SubItems.Add(name);
            listViewItem.SubItems.Add(category);

            return listViewItem;
        }

        public void RemoveSearchItem<T>(T field) where T : class
        {
            int removeIndex = -1;
            foreach (KeyValuePair<int, object> kvp in searchResults)
            {
                if (kvp.Value is T searchField && searchField == field)
                {
                    removeIndex = kvp.Key;
                    searchResultsListView.Items[kvp.Key].Remove();
                    break;
                }
            }

            if (removeIndex > -1)
            {
                searchResults.Remove(removeIndex);
            }
        }

        private bool IsMatch(string searchText, params string[] input)
        {
            string search = "^.*" + Regex.Escape(searchText)
                                    .Replace(@"\*", ".*")
                                    .Replace(@"\?", ".")
                                + ".*$";

            foreach (string inputItem in input)
            {
                if (string.IsNullOrEmpty(inputItem))
                {
                    continue;
                }
                if (Regex.IsMatch(inputItem, search, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant))
                {
                    return true;
                }
            }

            return false;
        }

        public void Search(string searchText)
        {
            searchResultsListView.Items.Clear();
            searchResults.Clear();

            if (Project.Current == null)
            {
                ListViewItem listViewItem = new ListViewItem()
                {
                    Text = "Error",
                };
                listViewItem.SubItems.Add("No project open");

                searchResultsListView.Items.Add(listViewItem);
                return;
            }

            if (searchText.Length < 3)
            {
                ListViewItem listViewItem = new ListViewItem()
                {
                    Text = "Error",
                };

                listViewItem.SubItems.Add("Your search query must be at least 3 characters in length.");

                searchResultsListView.Items.Add(listViewItem);
                return;
            }

            MainForm?.SetStatus("Searching...");

            List<Manifest> manifests = Project.Current.Manifests;
            int count = 0;

            foreach (Manifest manifest in manifests)
            {
                foreach (Field field in manifest.Fields)
                {
                    if(IsMatch(searchText, field.Name, field.TypeName))
                    {
                        ListViewItem listViewItem = CreateSearchItem("Field", $"{field.TypeName}::{field.Name}", Path.GetFileNameWithoutExtension(field.AssemblyName));

                        searchResultsListView.Items.Add(listViewItem);
                        searchResults.Add(listViewItem.Index, field);
                        count++;
                    }
                }

                foreach (Hook hook in manifest.Hooks)
                {
                    if(IsMatch(searchText, hook.Name, hook.HookCategory, hook.TypeName))
                    {
                        string prefix = !string.IsNullOrEmpty(hook.HookCategory)
                            ? $"{hook.HookCategory}."
                            : string.Empty;
                        ListViewItem listViewItem = CreateSearchItem("Hook", $"{hook.TypeName}::{prefix}{hook.Name}", Path.GetFileNameWithoutExtension(hook.AssemblyName));

                        searchResultsListView.Items.Add(listViewItem);
                        searchResults.Add(listViewItem.Index, hook);
                        count++;
                    }
                }

                foreach (Modifier modifier in manifest.Modifiers)
                {
                    string modifierName = modifier.Name;
                    int definePosition = modifierName.IndexOf("::", StringComparison.InvariantCultureIgnoreCase);
                    if (definePosition >= 0)
                    {
                        modifierName = modifierName.Substring(definePosition+2);
                    }

                    if(IsMatch(searchText, modifierName, modifier.TypeName))
                    {
                        ListViewItem listViewItem = CreateSearchItem("Modifier", modifier.Name, Path.GetFileNameWithoutExtension(modifier.AssemblyName));

                        searchResultsListView.Items.Add(listViewItem);
                        searchResults.Add(listViewItem.Index, modifier);
                        count++;
                    }
                }
            }

            TreeNode assemblyNode = null;
            if (MainForm != null)
            {
                foreach (TreeNode node in MainForm.ObjectView.Nodes)
                {
                    if (node.Text == "Assemblies")
                    {
                        assemblyNode = node;
                    }
                }
            }

            if (assemblyNode != null)
            {
                foreach (TreeNode assemblyTreeNode in assemblyNode.Nodes.Descendants())
                {
                    if (assemblyTreeNode.Tag is TypeDefinition typeDefinition)
                    {
                        string typeNamespace = !string.IsNullOrEmpty(typeDefinition.Namespace)
                            ? $"{typeDefinition.Namespace}."
                            : string.Empty;

                        if(IsMatch(searchText, typeDefinition.Namespace, typeDefinition.Name, typeDefinition.Module.Assembly.Name.Name))
                        {
                            ListViewItem listViewItem = CreateSearchItem("Type", $"{typeNamespace}{typeDefinition.Name}", typeDefinition.Module.Assembly.Name.Name);

                            searchResultsListView.Items.Add(listViewItem);
                            searchResults.Add(listViewItem.Index, typeDefinition);
                            count++;
                        }

                        foreach (MethodDefinition methodDefinition in typeDefinition.Methods)
                        {
                            if(IsMatch(searchText, methodDefinition.Name))
                            {
                                ListViewItem listViewItem = CreateSearchItem("Method", $"{typeNamespace}{typeDefinition.Name}.{methodDefinition.Name}", typeDefinition.Module.Assembly.Name.Name);

                                searchResultsListView.Items.Add(listViewItem);
                                searchResults.Add(listViewItem.Index, typeDefinition);
                                count++;
                            }
                        }

                        foreach (FieldDefinition fieldDefinition in typeDefinition.Fields)
                        {
                            if(IsMatch(searchText, fieldDefinition.Name))
                            {
                                ListViewItem listViewItem = CreateSearchItem("Field", $"{typeNamespace}{typeDefinition.Name}.{fieldDefinition.Name}", typeDefinition.Module.Assembly.Name.Name);

                                searchResultsListView.Items.Add(listViewItem);
                                searchResults.Add(listViewItem.Index, typeDefinition);
                                count++;
                            }
                        }

                        foreach (PropertyDefinition propertyDefinition in typeDefinition.Properties)
                        {
                            if(IsMatch(searchText, propertyDefinition.Name))
                            {
                                ListViewItem listViewItem = CreateSearchItem("Property", $"{typeNamespace}{typeDefinition.Name}.{propertyDefinition.Name}", typeDefinition.Module.Assembly.Name.Name);

                                searchResultsListView.Items.Add(listViewItem);
                                searchResults.Add(listViewItem.Index, typeDefinition);
                                count++;
                            }
                        }
                    }
                }
            }

            MainForm?.SelectSearch();

            MainForm?.SetStatus($"Found ({count}) item(s)...");
        }

        public void FindSelectedItem()
        {
            if (searchResultsListView.SelectedIndices.Count <= 0)
            {
                return;
            }
            if (searchResults.TryGetValue(searchResultsListView.SelectedIndices[0], out object patchItem))
            {
                if (patchItem is Field field)
                {
                    MainForm.SelectPatcherItem(field);
                }
                else if (patchItem is Hook hook)
                {
                    MainForm.SelectPatcherItem(hook);
                }
                else if (patchItem is Modifier modifier)
                {
                    MainForm.SelectPatcherItem(modifier);
                }
                else if (patchItem is TypeDefinition typeDefinition)
                {
                    MainForm.SelectPatcherItem(typeDefinition);
                }
            }
        }

        public void OpenItem()
        {
            if (searchResultsListView.SelectedIndices.Count <= 0)
            {
                return;
            }
            if (searchResults.TryGetValue(searchResultsListView.SelectedIndices[0], out object patchItem))
            {
                if (patchItem is Field field)
                {
                    MainForm.GotoField(field);
                }
                else if (patchItem is Hook hook)
                {
                    MainForm.GotoHook(hook);
                }
                else if (patchItem is Modifier modifier)
                {
                    MainForm.GotoModifier(modifier);
                }
                else if (patchItem is TypeDefinition typeDefinition)
                {
                    MainForm.GotoType(typeDefinition);
                }
            }
        }

        private void searchResultsListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            FindSelectedItem();
        }

        private void searchResultsListView_ItemActivate(object sender, EventArgs e)
        {
            FindSelectedItem();
            OpenItem();
        }

        private void searchResultsListView_Click(object sender, EventArgs e)
        {
            FindSelectedItem();
        }

        public void SetAutoWidths()
        {
            searchResultsListView.Columns[0].Width = GetPercent(Width, 10);
            searchResultsListView.Columns[1].Width = GetPercent(Width, 30);
            searchResultsListView.Columns[2].Width = GetPercent(Width, 59);
        }

        private int GetPercent(int total, int percent)
        {
            return percent * total / 100;
        }

        private void SearchViewControl_Load(object sender, EventArgs e)
        {
            SetAutoWidths();
        }

        private void searchResultsListView_Resize(object sender, EventArgs e)
        {
            SetAutoWidths();
        }
    }
}
