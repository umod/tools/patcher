﻿using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using uMod.Patcher.Fields;
using uMod.Patcher.Modifiers;

namespace uMod.Patcher.Views
{
    public partial class ClassViewControl : UserControl
    {
        /// <summary>
        /// Gets or sets the type definition to use
        /// </summary>
        public TypeDefinition TypeDef { get; set; }

        /// <summary>
        /// Gets or sets the main patcher form
        /// </summary>
        public PatcherForm MainForm { get; set; }

        private Control currentview;

        private Modifier modifierview;

        public ClassViewControl()
        {
            InitializeComponent();
        }

        protected override void OnLoad(System.EventArgs eventArgs)
        {
            // Populate the details
            FormUtility.PopulateDetails(TypeDef, typenametextbox, declarationtextbox);

            // Populate the tree
            FormUtility.PopulateTree(TypeDef, objectview);

            bool modifierfound = false;
            foreach (Manifest manifest in Project.Current.Manifests)
            {
                if (TypeDef != null)
                {
                    foreach (Modifier modifier in manifest.Modifiers)
                    {
                        if (modifier.Signature.Equals(Utility.GetModifierSignature(TypeDef)) && modifier.TypeName == TypeDef.FullName)
                        {
                            modifierfound = true;
                            modifierview = modifier;
                            break;
                        }
                    }
                }
            }
            if (modifierfound)
            {
                editbutton.Enabled = false;
                gotoeditbutton.Enabled = true;
            }
            else
            {
                editbutton.Enabled = true;
                gotoeditbutton.Enabled = false;
            }
        }

        private void objectview_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (currentview != null)
            {
                splitter.Panel2.Controls.Remove(currentview);
                currentview.Dispose();
                currentview = null;
            }

            TreeNode selected = e.Node;
            if (selected == null)
            {
                return;
            }

            switch (selected.Tag)
            {
                case MethodDefinition tag:
                {
                    MethodViewControl methodview = new MethodViewControl
                    {
                        Dock = DockStyle.Fill,
                        MethodDef = tag,
                        MainForm = MainForm
                    };
                    splitter.Panel2.Controls.Add(methodview);
                    currentview = methodview;
                    break;
                }
                case PropertyDefinition _:
                case FieldDefinition _:
                {
                    FieldAndPropertyViewControl fieldpropertyview = new FieldAndPropertyViewControl
                    {
                        Dock = DockStyle.Fill,
                        PropertyDef = selected.Tag as PropertyDefinition,
                        FieldDef = selected.Tag as FieldDefinition,
                        MainForm = MainForm
                    };
                    splitter.Panel2.Controls.Add(fieldpropertyview);
                    currentview = fieldpropertyview;
                    break;
                }
            }
        }

        private void editbutton_Click(object sender, System.EventArgs e)
        {
            Modifier modifier = new Modifier(TypeDef, MainForm.rassemblydict[TypeDef.Module.Assembly]);

            MainForm.AddModifier(modifier);
            MainForm.GotoModifier(modifier);

            modifierview = modifier;
            editbutton.Enabled = false;
            gotoeditbutton.Enabled = true;
        }

        private void gotoeditbutton_Click(object sender, System.EventArgs e)
        {
            MainForm.GotoModifier(modifierview);
        }

        private void injectfield_Click(object sender, System.EventArgs e)
        {
            Field field = new Field(TypeDef, MainForm.rassemblydict[TypeDef.Module.Assembly]);

            MainForm.AddField(field);
            MainForm.GotoField(field);
        }
    }
}
