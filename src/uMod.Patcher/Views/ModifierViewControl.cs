﻿using System.Windows.Forms;
using Mono.Cecil;
using uMod.Patcher.Modifiers;

namespace uMod.Patcher.Views
{
    public partial class ModifierViewControl : UserControl, IPatchView
    {
        /// <summary>
        /// Gets or sets the modifier to use
        /// </summary>
        public Modifier Modifier { get; set; }

        /// <summary>
        /// Gets or sets the main patcher form
        /// </summary>
        public PatcherForm MainForm { get; set; }

        public Button FlagButton { get; set; }

        public Button UnflagButton { get; set; }

        bool IPatchView.IsChanged => applybutton?.Enabled ?? false;

        public TabPage Tab { get; set; }

        public ModifierViewControl()
        {
            InitializeComponent();
            FlagButton = flagbutton;
            UnflagButton = unflagbutton;
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            ModifierSettingsControl settingsview = new ModifierSettingsControl
            {
                Modifier = Modifier,
                Controller = this,
                Dock = DockStyle.Fill
            };

            switch (Modifier.Type)
            {
                case ModifierType.Field:
                    detailsgroup.Text = "Field Details";
                    namelabel.Text = "Field Name:";
                    settingsview.FieldDef = MainForm.GetField(Modifier.AssemblyName, Modifier.TypeName, Modifier.Name, Modifier.Signature);
                    break;

                case ModifierType.Method:
                    detailsgroup.Text = "Method Details";
                    namelabel.Text = "Method Name:";
                    settingsview.MethodDef = MainForm.GetModifier(Modifier.AssemblyName, Modifier.TypeName, Modifier.Signature);
                    break;

                case ModifierType.Property:
                    detailsgroup.Text = "Property Details";
                    namelabel.Text = "Property Name:";
                    settingsview.PropertyDef = MainForm.GetProperty(Modifier.AssemblyName, Modifier.TypeName, Modifier.Name, Modifier.Signature);
                    break;

                case ModifierType.Type:
                    detailsgroup.Text = "Type Details";
                    namelabel.Text = "Type Name:";
                    settingsview.TypeDef = MainForm.GetType(Modifier.AssemblyName, Modifier.TypeName);
                    MemberSelectButton.Enabled = false;
                    break;
            }

            modifiersettingstab.Controls.Add(settingsview);

            assemblytextbox.Text = Modifier.AssemblyName;
            typenametextbox.Text = Modifier.TypeName;

            if (Modifier.Type == ModifierType.Type)
            {
                typenamelabel.Visible = false;
                typenametextbox.Visible = false;
            }

            if (settingsview.FieldDef != null || settingsview.MethodDef != null || settingsview.PropertyDef != null)
            {
                nametextbox.Text = $"{Modifier.TypeName}::{Modifier.Name}";
            }
            else if (settingsview.TypeDef != null)
            {
                nametextbox.Text = Modifier.TypeName;
            }
            else
            {
                nametextbox.Text = Modifier.Type != ModifierType.Type ? $"{Modifier.TypeName}::{Modifier.Name} (MISSING)" : $"{Modifier.TypeName} (MISSING)";
            }

            if (Modifier.Flagged)
            {
                flagbutton.Enabled = false;
                unflagbutton.Enabled = true;
                unflagbutton.Focus();
            }
            else
            {
                flagbutton.Enabled = true;
                unflagbutton.Enabled = false;
                flagbutton.Focus();
            }
        }

        private void deletebutton_Click(object sender, System.EventArgs e)
        {
            MainForm?.TryRemovePatch(Modifier);
        }

        private void flagbutton_Click(object sender, System.EventArgs e)
        {
            Modifier.Flagged = true;
            MainForm.UpdateModifier(Modifier, false);
            flagbutton.Enabled = false;
            unflagbutton.Enabled = true;
        }

        private void unflagbutton_Click(object sender, System.EventArgs e)
        {
            Modifier.Flagged = false;
            MainForm.UpdateModifier(Modifier, false);
            flagbutton.Enabled = true;
            unflagbutton.Enabled = false;
        }

        private void applybutton_Click(object sender, System.EventArgs e)
        {
            Apply();
        }

        public void Apply()
        {
            MainForm.UpdateModifier(Modifier, false);
            applybutton.Enabled = false;
            if((Tab?.Text?.EndsWith("*") ?? false))
            {
                Tab.Text = Tab.Text.TrimEnd('*').Trim();
            }
        }

        private void typeNameSearchButton_Click(object sender, System.EventArgs e)
        {
            TypeDefinition typeDef = MainForm?.GetTypeDefinition(Modifier.TypeName);
            if (typeDef != null)
            {
                MainForm?.GotoType(typeDef);
                return;
            }

            MainForm?.Search(Modifier.TypeName);
        }

        private void MemberSelectButton_Click(object sender, System.EventArgs e)
        {
            TypeDefinition typeDef = MainForm?.GetTypeDefinition(Modifier.TypeName);

            if (typeDef == null)
            {
                MainForm?.SetError($"No type found: {Modifier.TypeName}");
                return;
            }

            FormUtility.MemberFilter filter = FormUtility.MemberFilter.All;
            switch (Modifier.Type)
            {
                case ModifierType.Field:
                    filter = FormUtility.MemberFilter.Fields;
                    break;

                case ModifierType.Method:
                    filter = FormUtility.MemberFilter.Methods;
                    break;

                case ModifierType.Property:
                    filter = FormUtility.MemberFilter.Properties;
                    break;
            }

            TypeMethodPrompt typeMethodPrompt = new TypeMethodPrompt
            {
                StartPosition = FormStartPosition.CenterParent,
                MainForm = MainForm,
                TypeDef = typeDef,
                OnMemberSelected = MemberSelected,
                MemberFilter = filter
            };
            typeMethodPrompt.ShowDialog(this);
        }

        private void MemberSelected(TypeMethodPrompt prompt)
        {
            switch (Modifier.Type)
            {
                case ModifierType.Field:
                    Modifier.Signature = Utility.GetModifierSignature(prompt.SelectedFieldDefinition);
                    break;

                case ModifierType.Method:
                    Modifier.Signature = Utility.GetModifierSignature(prompt.SelectedMethodDefinition);
                    break;

                case ModifierType.Property:
                    Modifier.Signature = Utility.GetModifierSignature(prompt.SelectedPropertyDefinition);
                    break;
            }
            
            nametextbox.Text = Modifier.Signature.ToString();
        }

        public void MarkChanged()
        {
            applybutton.Enabled = true;
            if(!(Tab?.Text?.EndsWith("*") ?? true))
            {
                Tab.Text += " *";
            }
        }
    }
}
