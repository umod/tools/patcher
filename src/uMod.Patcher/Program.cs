﻿using Mono.Options;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using static System.Windows.Forms.Application;

namespace uMod.Patcher
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            AppDomain.CurrentDomain.AssemblyResolve += (_, args1) =>
            {
                string resourceName = "uMod.Patcher.Dependencies." + new AssemblyName(args1.Name).Name + ".dll";
                if (resourceName.Contains("resources.dll"))
                {
                    return null;
                }

                using Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
                if (stream != null)
                {
                    byte[] assemblyData = new byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }

                return null;
            };

            string defaultProject = null;
            string defaultTarget = null;

            OptionSet optionSet = new()
            {
                { "<>", "the .upj or .opj file or directory", value => { defaultProject = value; } },
                { "t|target=", "the project target directory", value => { defaultTarget = value; } }
            };

            try
            {
                optionSet.Parse(args);
            }
            catch (OptionException oEx)
            {
                Console.WriteLine($"{oEx.OptionName}: {oEx.Message}");
            }

            EnableVisualStyles();
            SetCompatibleTextRenderingDefault(false);
            PatcherForm patcherForm = new PatcherForm();
            if (!string.IsNullOrEmpty(defaultProject))
            {
                patcherForm.CurrentProjectFilename = defaultProject;
            }

            if(!string.IsNullOrEmpty(defaultTarget))
            {
                patcherForm.DefaultTarget = defaultTarget;
            }
            Run(patcherForm);
        }
    }
}
