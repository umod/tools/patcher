﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using uMod.Patcher.Fields;
using uMod.Patcher.Hooks;
using uMod.Patcher.Modifiers;

namespace uMod.Patcher
{
    public abstract class HistoryItem
    {
        public bool Saved { get; set; }
    }

    public class HistoryItemBatch : HistoryItem
    {
        public Queue<HistoryItem> History { get; set; }
    }

    // happens when hook is moved to category
    public class PatchCategoryChanged : HistoryItem
    {
        public Patch Before { get; set; }
        public Patch After { get; set; }

        public string BeforeCategory { get; set; }
        public string AfterCategory { get; set; }
        public Manifest Manifest { get; internal set; }
    }

    // happens when patch is moved to manifest
    public class PatchManifestChanged : PatchCategoryChanged
    {
        public Manifest BeforeManifest { get; set; }
        public Manifest AfterManifest { get; set; }
    }

    public class HookChanged : HistoryItem
    {
        public Hook Before { get; set; }
        public Hook After { get; set; }

        public Manifest Manifest { get; set; }

        public HookChanged(Hook hooknode1, Hook hooknode2, Manifest manifest = null)
        {
            Before = hooknode1;
            After = hooknode2;
            Manifest = manifest;
        }
    }

    public class ModifierChanged : HistoryItem
    {
        public Modifier Before { get; set; }
        public Modifier After { get; set; }

        public Manifest Manifest { get; set; }

        public ModifierChanged(Modifier modifier1, Modifier modifier2, Manifest manifest = null)
        {
            Before = modifier1;
            After = modifier2;
            Manifest = manifest;
        }
    }

    public class FieldChanged : HistoryItem
    {
        public Field Before { get; set; }
        public Field After { get; set; }

        public Manifest Manifest { get; set; }

        public FieldChanged(Field field1, Field field2, Manifest manifest = null)
        {
            Before = field1;
            After = field2;
            Manifest = manifest;
        }
    }

    public class ProjectChanged : HistoryItem
    {
        public string BeforeName { get; set; }
        public string AfterName { get; set; }

        public string BeforeTargetDirectory { get; set; }
        public string AfterTargetDirectory { get; set; }

        public ProjectChanged(string beforename, string beforetarget, Project project)
        {
            BeforeName = beforename;
            AfterName = project.Name;
            BeforeTargetDirectory = beforetarget;
            AfterTargetDirectory = project.TargetDirectory;
        }
    }

    public class ManifestChanged : HistoryItem
    {
        public string BeforeName { get; set; }
        public string AfterName { get; set; }

        public Manifest Before { get; set; }
        public Manifest After { get; set; }

        public List<string> HookTypes {get;set;}

        public ManifestChanged(Manifest before, Manifest after, string beforeName = null, string afterName = null)
        {
            Before = before;
            After = after;
            BeforeName = beforeName;
            AfterName = afterName;
        }
    }
}
