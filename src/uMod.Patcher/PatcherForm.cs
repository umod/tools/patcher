using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using uMod.Patcher.Deobfuscation;
using uMod.Patcher.Exceptions;
using uMod.Patcher.Fields;
using uMod.Patcher.Hooks;
using uMod.Patcher.Modifiers;
using uMod.Patcher.Patching;
using uMod.Patcher.Views;
using static System.Windows.Forms.Application;

namespace uMod.Patcher
{
    public partial class PatcherForm : Form
    {
        /// <summary>
        /// Gets the filename of the currently open project
        /// </summary>
        public string CurrentProjectFilename { get; internal set; }

        /// <summary>
        /// Gets the current settings
        /// </summary>
        public PatcherFormSettings CurrentSettings { get; private set; }

        /// <summary>
        /// Gets the uMod assembly
        /// </summary>
        public AssemblyDefinition CoreAssembly { get; private set; }

        public string CorePath { get; set; }

        private Dictionary<string, AssemblyDefinition> assemblydict;
        internal Dictionary<AssemblyDefinition, string> rassemblydict;

        private static IAssemblyResolver resolver;

        private readonly Version version = Assembly.GetExecutingAssembly().GetName().Version;

        private MouseEventArgs mea;

        public static PatcherForm MainForm { get; private set; }

        private MRUManager mruManager;

        private int newCategoryCount;

        private TreeNode dragNode;

        private TreeNode tempDropNode;

        private TreeNode lastDragDestination;

        private DateTime lastDragDestinationTime;

        public int HistoryIndex = -1;

        public List<HistoryItem> History = new List<HistoryItem>();

        internal class NodeAssemblyData
        {
            public bool Included { get; set; }
            public bool Loaded { get; set; }
            public string AssemblyName { get; set; }
            public AssemblyDefinition Definition { get; set; }
        }

        public PatcherForm()
        {
            InitializeComponent();
            string title = string.Format(Text, version);
            Text = title.Slice(0, title.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase));
            MainForm = this;
        }

        public PatcherForm(string filename)
        {
            InitializeComponent();
            string title = string.Format(Text, version);
            Text = title.Slice(0, title.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase));
            if (File.Exists(filename) || Directory.Exists(filename))
            {
                CurrentProjectFilename = filename;
            }
            else
            {
                MessageBox.Show(filename + " does not exist!", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            MainForm = this;
        }

        internal void ClearStatus()
        {
            SetStatus("");
        }

        internal void SetStatus(string message)
        {
            SetStatus(message, Color.Black);
        }

        internal void SetStatus(string message, Color color)
        {
            statuslabel.Text = message;
            statuslabel.ForeColor = color;
            statuslabel.Invalidate();
        }

        internal void SetError(string message)
        {
            SetStatus($"Error: {message}", Color.Red);
        }

        protected string OpenCoreFileDialog()
        {
            coreFileDialog.InitialDirectory = Environment.CurrentDirectory;
            coreFileDialog.Filter = "Oxide Core (*.dll)|Oxide.Core.dll|uMod Core (*.dll)|uMod.Core.dll|All files (*.*)|*.*";
            coreFileDialog.FilterIndex = 2;
            coreFileDialog.RestoreDirectory = true;

            if (coreFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Get the path of specified file
                return coreFileDialog.FileName;
            }

            Environment.Exit(0);
            return string.Empty;
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Load mod
            string filename = string.Empty;
            string oxideFilename = Path.Combine(StartupPath, "Oxide.Core.dll");
            string uModFilename = Path.Combine(StartupPath, "uMod.Core.dll");
            if (File.Exists(oxideFilename))
            {
                filename = oxideFilename;
            }
            else if (File.Exists(uModFilename))
            {
                filename = uModFilename;
            }
            else
            {
                MessageBox.Show("Failed to locate Oxide.Core.dll or uMod.Core.dll. Please specify the Oxide.Core.dll or uMod.Core.dll to use.", "uMod Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                filename = OpenCoreFileDialog();
            }

            CorePath = filename;
            CoreAssembly = AssemblyDefinition.ReadAssembly(filename);

            // Load MRU
            mruManager = new MRUManager(recentprojects, "uMod.Patcher", 10, openrecentproject_Click);

            // Load settings
            // CurrentSettings = PatcherFormSettings.Load();
            // Location = CurrentSettings.FormPosition;
            // Size = CurrentSettings.FormSize;
            // WindowState = CurrentSettings.WindowState;

            assemblydict = new Dictionary<string, AssemblyDefinition>();
            rassemblydict = new Dictionary<AssemblyDefinition, string>();

            if (CurrentProjectFilename != null)
            {
                OpenProject(CurrentProjectFilename);
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            // base.OnFormClosing(e);

            // Save settings
            // CurrentSettings.FormPosition = Location;
            // CurrentSettings.FormSize = Size;
            // CurrentSettings.WindowState = WindowState;
            // CurrentSettings.Save();
        }

        #region Menu Handlers

        /// <summary>
        /// Called when the open project menu item was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openproject_Click(object sender, System.EventArgs e)
        {
            DialogResult result = openprojectdialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                OpenProject(openprojectdialog.FileName);
            }
        }

        private void openrecentproject_Click(object sender, System.EventArgs e)
        {
            string file = (sender as ToolStripItem).Text;
            if (!File.Exists(file))
            {
                if (MessageBox.Show($"{file} doesn't exist. Do you want to remove it from the recent files list?", "File not found", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    mruManager.Remove(file);
                }

                return;
            }

            mruManager.AddOrUpdate(file);
            OpenProject(file);
        }

        /// <summary>
        /// Called when the new project menu item was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newproject_Click(object sender, System.EventArgs e)
        {
            NewProjectForm form = new NewProjectForm { StartPosition = FormStartPosition.CenterParent };
            form.ShowDialog(this);
        }

        /// <summary>
        /// Called when the exit menu item was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exit_Click(object sender, System.EventArgs e)
        {
            Exit();
        }

        #endregion Menu Handlers

        #region Toolbar Handlers

        /// <summary>
        /// Called when the new project tool icon was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newprojecttool_Click(object sender, System.EventArgs e)
        {
            NewProjectForm form = new NewProjectForm { StartPosition = FormStartPosition.CenterParent };
            form.ShowDialog(this);
        }

        /// <summary>
        /// Called when the open project tool icon was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openprojecttool_Click(object sender, System.EventArgs e)
        {
            DialogResult result = openprojectdialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                OpenProject(openprojectdialog.FileName);
            }
        }

        /// <summary>
        /// Called when the patch tool icon was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void patchtool_Click(object sender, System.EventArgs e)
        {
            PatchProcessForm patchprocess = new PatchProcessForm
            {
                StartPosition = FormStartPosition.CenterParent,
                PatchProject = Project.Current
            };
            patchprocess.ShowDialog(this);
            UpdateAllHooks();
        }

        #endregion Toolbar Handlers

        #region Object View Handlers

        public Hook GetHook(string hookName)
        {
            TreeNode assemblyNode = null;
            if (MainForm != null)
            {
                foreach (TreeNode node in MainForm.ObjectView.Nodes)
                {
                    if (node.Text == "Hooks")
                    {
                        assemblyNode = node;
                    }
                }
            }

            if (assemblyNode != null)
            {
                foreach (TreeNode assemblyTreeNode in assemblyNode.Nodes.Descendants())
                {
                    if (assemblyTreeNode.Tag is Hook hookDefinition && hookDefinition.Name == hookName)
                    {
                        return hookDefinition;
                    }
                }
            }

            return null;
        }

        public Mono.Cecil.TypeDefinition GetTypeDefinition(string typeName)
        {
            TreeNode assemblyNode = null;
            if (MainForm != null)
            {
                foreach (TreeNode node in MainForm.ObjectView.Nodes)
                {
                    if (node.Text == "Assemblies")
                    {
                        assemblyNode = node;
                    }
                }
            }

            if (assemblyNode != null)
            {
                foreach (TreeNode assemblyTreeNode in assemblyNode.Nodes.Descendants())
                {
                    if (assemblyTreeNode.Tag is Mono.Cecil.TypeDefinition typeDefinition && typeDefinition.Name == typeName)
                    {
                        return typeDefinition;
                    }
                }
            }

            return null;
        }

        public void ClearSearch()
        {
            searchTextBox.Text = string.Empty;
        }

        public void SelectSearch()
        {
            searchTextBox.Focus();
        }

        public void MinimizeAllTreeNodes(TreeNode except = null)
        {
            foreach (TreeNode node in objectview.Nodes.Descendants())
            {
                if (except == node)
                {
                    continue;
                }
                node.Collapse();
            }
        }

        public void SelectPatcherItem(Field field)
        {

            foreach (TreeNode node in objectview.Nodes.Descendants())
            {
                if (node.Tag is Field fieldTag && fieldTag == field)
                {
                    MinimizeAllTreeNodes(node);
                    objectview.SelectedNode = node;
                    objectview.Focus();
                }
            }
        }

        public void SelectPatcherItem(Hook hook)
        {
            foreach (TreeNode node in objectview.Nodes.Descendants())
            {
                if (node.Tag is Hook hookTag && hookTag == hook)
                {
                    MinimizeAllTreeNodes(node);
                    objectview.SelectedNode = node;
                    objectview.Focus();
                }
            }
        }

        public void SelectPatcherItem(Modifier modifier)
        {
            foreach (TreeNode node in objectview.Nodes.Descendants())
            {
                if (node.Tag is Modifier modifierTag && modifierTag == modifier)
                {
                    MinimizeAllTreeNodes(node);
                    objectview.SelectedNode = node;
                    objectview.Focus();
                }
            }
        }

        public void SelectPatcherItem(Mono.Cecil.TypeDefinition typeDefinition)
        {
            foreach (TreeNode node in objectview.Nodes.Descendants())
            {
                if (node.Tag is Mono.Cecil.TypeDefinition typeDefinitionTag && typeDefinitionTag == typeDefinition)
                {
                    MinimizeAllTreeNodes(node);
                    objectview.SelectedNode = node;
                    objectview.Focus();
                }
            }
        }

        private void objectview_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                TreeNode node = e.Node;
                if (node != null)
                {
                    objectview.SelectedNode = node;
                    string str = node.Tag as string ?? string.Empty;
                    if (node.Tag is NodeAssemblyData data)
                    {
                        if (!data.Included)
                        {
                            unloadedassemblymenu.Show(objectview, e.X, e.Y);
                        }
                        else
                        {
                            loadedassemblymenu.Show(objectview, e.X, e.Y);
                        }
                    }
                    else if (node.Tag is Patch hook)
                    {
                        cloneToolStripMenuItem.Enabled = hook is Hook;
                        if (hook.Flagged)
                        {
                            FlagMenuItem.Enabled = false;
                            UnflagMenuItem.Enabled = true;
                        }
                        else
                        {
                            FlagMenuItem.Enabled = true;
                            UnflagMenuItem.Enabled = false;
                        }
                        hooksmenu.Show(objectview, e.X, e.Y);
                    }
                    else if (str == "Hooks" || str == "Modifiers" || str == "Fields")
                    {
                        hookmenu.Show(objectview, e.X, e.Y);
                    }
                    else if (str == "Category")
                    {
                        categorymenu.Show(objectview, e.X, e.Y);
                    }
                    else if (node.Tag is Manifest)
                    {
                        manifestmenu.Show(objectview, e.X, e.Y);
                        if (!IsHookNode(node))
                        {
                            addCategoryToolStripMenuItem.Enabled = false;
                        }
                        else
                        {
                            addCategoryToolStripMenuItem.Enabled = true;
                        }
                    }
                }
            }
        }

        internal IEnumerable<Hook> GetHooksWithBaseHook(Hook hook)
        {
            Manifest manifest = Project.Current.GetManifestByAssembly(hook.AssemblyName);
            return manifest.Hooks.Where(h => h.BaseHook == hook);
        }

        private void objectview_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            // Check if the tab is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if ((tabpage.Tag is ProjectSettingsControl psControl || tabpage.Tag is ClassViewControl cvControl) &&
                     e.Node.Text == tabpage.Text)
                {
                    tabview.SelectedTab = tabpage;
                    return;
                }
            }

            if (e.Node.Tag is string str)
            {
                switch (str)
                {
                    case "Project Settings":
                        ProjectSettingsControl projectsettings = new ProjectSettingsControl();
                        projectsettings.ProjectFilename = CurrentProjectFilename;
                        projectsettings.ProjectObject = Project.Current;
                        AddTab("Project Settings", projectsettings, projectsettings);
                        break;
                }
            }
            else if (e.Node.Tag is Mono.Cecil.TypeDefinition typedef)
            {
                GotoType(typedef);
            }
            else if (e.Node.Tag is Hook hook)
            {
                GotoHook(hook);
            }
            else if (e.Node.Tag is Modifier modifier)
            {
                GotoModifier(modifier);
            }
            else if (e.Node.Tag is Field field)
            {
                GotoField(field);
            }
        }

        private void unloadedassemblymenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Name == "addtoproject")
            {
                NodeAssemblyData data = (NodeAssemblyData)objectview.SelectedNode.Tag;
                Project.Current.AddManifestByAssembly(data.AssemblyName);
                Manifest manifest = Project.Current.GetManifestByAssembly(data.AssemblyName);
                AddHistory(new ManifestChanged(null, manifest.DeepClone(), null, manifest.Name));
                //Project.Current.Save(CurrentProjectFilename);
                data.Included = true;
                data.Loaded = true;
                data.Definition = LoadAssembly(data.AssemblyName);
                objectview.SelectedNode.ImageKey = "accept.png";
                objectview.SelectedNode.SelectedImageKey = "accept.png";
                objectview.SelectedNode.Nodes.Clear();

                string realfilename = Path.Combine(Project.Current.TargetDirectory, data.AssemblyName);
                string origfilename = Path.Combine(Project.Current.TargetDirectory, Path.GetFileNameWithoutExtension(data.AssemblyName) + "_Original" + Path.GetExtension(data.AssemblyName));
                if (!File.Exists(origfilename))
                {
                    CreateOriginal(realfilename, origfilename);
                }

                // Populate
                PopulateAssemblyNode(objectview.SelectedNode, data.Definition);
            }
        }

        private void loadedassemblymenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Name == "removefromproject")
            {
                NodeAssemblyData data = (NodeAssemblyData)objectview.SelectedNode.Tag;
                Manifest manifest = Project.Current.GetManifestByAssembly(data.AssemblyName);
                Project.Current.RemoveManifestByAssembly(data.AssemblyName);
                AddHistory(new ManifestChanged(manifest.DeepClone(), null, manifest.Name, null));
                //Project.Current.Save(CurrentProjectFilename);
                data.Included = false;
                data.Loaded = false;
                data.Definition = null;
                if (objectview.SelectedNode.Tag == null)
                {
                    objectview.SelectedNode.Parent.Nodes.Remove(objectview.SelectedNode);
                }
                else
                {
                    objectview.SelectedNode.ImageKey = "cross.png";
                    objectview.SelectedNode.SelectedImageKey = "cross.png";
                    objectview.SelectedNode.Nodes.Clear();
                }
            }
        }

        private void flag_Click(object sender, System.EventArgs e)
        {
            if (Project.Current != null)
            {
                if (objectview.SelectedNode.Tag is Patch patch && !patch.Flagged)
                {
                    patch.Flagged = true;
                    UpdatePatch(patch);
                }
            }
        }

        private void unflag_Click(object sender, System.EventArgs e)
        {
            if (Project.Current != null)
            {
                if (objectview.SelectedNode.Tag is Patch patch && patch.Flagged)
                {
                    patch.Flagged = false;
                    UpdatePatch(patch);
                }
            }
        }

        private void unflagall_Click(object sender, System.EventArgs e)
        {
            if (Project.Current != null)
            {
                UnflagAll(objectview.SelectedNode.Nodes);
                UpdateAllHooks();
            }
        }

        private void UnflagAll(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Patch patch)
                {
                    if (patch.Flagged)
                    {
                        patch.Flagged = false;
                        UpdatePatch(patch);
                    }
                }
                else
                {
                    UnflagAll(node.Nodes);
                }
            }
        }

        private void flagall_Click(object sender, System.EventArgs e)
        {
            if (Project.Current != null)
            {
                FlagAll(objectview.SelectedNode.Nodes);
                UpdateAllHooks();
            }
        }

        private void FlagAll(TreeNodeCollection nodes)
        {
            BeginHistoryBatch();
            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Patch patch)
                {
                    if (!patch.Flagged)
                    {
                        patch.Flagged = true;
                        UpdatePatch(patch);
                    }
                }
                else
                {
                    FlagAll(node.Nodes);
                }
            }
            EndHistoryBatch();
        }

        private void FlagCategory_Click(object sender, System.EventArgs e)
        {
            if (Project.Current != null)
            {
                BeginHistoryBatch();
                FlagAll(objectview.SelectedNode.Nodes);
                EndHistoryBatch();
            }
        }

        private void UnflagCategory_Click(object sender, System.EventArgs e)
        {
            if (Project.Current != null)
            {
                BeginHistoryBatch();
                UnflagAll(objectview.SelectedNode.Nodes);
                EndHistoryBatch();
            }
        }

        private void addcategory_Click(object sender, System.EventArgs e)
        {
            TreeNode node = objectview.SelectedNode;
            if (node != null && node.Tag is Manifest)
            {
                TreeNode category = new TreeNode($"New Category {newCategoryCount++}")
                {
                    Tag = "Category",
                    ImageKey = "folder.png",
                    SelectedImageKey = "folder.png"
                };

                node.Nodes.Insert(0, category);
                node.Expand();
                objectview.LabelEdit = true;
                if (!node.Nodes[0].IsEditing)
                {
                    node.Nodes[0].BeginEdit();
                }
            }
        }

        private void renamecategory_Click(object sender, System.EventArgs e)
        {
            TreeNode node = objectview.SelectedNode;
            if (node == null || (node.Tag.ToString() != "Category" && !(node.Tag is Manifest)))
            {
                return;
            }

            objectview.LabelEdit = true;
            if (!node.IsEditing)
            {
                node.BeginEdit();
            }
        }

        private void objectview_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (e.Label != null)
            {
                if (e.Label.Length > 0)
                {
                    if (e.Node.Tag is Manifest manifest)
                    {
                        bool flag = ManifestExists(e.Label);
                        if (e.Label.IndexOfAny(new[] { '@', '.', ',', '!', '"' }) == -1 && !flag)
                        {
                            e.Node.EndEdit(false);
                            objectview.LabelEdit = false;
                            e.Node.Text = e.Label;
                            e.Node.Name = e.Label;

                            Project.Current.RenameManifest(manifest.Name, e.Label);
                            //Project.Current.Save(CurrentProjectFilename);
                        }
                        else if (flag)
                        {
                            e.CancelEdit = true;
                            MessageBox.Show("A manifest with this name already exists!", "Invalid Manifest",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Node.BeginEdit();
                        }
                        else
                        {
                            e.CancelEdit = true;
                            MessageBox.Show(
                                "Invalid manifest!\nThe manifest contains invalid characters!\nThese characters are: '@','.', ',', '!', '\"'",
                                "Invalid Manifest", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Node.BeginEdit();
                        }
                    }
                    else if (e.Node.Tag.ToString() == "Category")
                    {
                        bool flag = CategoryExists(e.Label);
                        if (e.Label.IndexOfAny(new[] { '@', '.', ',', '!', '"' }) == -1 && !flag)
                        {
                            e.Node.EndEdit(false);
                            objectview.LabelEdit = false;
                            e.Node.Text = e.Label;
                            e.Node.Name = e.Label;

                            foreach (TreeNode node in e.Node.Nodes)
                            {
                                Hook hook = node.Tag as Hook;
                                if (hook == null)
                                {
                                    continue;
                                }

                                hook.HookCategory = e.Label;
                                UpdateHook(hook, false);
                            }

                            objectview.BeginInvoke(new Action(() =>
                            {
                                if (e.Node.Parent != null)
                                {
                                    Sort(e.Node.Parent.Nodes);
                                }
                                //objectview.SelectedNode = objectview.Nodes["Hooks"].Nodes[e.Label];
                            }));
                        }
                        else if (flag)
                        {
                            e.CancelEdit = true;
                            MessageBox.Show("A category with this name already exists!", "Invalid Category",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Node.BeginEdit();
                        }
                        else
                        {
                            e.CancelEdit = true;
                            MessageBox.Show(
                                "Invalid category!\nThe category contains invalid characters!\nThese characters are: '@','.', ',', '!', '\"'",
                                "Invalid Category", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Node.BeginEdit();
                        }
                    }
                }
                else
                {
                    e.CancelEdit = true;
                    MessageBox.Show("Invalid name!\nThe name can't be empty!", "Invalid Name",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Node.BeginEdit();
                }
            }
            else
            {
                objectview.LabelEdit = false;
            }
        }

        private bool ManifestExists(string label)
        {
            string[] names = Project.Current.GetManifestNames();
            if (names != null)
            {
                return names.Contains(label, StringComparer.InvariantCultureIgnoreCase);
            }

            return false;
        }

        private void removecategory_Click(object sender, System.EventArgs e)
        {
            TreeNode node = objectview.SelectedNode;
            if (node == null || (node.Tag.ToString() != "Category" && !(node.Tag is Manifest)))
            {
                return;
            }

            if (node.Tag is Manifest manifest)
            {
                Project.Current.DeleteManifest(manifest.OriginalFile);
                RemoveManifestNodes(manifest);
                AddHistory(new ManifestChanged(manifest.DeepClone(), null, manifest.Name));
            }
            else
            {
                for (int i = node.Nodes.Count; i > 0; i--)
                {
                    TreeNode child = node.Nodes[i - 1];
                    node.Nodes.Remove(child);
                    if (child.Tag is Hook hook)
                    {
                        hook.HookCategory = null;
                        UpdateHook(hook, false);
                    }
                    node.Parent.Nodes.Add(child);
                }
            }
            if (node.Parent != null)
            {
                Sort(node.Parent.Nodes, false);
            }
            node.Remove();
        }

        private void RemoveManifestNodes(Manifest manifest, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes;
            }

            List<TreeNode> removeNodes = new List<TreeNode>();
            foreach (TreeNode treenode in nodes)
            {
                if (treenode.Tag is Manifest submanifest && manifest.OriginalFile == submanifest.OriginalFile)
                {
                    removeNodes.Add(treenode);
                }

                RemoveManifestNodes(manifest, treenode.Nodes);
            }

            foreach (TreeNode treenode in removeNodes)
            {
                treenode.Remove();
            }
        }

        private void ReplaceManifestNodes(Manifest manifest, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Manifest && node.Tag.Equals(manifest))
                {
                    node.Tag = manifest;
                    continue;
                }

                ReplaceManifestNodes(manifest, node.Nodes);
            }
        }

        private void objectview_MouseDown(object sender, MouseEventArgs e)
        {
            objectview.SelectedNode = objectview.GetNodeAt(e.X, e.Y);
        }

        private void objectview_ItemDrag(object sender, ItemDragEventArgs e)
        {
            TreeNode node = e.Item as TreeNode;
            if (node?.Tag != null && !(node.Tag is Hook) && !(node.Tag is Modifier) && !(node.Tag is Field))
            {
                return;
            }

            dragNode = (TreeNode)e.Item;

            imagelistDragDrop.Images.Clear();
            imagelistDragDrop.ImageSize = new Size(Math.Min(dragNode.Bounds.Width + objectview.Indent, 256), dragNode.Bounds.Height);

            Bitmap bmp = new Bitmap(dragNode.Bounds.Width + objectview.Indent + 5, dragNode.Bounds.Height);
            Graphics gfx = Graphics.FromImage(bmp);
            gfx.CompositingQuality = CompositingQuality.HighQuality;
            gfx.DrawImage(imagelist.Images[dragNode.ImageKey], 0, 0);
            gfx.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            gfx.DrawString(dragNode.Text.Length > 34 ? $"{dragNode.Text[..34]}..." : dragNode.Text, objectview.Font, new SolidBrush(objectview.ForeColor), objectview.Indent, 0f);
            imagelistDragDrop.Images.Add(bmp);

            if (!DragDropHelper.ImageList_BeginDrag(imagelistDragDrop.Handle, 0, -8, 55))
            {
                return;
            }

            DoDragDrop(bmp, DragDropEffects.Move);
            DragDropHelper.ImageList_EndDrag();
        }

        private void objectview_DragOver(object sender, DragEventArgs e)
        {
            TreeNode targetNode = objectview.GetNodeAt(objectview.PointToClient(Cursor.Position));

            Point p = PointToClient(new Point(e.X, e.Y));
            DragDropHelper.ImageList_DragMove(p.X, p.Y);

            objectview.Scroll();

            e.Effect = DragDropEffects.None;

            if (targetNode == null)
            {
                return;
            }

            targetNode.EnsureVisible();
            if (targetNode != tempDropNode)
            {
                DragDropHelper.ImageList_DragShowNolock(false);
                objectview.SelectedNode = targetNode;
                DragDropHelper.ImageList_DragShowNolock(true);
                tempDropNode = targetNode;
            }

            if (dragNode.Tag is Hook && !IsHookNode(targetNode))
            {
                return;
            }

            if (dragNode.Tag is Modifier && !IsModifierNode(targetNode))
            {
                return;
            }

            if (dragNode.Tag is Field && !IsFieldNode(targetNode))
            {
                return;
            }

            if (targetNode.Tag.ToString() != "Category" && !(targetNode.Tag is Manifest))
            {
                return;
            }

            if (lastDragDestination != targetNode)
            {
                lastDragDestination = targetNode;
                lastDragDestinationTime = DateTime.Now;
            }
            else
            {
                TimeSpan hoverTime = DateTime.Now.Subtract(lastDragDestinationTime);
                if (!targetNode.IsExpanded && hoverTime.TotalSeconds >= 2)
                {
                    targetNode.Expand();
                }
            }

            e.Effect = DragDropEffects.Move;
        }

        private void objectview_DragDrop(object sender, DragEventArgs e)
        {
            DragDropHelper.ImageList_DragLeave(objectview.Handle);

            TreeNode targetNode = objectview.GetNodeAt(objectview.PointToClient(Cursor.Position));

            if (targetNode == null)
            {
                return;
            }

            if (!targetNode.IsExpanded)
            {
                targetNode.Expand();
            }

            if (dragNode.Tag is Hook hook)
            {
                Tuple<Manifest, Manifest> manifestPosition = AddPatchNode(targetNode, hook);
                Manifest oldManifest = manifestPosition.Item1;
                Manifest newManifest = manifestPosition.Item2;

                objectview.Nodes.Remove(dragNode);
                targetNode.Nodes.Add(dragNode);
                MoveHookNode(targetNode, hook, oldManifest, newManifest);
                objectview.SelectedNode = dragNode;
                dragNode.EnsureVisible();
            }
            else if (dragNode.Tag is Modifier modifier)
            {
                Tuple<Manifest, Manifest> manifestPosition = AddPatchNode(targetNode, modifier);
                Manifest oldManifest = manifestPosition.Item1;
                Manifest newManifest = manifestPosition.Item2;

                objectview.Nodes.Remove(dragNode);
                targetNode.Nodes.Add(dragNode);
                MoveModifierNode(targetNode, modifier, oldManifest, newManifest);

                objectview.SelectedNode = dragNode;
                dragNode.EnsureVisible();
            }
            else if (dragNode.Tag is Field field)
            {
                Tuple<Manifest, Manifest> manifestPosition = AddPatchNode(targetNode, field);
                Manifest oldManifest = manifestPosition.Item1;
                Manifest newManifest = manifestPosition.Item2;

                objectview.Nodes.Remove(dragNode);
                targetNode.Nodes.Add(dragNode);
                MoveFieldNode(targetNode, field, oldManifest, newManifest);

                objectview.SelectedNode = dragNode;
                dragNode.EnsureVisible();
            }
        }

        private string GetPatchTypeRootName(Patch patch)
        {
            switch (patch)
            {
                case Hook:
                    return "Hooks";
                case Modifier:
                    return "Modifiers";
                case Field:
                    return "Fields";
            }

            return string.Empty;
        }

        private void MovePatchNode(TreeNode targetNode, Patch patch, Manifest oldManifest, Manifest newManifest)
        {
            switch (patch)
            {
                case Hook hook:
                    MoveHookNode(targetNode, hook, oldManifest, newManifest);
                    break;
                case Modifier modifier:
                    MoveModifierNode(targetNode, modifier, oldManifest, newManifest);
                    break;
                case Field field:
                    MoveFieldNode(targetNode, field, oldManifest, newManifest);
                    break;
            }
        }

        private void MoveFieldNode(TreeNode targetNode, Field field, Manifest oldManifest, Manifest newManifest)
        {
            if (oldManifest != newManifest)
            {
                LastPatches.TryGetValue(field, out Patch patch);
                AddHistory(new PatchManifestChanged()
                {
                    BeforeManifest = oldManifest,
                    AfterManifest = newManifest,
                    Before = patch.DeepClone(),
                    After = field.DeepClone()
                });
            }

            UpdateField(field, false);
            Sort(targetNode.Nodes);
        }

        private void MoveModifierNode(TreeNode targetNode, Modifier modifier, Manifest oldManifest, Manifest newManifest)
        {
            if (oldManifest != newManifest)
            {
                LastPatches.TryGetValue(modifier, out Patch patch);
                AddHistory(new PatchManifestChanged()
                {
                    BeforeManifest = oldManifest,
                    AfterManifest = newManifest,
                    Before = patch.DeepClone(),
                    After = modifier.DeepClone()
                });
            }

            UpdateModifier(modifier, false);
            Sort(targetNode.Nodes);
        }

        private void MoveHookNode(TreeNode targetNode, Hook hook, Manifest oldManifest, Manifest newManifest)
        {
            string oldCategory = hook.HookCategory;

            if (targetNode.Tag.ToString() == "Category")
            {
                hook.HookCategory = targetNode.Text;
            }
            else
            {
                hook.HookCategory = null;
            }

            if (oldManifest != newManifest)
            {
                LastPatches.TryGetValue(hook, out Patch patch);
                Hook clone = hook.DeepClone();
                AddHistory(new PatchManifestChanged()
                {
                    BeforeManifest = oldManifest,
                    AfterManifest = newManifest,
                    BeforeCategory = oldCategory,
                    AfterCategory = hook.HookCategory,
                    Before = patch.DeepClone() ?? clone,
                    After = clone
                });
            }
            else if (oldCategory != hook.HookCategory)
            {
                LastPatches.TryGetValue(hook, out Patch patch);
                Hook clone = hook.DeepClone();
                AddHistory(new PatchCategoryChanged()
                {
                    BeforeCategory = oldCategory,
                    AfterCategory = hook.HookCategory,
                    Before = patch.DeepClone() ?? clone,
                    After = clone,
                    Manifest = oldManifest
                });
            }

            UpdateHook(hook, false);
            Sort(targetNode.Nodes);
        }

        public void AddHistory(HistoryItem historyItem)
        {
            if (BatchingHistory)
            {
                LastHistoryItemBatch.Enqueue(historyItem);
                return;
            }

            int len = History.Count - 1;

            // user undid some stuff, branch undo history
            if (HistoryIndex < len)
            {
                // remove everything after current index
                for (int i = (History.Count - 1); i > HistoryIndex; i--)
                {
                    History.RemoveAt(i);
                }
            }

            History.Add(historyItem);

            if (History.Count > 150)
            {
                History.RemoveRange(0, 10);
            }

            HistoryIndex = History.Count - 1;
        }

        private Tuple<Manifest, Manifest> AddPatchNode(TreeNode targetNode, Patch patch)
        {
            Manifest originalManifest = GetClosestManifest(dragNode);
            Manifest newManifest = GetClosestManifest(targetNode);

            if (originalManifest.Name != newManifest.Name)
            {
                originalManifest?.RemovePatch(patch);
                if (string.IsNullOrEmpty(newManifest.AssemblyName))
                {
                    newManifest.AssemblyName = patch.AssemblyName;
                }
                else if (newManifest.AssemblyName != patch.AssemblyName)
                {
                    newManifest = newManifest.Clone();
                    newManifest.AssemblyName = patch.AssemblyName;
                }
                newManifest.AddPatch(patch);
            }

            return new Tuple<Manifest, Manifest>(originalManifest, newManifest);
        }

        private Manifest GetClosestManifest(TreeNode node)
        {
            if (node.Tag is Manifest manifest)
            {
                return manifest;
            }

            if (node.Parent != null)
            {
                return GetClosestManifest(node.Parent);
            }

            return null;
        }

        private bool IsHookNode(TreeNode node)
        {
            if (node.Tag.ToString() == "Hooks")
            {
                return true;
            }

            if (node.Parent != null)
            {
                return IsHookNode(node.Parent);
            }

            return false;
        }

        private bool IsModifierNode(TreeNode node)
        {
            if (node.Tag.ToString() == "Modifiers")
            {
                return true;
            }

            if (node.Parent != null)
            {
                return IsModifierNode(node.Parent);
            }

            return false;
        }

        private bool IsFieldNode(TreeNode node)
        {
            if (node.Tag.ToString() == "Fields")
            {
                return true;
            }

            if (node.Parent != null)
            {
                return IsFieldNode(node.Parent);
            }

            return false;
        }

        private void objectview_DragEnter(object sender, DragEventArgs e)
        {
            DragDropHelper.ImageList_DragEnter(objectview.Handle, e.X - objectview.Left, e.Y - objectview.Top);
        }

        private void objectview_DragLeave(object sender, System.EventArgs e)
        {
            DragDropHelper.ImageList_DragLeave(objectview.Handle);
        }

        #endregion Object View Handlers

        #region Tab View Handlers

        private void closetab_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < tabview.TabCount; ++i)
            {
                if (tabview.GetTabRect(i).Contains(mea.Location))
                {
                    if (tabview.Controls[i].Controls[0] is SearchViewControl)
                    {
                        currentSearchControl = null;
                    }
                    (tabview.Controls[i] as TabPage).Dispose();
                }
            }
        }

        private void closeAllTabsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabview.TabPages.Clear();
        }

        private void closeothertabs_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < tabview.TabCount; ++i)
            {
                if (tabview.GetTabRect(i).Contains(mea.Location))
                {
                    tabview.SelectedTab = tabview.Controls[i] as TabPage;
                }
            }
            while (tabview.Controls.Count > 1)
            {
                foreach (TabPage tab in tabview.Controls)
                {
                    if (tab != tabview.SelectedTab)
                    {
                        if (tab.Controls[0] is SearchViewControl)
                        {
                            currentSearchControl = null;
                        }
                        tab.Dispose();
                    }
                }
            }
        }

        private void tabview_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                mea = e;
                tabviewcontextmenu.Show(tabview, e.Location);
            }
        }

        #endregion Tab View Handlers

        internal AssemblyDefinition LoadAssembly(string name, PatcherForm form = null)
        {
            if (assemblydict.TryGetValue(name, out AssemblyDefinition assemblyDefinition))
            {
                return assemblyDefinition;
            }

            string file = $"{Path.GetFileNameWithoutExtension(name)}_Original{Path.GetExtension(name)}";
            string filename = Path.Combine(Project.Current.TargetDirectory, file);
            if (!File.Exists(filename))
            {
                string oldfilename = Path.Combine(Project.Current.TargetDirectory, name);
                if (!File.Exists(oldfilename))
                {
                    return null;
                }

                CreateOriginal(oldfilename, filename, form);
            }
            assemblyDefinition = AssemblyDefinition.ReadAssembly(filename, new ReaderParameters { AssemblyResolver = resolver });
            assemblydict.Add(name, assemblyDefinition);
            rassemblydict.Add(assemblyDefinition, name);
            return assemblyDefinition;
        }

        private void CreateOriginal(string oldfile, string newfile, PatcherForm form = null)
        {
            AssemblyDefinition assembly = AssemblyDefinition.ReadAssembly(oldfile, new ReaderParameters { AssemblyResolver = resolver });
            Deobfuscator deob = Deobfuscators.Find(assembly);
            if (deob != null)
            {
                DialogResult result = MessageBox.Show(this,
                    $"Assembly '{assembly.MainModule.Name}' appears to be obfuscated using '{deob.Name}', attempt to deobfuscate?", "uMod Patcher", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    // Deobfuscate
                    if (deob.Deobfuscate(assembly))
                    {
                        // Success
                        if (File.Exists(newfile))
                        {
                            File.Delete(newfile);
                        }

                        assembly.Write(newfile);
                        return;
                    }

                    MessageBox.Show(this, "Deobfuscation failed!", "uMod Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            File.Copy(oldfile, newfile);
        }

        private bool IsFileOriginal(string filename)
        {
            string name = Path.GetFileNameWithoutExtension(filename);
            const string postfix = "_Original";
            if (name != null && name.Length <= postfix.Length)
            {
                return false;
            }

            return name.Substring(name.Length - postfix.Length) == postfix;
        }

        public TreeView ObjectView => objectview;

        public string DefaultTarget { get; internal set; }

        private void PopulateInitialTree()
        {
            // Set status
            SetStatus("Loading project...");
            objectview.Nodes.Clear();

            // Add project settings
            TreeNode projectsettings = new TreeNode("Project Settings")
            {
                ImageKey = "cog_edit.png",
                SelectedImageKey = "cog_edit.png",
                Tag = "Project Settings"
            };
            objectview.Nodes.Add(projectsettings);

            _ = Task.Run(() =>
            {
                // Add hooks
                _ = Task.Run(async () =>
                {
                    //Get tree node with all hooks
                    TreeNode hooks = await GetHooks();

                    //Sort and add to main tree
                    await SortAndUpdate(hooks, 1);
                });

                // Add modifiers
                _ = Task.Run(async () =>
                {
                    TreeNode modifiers = await GetModifiers();

                    await SortAndUpdate(modifiers, 2);
                });

                // Add fields
                _ = Task.Run(async () =>
                {
                    TreeNode fields = await GetFields();

                    await SortAndUpdate(fields, 3);
                });

                // Add assemblies
                _ = Task.Run(async () =>
                {
                    TreeNode assemblies = await GetAssemblies();

                    await SortAndUpdate(assemblies, 4);
                });

                Invoke(new Action(() => ClearStatus()));
            });
        }

        private async Task<TreeNode> GetHooks()
        {
            return await Task.Run(() =>
            {
                // Add hooks top node
                TreeNode hooks = new TreeNode("Hooks")
                {
                    ImageKey = "lightning.png",
                    Name = "Hooks",
                    SelectedImageKey = "lightning.png",
                    Tag = "Hooks"
                };

                Dictionary<string, TreeNode> hookCategory = new Dictionary<string, TreeNode>();
                foreach (Manifest manifest in Project.Current.Manifests.Where(m => !string.IsNullOrEmpty(m.OriginalFile)))
                {
                    PopulateManifestHooks(manifest, hooks, hookCategory);
                }

                foreach (Hook hook in Project.Current.Manifests.Where(m => string.IsNullOrEmpty(m.OriginalFile)).SelectMany(m => m.Hooks).OrderBy(h => h.Name))
                {
                    PopulateHook(hooks, hook);
                }

                return hooks;
            });
        }

        private void PopulateManifestHooks(Manifest manifest, TreeNode hooks = null, Dictionary<string, TreeNode> hookCategory = null)
        {
            if (hooks == null)
            {
                hooks = objectview.Nodes["Hooks"];
            }

            if (!string.IsNullOrEmpty(manifest.Name))
            {
                if (manifest.Hooks.Count > 0)
                {
                    TreeNode manifestnode = GetOrCreateManifestNode(hooks, hookCategory, manifest);

                    foreach (Hook hook in manifest.Hooks.OrderBy(h => h.Name))
                    {
                        PopulateHook(manifestnode, hook);
                    }
                }
            }
        }

        private async Task<TreeNode> GetModifiers()
        {
            return await Task.Run(() =>
            {
                // Add modifiers top node
                TreeNode modifiers = new TreeNode("Modifiers")
                {
                    ImageKey = "lightning.png",
                    Name = "Modifiers",
                    SelectedImageKey = "lightning.png",
                    Tag = "Modifiers"
                };

                Dictionary<string, TreeNode> modifierCategory = new Dictionary<string, TreeNode>();
                foreach (Manifest manifest in Project.Current.Manifests.Where(m => !string.IsNullOrEmpty(m.OriginalFile)))
                {
                    PopulateManifestModifiers(manifest, modifiers, modifierCategory);
                }

                foreach (Modifier modifier in Project.Current.Manifests.Where(m => string.IsNullOrEmpty(m.OriginalFile)).SelectMany(m => m.Modifiers).OrderBy(m => m.Name))
                {
                    TreeNode categorynode = GetOrCreateModifierCategoryNode(modifiers, modifier.TypeName);
                    PopulateModifier(categorynode, modifier);
                }

                return modifiers;
            });
        }

        private void PopulateManifestModifiers(Manifest manifest, TreeNode modifiers = null, Dictionary<string, TreeNode> modifierCategory = null)
        {
            if (modifiers == null)
            {
                modifiers = objectview.Nodes["Modifiers"];
            }
            if (!string.IsNullOrEmpty(manifest.Name))
            {
                if (manifest.Modifiers.Count > 0)
                {
                    TreeNode manifestnode = GetOrCreateManifestNode(modifiers, modifierCategory, manifest);

                    foreach (Modifier modifier in manifest.Modifiers.OrderBy(m => m.Name))
                    {
                        TreeNode categorynode = GetOrCreateModifierCategoryNode(manifestnode, modifier.TypeName);
                        PopulateModifier(categorynode, modifier);
                    }
                }
            }
        }

        private TreeNode GetOrCreateModifierCategoryNode(TreeNode manifestnode, string typeName)
        {
            if (!manifestnode.Nodes.ContainsKey(typeName))
            {
                TreeNode categorynode = new TreeNode(typeName)
                {
                    Name = typeName,
                    Tag = "Type",
                    ImageKey = "folder.png",
                    SelectedImageKey = "folder.png"
                };

                manifestnode.Nodes.Add(categorynode);
                Sort(manifestnode.Nodes, false);
                Sort(categorynode.Nodes, false);

                return categorynode;
            }
            return manifestnode.Nodes[typeName];
        }

        private async Task<TreeNode> GetFields()
        {
            return await Task.Run(() =>
            {
                // Add fields top node
                TreeNode fields = new TreeNode("Fields")
                {
                    ImageKey = "lightning.png",
                    Name = "Fields",
                    SelectedImageKey = "lightning.png",
                    Tag = "Fields"
                };
                Dictionary<string, TreeNode> fieldCategory = new Dictionary<string, TreeNode>();

                foreach (Manifest manifest in Project.Current.Manifests.Where(m => !string.IsNullOrEmpty(m.OriginalFile)))
                {
                    PopulateManifestFields(manifest, fields, fieldCategory);
                }

                foreach (Field field in Project.Current.Manifests.Where(m => string.IsNullOrEmpty(m.OriginalFile)).SelectMany(m => m.Fields).OrderBy(f => f.Name))
                {
                    PopulateField(fields, field);
                }

                return fields;
            });
        }

        private void PopulateManifestFields(Manifest manifest, TreeNode fields = null, Dictionary<string, TreeNode> fieldCategory = null)
        {
            if (fields == null)
            {
                fields = objectview.Nodes["Fields"];
            }
            if (!string.IsNullOrEmpty(manifest.Name))
            {
                if (manifest.Fields.Count > 0)
                {
                    TreeNode manifestnode = GetOrCreateManifestNode(fields, fieldCategory, manifest);

                    foreach (Field field in manifest.Fields.OrderBy(f => f.Name))
                    {
                        PopulateField(manifestnode, field);
                    }
                }
            }
        }

        private void PopulateManifestNodes(Manifest manifest)
        {
            PopulateManifestHooks(manifest);
            PopulateManifestFields(manifest);
            PopulateManifestModifiers(manifest);
        }

        private async Task<TreeNode> GetAssemblies()
        {
            return await Task.Run(() =>
            {
                // Add assemblies
                TreeNode assemblies = new TreeNode("Assemblies")
                {
                    ImageKey = "folder.png",
                    SelectedImageKey = "folder.png"
                };

                List<TreeNode> assemblynodes = new List<TreeNode>();
                IEnumerable<string> files = Directory.GetFiles(Project.Current.TargetDirectory).Where(f => f.EndsWith(".dll") || f.EndsWith(".exe") && !Path.GetFileName(f).StartsWith(typeof(Program).Assembly.GetName().Name));
                foreach (string file in files)
                {
                    // Check if it is an original dll
                    if (IsFileOriginal(file))
                    {
                        continue;
                    }
                    // See if it has a manifest
                    string assemblyname = Path.GetFileNameWithoutExtension(file);
                    string assemblyfile = Path.GetFileName(file);
                    if (Project.Current.Manifests.Any(x => x.AssemblyName == assemblyfile))
                    {
                        // Get the manifest
                        // Manifest manifest = CurrentProject.Manifests.Single((x) => x.AssemblyName == assemblyname);

                        // Load the assembly
                        NodeAssemblyData data = new NodeAssemblyData
                        {
                            Included = true,
                            AssemblyName = assemblyfile,
                            Loaded = true,
                            Definition = LoadAssembly(assemblyfile)
                        };

                        // Create a node for it
                        TreeNode assembly = new TreeNode(assemblyname);
                        if (data.Definition == null)
                        {
                            assembly.ImageKey = "error.png";
                            assembly.SelectedImageKey = "error.png";
                        }
                        else
                        {
                            assembly.ImageKey = "accept.png";
                            assembly.SelectedImageKey = "accept.png";
                        }
                        assembly.Tag = data;
                        assemblynodes.Add(assembly);

                        // Populate
                        if (data.Definition != null)
                        {
                            PopulateAssemblyNode(assembly, data.Definition);
                        }
                    }
                    else
                    {
                        // Nope, just make an empty node for it
                        TreeNode assembly = new TreeNode(assemblyname)
                        {
                            ImageKey = "cross.png",
                            SelectedImageKey = "cross.png",
                            Tag = new NodeAssemblyData { Included = false, AssemblyName = assemblyfile }
                        };
                        assemblynodes.Add(assembly);
                    }
                }

                // Sort
                assemblynodes = assemblynodes.OrderBy(x => x.ImageKey).ThenBy(x => x.Text).ToList();

                // Add
                for (int i = 0; i < assemblynodes.Count; i++)
                {
                    assemblies.Nodes.Add(assemblynodes[i]);
                }

                return assemblies;
            });
        }

        private async Task SortAndUpdate(TreeNode node, int insertIndex)
        {
            await Task.Run(() =>
            {
                //Sort nodes
                Sort(node.Nodes, false);

                //Add to main tree
                Invoke(new Action(() => objectview.Nodes.Insert(insertIndex, node)));
            });
        }

        private static TreeNode GetOrCreateManifestNode(TreeNode patchNode, Dictionary<string, TreeNode> patchCategory, Manifest manifest)
        {
            if (!patchCategory.TryGetValue(manifest.Name, out TreeNode manifestnode))
            {
                patchCategory.Add(manifest.Name, manifestnode = new TreeNode(manifest.Name)
                {
                    Name = manifest.Name,
                    Tag = manifest
                });

                patchNode.Nodes.Add(manifestnode);
            }

            return manifestnode;
        }

        private TreeNode GetOrCreateManifestNode(TreeNode patchNode, Manifest manifest)
        {
            TreeNode manifestnode = GetManifestNode(manifest, patchNode.Nodes);

            if (manifestnode == null)
            {
                manifestnode = new TreeNode(manifest.Name)
                {
                    Name = manifest.Name,
                    Tag = manifest
                };

                patchNode.Nodes.Add(manifestnode);
                Sort(patchNode.Nodes, false);
            }

            return manifestnode;
        }

        private static void PopulateField(TreeNode fields, Field field)
        {
            TreeNode fieldnode = new TreeNode($"{field.TypeName}::{field.Name}");
            if (field.Flagged)
            {
                fieldnode.ImageKey = "script_error.png";
                fieldnode.SelectedImageKey = "script_error.png";
            }
            else
            {
                fieldnode.ImageKey = "script_lightning.png";
                fieldnode.SelectedImageKey = "script_lightning.png";
            }

            fieldnode.Tag = field;
            fields.Nodes.Add(fieldnode);
        }

        private static void PopulateModifier(TreeNode modifiers, Modifier modifier)
        {
            TreeNode modifiernode = new TreeNode(modifier.Name.Replace($"{modifier.TypeName}::", string.Empty));
            if (modifier.Flagged)
            {
                modifiernode.ImageKey = "script_error.png";
                modifiernode.SelectedImageKey = "script_error.png";
            }
            else
            {
                modifiernode.ImageKey = "script_lightning.png";
                modifiernode.SelectedImageKey = "script_lightning.png";
            }

            modifiernode.Tag = modifier;
            modifiers.Nodes.Add(modifiernode);
        }

        private static void PopulateHook(TreeNode hooks, Hook hook)
        {
            TreeNode category = new TreeNode(hook.HookCategory);
            if (hook.HookCategory != null)
            {
                category.ImageKey = "folder.png";
                category.Name = hook.HookCategory;
                category.SelectedImageKey = "folder.png";
                category.Tag = "Category";
                if (!hooks.Nodes.ContainsKey(hook.HookCategory))
                {
                    hooks.Nodes.Add(category);
                }
                else
                {
                    category = hooks.Nodes.Find(hook.HookCategory, true)[0];
                }
            }

            TreeNode hooknode = new TreeNode(hook.Name);
            if (hook.Flagged)
            {
                hooknode.ImageKey = "script_error.png";
                hooknode.SelectedImageKey = "script_error.png";
            }
            else
            {
                hooknode.ImageKey = "script_lightning.png";
                hooknode.SelectedImageKey = "script_lightning.png";
            }

            hooknode.Tag = hook;

            if (hook.HookCategory == null)
            {
                hooks.Nodes.Add(hooknode);
            }
            else
            {
                category.Nodes.Add(hooknode);
                if (!hook.Flagged)
                {
                    return;
                }

                category.ImageKey = "folder_flagged.png";
                category.SelectedImageKey = "folder_flagged.png";
            }

        }

        private void UpdateNodes(TreeNodeCollection nodes, string collection)
        {
            //Clear nodes from visible node
            objectview.Nodes[collection].Nodes.Clear();

            //Add nodes to visible node
            foreach (TreeNode node in nodes)
            {
                objectview.Nodes[collection].Nodes.Add(node);
            }
        }

        private sealed class NamespaceData
        {
            public string Name { get; }
            public List<NamespaceData> ChildNamespaces { get; }
            public List<Mono.Cecil.TypeDefinition> ChildTypes { get; }
            public NamespaceData Parent { get; set; }

            public NamespaceData(string name)
            {
                Name = name;
                ChildNamespaces = new List<NamespaceData>();
                ChildTypes = new List<Mono.Cecil.TypeDefinition>();
            }
        }

        private void PopulateAssemblyNode(TreeNode root, AssemblyDefinition definition)
        {
            // Build collection of all types
            HashSet<Mono.Cecil.TypeDefinition> alltypes = new HashSet<Mono.Cecil.TypeDefinition>(definition.Modules.SelectMany(x => x.GetTypes()));

            // Sort types into their namespaces
            Dictionary<string, NamespaceData> namespaces = new Dictionary<string, NamespaceData>();
            NamespaceData globalnamespace = new NamespaceData("");
            namespaces.Add("", globalnamespace);
            foreach (Mono.Cecil.TypeDefinition typedef in alltypes)
            {
                if (!namespaces.TryGetValue(typedef.Namespace, out NamespaceData nspcdata))
                {
                    nspcdata = new NamespaceData(typedef.Namespace);
                    namespaces.Add(nspcdata.Name, nspcdata);
                }
                if (typedef.Namespace == "")
                {
                    globalnamespace = nspcdata;
                }

                nspcdata.ChildTypes.Add(typedef);
            }

            // Setup namespace hierarchy
            bool done = false;
            while (!done)
            {
                done = true;
                foreach (KeyValuePair<string, NamespaceData> pair in namespaces)
                {
                    if (pair.Value.Parent == null && pair.Value != globalnamespace)
                    {
                        if (pair.Key.Contains('.'))
                        {
                            string[] spl = pair.Key.Split('.');
                            string[] splm = new string[spl.Length - 1];
                            Array.Copy(spl, splm, splm.Length);
                            string parent = string.Concat(splm);
                            if (!namespaces.TryGetValue(parent, out NamespaceData parentnamespace))
                            {
                                parentnamespace = new NamespaceData(parent);
                                namespaces.Add(parent, parentnamespace);
                                parentnamespace.ChildNamespaces.Add(pair.Value);
                                pair.Value.Parent = parentnamespace;
                                done = false;
                                break;
                            }
                            parentnamespace.ChildNamespaces.Add(pair.Value);
                            pair.Value.Parent = parentnamespace;
                        }
                        else
                        {
                            globalnamespace.ChildNamespaces.Add(pair.Value);
                            pair.Value.Parent = globalnamespace;
                        }
                    }
                }
            }

            // Populate tree
            PopulateAssemblyNode(root, globalnamespace, true);
        }

        private void PopulateAssemblyNode(TreeNode root, NamespaceData namespacedata, bool isglobal = false)
        {
            // Add global namespace node if needed
            TreeNode typeparent;
            if (isglobal)
            {
                typeparent = new TreeNode("-")
                {
                    ImageKey = "namespace.png",
                    SelectedImageKey = "namespace.png"
                };
                root.Nodes.Add(typeparent);
            }
            else
            {
                typeparent = root;
            }

            // Add all namespaces
            NamespaceData[] namespaces = namespacedata.ChildNamespaces.ToArray();
            Array.Sort(namespaces, (a, b) => Comparer<string>.Default.Compare(a.Name, b.Name));
            for (int i = 0; i < namespaces.Length; i++)
            {
                // Get namespace
                NamespaceData data = namespaces[i];
                string[] spl = data.Name.Split('.');
                string subname = spl[spl.Length - 1];

                // Create a node for it
                TreeNode namespacenode = new TreeNode(subname)
                {
                    ImageKey = "namespace.png",
                    SelectedImageKey = "namespace.png"
                };
                root.Nodes.Add(namespacenode);

                // Recursively fill in children
                PopulateAssemblyNode(namespacenode, data);
            }

            // Add all types
            Mono.Cecil.TypeDefinition[] typedefs = namespacedata.ChildTypes.ToArray();
            Array.Sort(typedefs, (a, b) => Comparer<string>.Default.Compare(a.Name, b.Name));
            for (int i = 0; i < typedefs.Length; i++)
            {
                // Get type
                Mono.Cecil.TypeDefinition typedef = typedefs[i];
                if (!typedef.IsNested)
                {
                    // Create a node for it
                    TreeNode typenode = new TreeNode(typedef.Name);
                    string img = SelectIcon(typedef);
                    typenode.ImageKey = img;
                    typenode.SelectedImageKey = img;
                    typenode.Tag = typedef;
                    typeparent.Nodes.Add(typenode);

                    // Populate any nested types
                    PopulateAssemblyNode(typenode, typedef);
                }
            }
        }

        private void PopulateAssemblyNode(TreeNode root, Mono.Cecil.TypeDefinition roottype)
        {
            // Add all types
            Mono.Cecil.TypeDefinition[] typedefs = roottype.NestedTypes.ToArray();
            Array.Sort(typedefs, (a, b) => Comparer<string>.Default.Compare(a.Name, b.Name));
            for (int i = 0; i < typedefs.Length; i++)
            {
                // Get type
                Mono.Cecil.TypeDefinition typedef = typedefs[i];

                // Create a node for it
                TreeNode typenode = new TreeNode(typedef.Name);
                string img = SelectIcon(typedef);
                typenode.ImageKey = img;
                typenode.SelectedImageKey = img;
                typenode.Tag = typedef;
                root.Nodes.Add(typenode);

                // Populate any nested types
                PopulateAssemblyNode(typenode, typedef);
            }
        }

        private TreeNodeCollection GetTreeNodeCollection(string collection)
        {
            TreeNodeCollection clonedCollection = new TreeNode().Nodes;

            foreach (TreeNode node in objectview.Nodes[collection].Nodes)
            {
                clonedCollection.Add(node.Clone() as TreeNode);
            }

            return clonedCollection;
        }

        private string SelectIcon(Mono.Cecil.TypeDefinition typedef)
        {
            if (typedef.IsClass)
            {
                if (!typedef.IsPublic)
                {
                    return "Class-Private_493.png";
                }

                if (typedef.IsSealed)
                {
                    return "Class-Sealed_490.png";
                }

                return "Class_489.png";
            }

            if (typedef.IsInterface)
            {
                if (!typedef.IsPublic)
                {
                    return "Interface-Private_616.png";
                }

                if (typedef.IsSealed)
                {
                    return "Interface-Sealed_613.png";
                }

                return "Interface_612.png";
            }
            return "script_error.png";
        }

        private void AddTab(string name, Control control, object tag)
        {
            // Create tab
            TabPage tab = new TabPage
            {
                Text = name,
                Tag = tag
            };

            // Measure text size
            //var size = tab.CreateGraphics().MeasureString(tab.Text, tab.Font);
            //tab.Width = (int)size.Width + 100;

            // Setup child control
            if (control != null)
            {
                tab.Controls.Add(control);
                control.Dock = DockStyle.Fill;
            }

            if (control is IPatchView patchView)
            {
                patchView.Tab = tab;
            }

            // Add tab and select
            tabview.TabPages.Add(tab);
            tabview.SelectedTab = tab;
        }

        private void VerifyProject()
        {
            List<Notification> notifications = new List<Notification>();
            // Step 1: Check all included assemblies are intact
            // Step 2: Check all hooks are intact
            // Step 3: Check all modifiers are intact
            //int missingassemblies = 0, missingmethods = 0, changedmethods = 0, changedfields = 0, changedmodmethods = 0, changedproperties = 0, changednewfields = 0;
            foreach (Manifest manifest in Project.Current.Manifests)
            {
                if (manifest.AssemblyName == null)
                {
                    continue;
                }
                AssemblyDefinition assdef = LoadAssembly(manifest.AssemblyName);
                if (assdef == null)
                {
                    notifications.Add(new AssemblyMissingNotification(manifest.AssemblyName));
                    foreach (Hook hook in manifest.Hooks)
                    {
                        hook.Flagged = true;
                    }
                }
                else
                {
                    foreach (Hook hook in manifest.Hooks)
                    {
                        MethodDefinition method = GetMethod(hook.AssemblyName, hook.TypeName, hook.Signature, out bool shouldFlag);
                        if (method == null)
                        {
                            notifications.Add(new MethodMissingNotification(hook));
                            hook.Flagged = true;
                        }
                        else
                        {
                            string hash = new IlWeaver(method.Body).Hash;
                            if (hash != hook.MSILHash)
                            {
                                if (shouldFlag)
                                {
                                    hook.Signature = Utility.GetMethodSignature(method);
                                }
                                notifications.Add(new MethodChangedNotification(hook));
                                hook.MSILHash = hash;
                                hook.Flagged = true;
                            }
                            else if (shouldFlag)
                            {
                                notifications.Add(new MethodChangedNotification(hook));
                                hook.Signature = Utility.GetMethodSignature(method);
                                hook.Flagged = true;
                            }
                        }
                    }

                    foreach (Modifier modifier in manifest.Modifiers)
                    {
                        switch (modifier.Type)
                        {
                            case ModifierType.Field:
                                FieldDefinition fielddef = GetField(modifier.AssemblyName, modifier.TypeName, modifier.Name, modifier.Signature);
                                if (fielddef == null)
                                {
                                    notifications.Add(new FieldModificationChangedNotification(modifier));
                                    modifier.Flagged = true;
                                }
                                break;

                            case ModifierType.Method:
                                MethodDefinition methoddef = GetModifier(modifier.AssemblyName, modifier.TypeName, modifier.Signature);
                                if (methoddef == null)
                                {
                                    notifications.Add(new MethodModificationChangedNotification(modifier));
                                    modifier.Flagged = true;
                                }
                                break;

                            case ModifierType.Property:
                                PropertyDefinition propertydef = GetProperty(modifier.AssemblyName, modifier.TypeName, modifier.Name, modifier.Signature);
                                if (propertydef == null)
                                {
                                    notifications.Add(new PropertyChangedNotification(modifier));
                                    modifier.Flagged = true;
                                }
                                break;
                        }
                    }

                    foreach (Field field in manifest.Fields)
                    {
                        try
                        {
                            if (field.IsValid())
                            {
                                continue;
                            }
                        }
                        catch (FieldInvalidException ex)
                        {
                            MessageBox.Show(ex.Text, ex.Header, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        notifications.Add(new NewFieldChangedNotification(field));
                        field.Flagged = true;
                    }
                }
            }

            if (notifications.Count > 0)
            {
                NotificationsControl notificationspanel = new NotificationsControl();
                notificationspanel.MainForm = this;
                notificationspanel.Notifications = notifications;
                notificationspanel.PopulateNotifications();
                AddTab("Patch Flags", notificationspanel, notificationspanel);
            }
        }

        public void SaveProject()
        {
            foreach (TabPage tab in tabview.TabPages)
            {
                if (tab.Controls.Count > 0 && tab.Controls[0] is IPatchView patchView && patchView.IsChanged)
                {
                    patchView.Apply();
                }
            }

            Project.Current.Save(CurrentProjectFilename);
            if (HistoryIndex > -1)
            {
                HistoryItem historyItem = History[HistoryIndex];
                historyItem.Saved = true;
            }
            ClearStatus();
        }

        private bool CategoryExists(string label, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Hooks"].Nodes;
            }

            foreach (TreeNode t in nodes)
            {
                if (t.Tag.ToString() == "Category" && t.Text.Equals(label))
                {
                    return true;
                }

                if (CategoryExists(label, t.Nodes))
                {
                    return true;
                }
            }

            return false;
        }

        private void Sort(TreeNodeCollection nodes, bool subNodes = true)
        {
            bool sorting = true;
            while (sorting)
            {
                sorting = false;
                int i;
                for (i = 1; i < nodes.Count; i++)
                {
                    if (subNodes)
                    {
                        if (nodes[i - 1].Nodes.Count > 0)
                        {
                            Sort(nodes[i - 1].Nodes);
                        }

                        if (nodes[i].Nodes.Count > 0)
                        {
                            Sort(nodes[i].Nodes);
                        }
                    }

                    if (CompareTreeNodes(nodes[i], nodes[i - 1]) >= 0)
                    {
                        continue;
                    }

                    SwapTreeNodes(nodes, i, i - 1);
                    sorting = true;
                }

                if (i == 1 && nodes.Count == 1 && subNodes)
                {
                    if (nodes[0].Nodes.Count > 0)
                    {
                        Sort(nodes[0].Nodes);
                    }
                }
            }
        }

        private int CompareTreeNodes(TreeNode a, TreeNode b)
        {
            if (a.Tag.GetType().BaseType == b.Tag.GetType().BaseType)
            {
                return string.CompareOrdinal(a.Text, b.Text);
            }

            return a.Tag is string ? -1 : 1;
        }

        private void SwapTreeNodes(TreeNodeCollection collection, int a, int b)
        {
            TreeNode aNode = collection[a];
            TreeNode bNode = collection[b];
            collection.Remove(aNode);
            collection.Remove(bNode);
            collection.Insert(a, bNode);
            collection.Insert(b, aNode);
        }

        #region Code Interface

        /// <summary>
        /// Opens the specified project
        /// </summary>
        /// <param name="paths"></param>
        public void OpenProject(params string[] paths)
        {
            bool merge = false;
            // Ask to add to existing project or start new project
            string ext = Path.GetExtension(paths[0]);
            if (Project.Current != null && CurrentProjectFilename != Path.GetDirectoryName(paths[0]) && ext == ".opj")
            {
                DialogResult dr = MessageBox.Show("Add patches to existing project or open a new project?",
                          "Append to existing project", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        merge = true;
                        break;
                    case DialogResult.No:
                        // Close current project
                        CloseProject();
                        break;
                }
            }

            // Open new project data
            CurrentProjectFilename = Path.GetDirectoryName(paths[0]);

            try
            {
                Project project = Project.Load(paths);
                if (merge)
                {
                    Project.Current.Merge(project);
                }
                else
                {
                    Project.Current = project;
                }
            }
            catch (ProjectInvalidException ex)
            {
                MessageBox.Show(ex.Text, ex.Header, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (Project.Current == null)
            {
                return;
            }

            string title = string.Format("{0} - uMod Patcher - Version {1}", Project.Current.GetPrimaryManifest().Name, version);
            Text = title.Slice(0, title.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase));

            Project.Current.CorePath = CorePath;

            if (string.IsNullOrEmpty(Project.Current.TargetDirectory))
            {
                Project.Current.TargetDirectory = !string.IsNullOrEmpty(DefaultTarget) ? DefaultTarget : Environment.CurrentDirectory;
            }

            if (!Directory.Exists(Project.Current.TargetDirectory))
            {
                MessageBox.Show("Project target missing", "Please choose a server installation managed assembly directory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                // allow user to select new directory
                DialogResult result = openmanageddialog.ShowDialog(this);
                if (result == DialogResult.OK && Directory.Exists(openmanageddialog.SelectedPath))
                {
                    Project.Current.TargetDirectory = openmanageddialog.SelectedPath;
                }
                else
                {
                    SetStatus($"{Project.Current.TargetDirectory} specified in project file does not exist!");
                    MessageBox.Show(this, $"{Project.Current.TargetDirectory} does not exist!", "Directory Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    // Add project settings
                    TreeNode projectsettings = new TreeNode("Project Settings")
                    {
                        ImageKey = "cog_edit.png",
                        SelectedImageKey = "cog_edit.png",
                        Tag = "Project Settings"
                    };
                    objectview.Nodes.Add(projectsettings);
                    return;
                }
            }
            resolver = Project.Current.GetAssemblyResolver();

            // Verify
            VerifyProject();

            // Populate tree
            PopulateInitialTree();

            // Enable the patch button
            patchtool.Enabled = true;
            windowsToolStripMenuItem.Enabled = true;
            linuxToolStripMenuItem.Enabled = true;

            // Add the file to the MRU
            foreach (string path in paths)
            {
                mruManager.AddOrUpdate(path);
            }
        }

        /// <summary>
        /// Closes the current project
        /// </summary>
        public void CloseProject()
        {
            // Clear the tree and tabs
            objectview.Nodes.Clear();
            tabview.Controls.Clear();

            // Set project to null
            Project.Current = null;
            CurrentProjectFilename = null;

            // Clear the assembly dictionary
            assemblydict.Clear();

            // Clear history
            History.Clear();
            HistoryIndex = -1;
            string title = string.Format("uMod Patcher - Version {0}", version);
            Text = title.Slice(0, title.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase));
        }

        public void GotoType(Mono.Cecil.TypeDefinition typeDefinition)
        {
            // Check if it is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is ClassViewControl control && control.TypeDef == typeDefinition)
                {
                    tabview.SelectedTab = tabpage;
                    return;
                }
            }

            if (typeDefinition.IsClass)
            {
                ClassViewControl classview = new ClassViewControl
                {
                    TypeDef = typeDefinition,
                    MainForm = this
                };
                AddTab(typeDefinition.FullName, classview, classview);
            }
        }

        Dictionary<Patch, Patch> LastPatches = new Dictionary<Patch, Patch>();

        /// <summary>
        /// Opens or focuses the specified hook view
        /// </summary>
        /// <param name="hook"></param>
        public void GotoHook(Hook hook)
        {
            // Check if it is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is HookViewControl control && control.Hook == hook)
                {
                    tabview.SelectedTab = tabpage;
                    return;
                }
            }

            // save original hook
            SaveOriginalPatch(hook);

            // Create
            HookViewControl view = new HookViewControl
            {
                Hook = hook,
                MainForm = this,
                Dock = DockStyle.Fill
            };
            AddTab(hook.Name, view, view);
        }

        private void SaveOriginalPatch(Patch patch)
        {
            if (!LastPatches.TryGetValue(patch, out _))
            {
                LastPatches.Add(patch, patch.DeepClone());
            }
            else
            {
                LastPatches[patch] = patch.DeepClone();
            }
        }

        /// <summary>
        /// Opens or focuses the specified modifier view
        /// </summary>
        /// <param name="modifier"></param>
        public void GotoModifier(Modifier modifier)
        {
            // Check if it is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is ModifierViewControl control && control.Modifier == modifier)
                {
                    tabview.SelectedTab = tabpage;
                    return;
                }
            }

            // save original modifier
            SaveOriginalPatch(modifier);

            // Create
            ModifierViewControl view = new ModifierViewControl
            {
                Modifier = modifier,
                MainForm = this,
                Dock = DockStyle.Fill
            };
            AddTab(modifier.Name, view, view);
        }

        /// <summary>
        /// Opens or focuses the specified field view
        /// </summary>
        /// <param name="field"></param>
        public void GotoField(Field field)
        {
            // Check if it is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is FieldViewControl control && control.Field == field)
                {
                    tabview.SelectedTab = tabpage;
                    return;
                }
            }

            // save original field
            SaveOriginalPatch(field);

            // Create
            FieldViewControl view = new FieldViewControl
            {
                Field = field,
                MainForm = this,
                Dock = DockStyle.Fill
            };
            AddTab($"{field.TypeName}::{field.Name}", view, view);
        }

        public void RefreshPatch(Patch patch)
        {
            if (patch is Hook hook)
            {
                RefreshHook(hook);
            }
            else if (patch is Modifier modifier)
            {
                RefreshModifier(modifier);
            }
            else if (patch is Field field)
            {
                RefreshField(field);
            }
        }

        /// <summary>
        /// Opens or focuses the specified hook view
        /// </summary>
        /// <param name="hook"></param>
        public void RefreshHook(Hook hook)
        {
            // Check if it is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is HookViewControl control && control.Hook.Equals(hook))
                {
                    HookViewControl view = new HookViewControl
                    {
                        Hook = hook,
                        MainForm = this,
                        Dock = DockStyle.Fill
                    };

                    tabpage.Controls.Clear();
                    tabpage.Controls.Add(view);
                    return;
                }
            }
        }

        /// <summary>
        /// Opens or focuses the specified modifier view
        /// </summary>
        /// <param name="modifier"></param>
        public void RefreshModifier(Modifier modifier)
        {
            // Check if it is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is ModifierViewControl control && control.Modifier.Equals(modifier))
                {
                    ModifierViewControl view = new ModifierViewControl
                    {
                        Modifier = modifier,
                        MainForm = this,
                        Dock = DockStyle.Fill
                    };

                    tabpage.Controls.Clear();
                    tabpage.Controls.Add(view);
                    return;
                }
            }
        }

        /// <summary>
        /// Opens or focuses the specified field view
        /// </summary>
        /// <param name="field"></param>
        public void RefreshField(Field field)
        {
            // Check if it is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is FieldViewControl control && control.Field.Equals(field))
                {
                    FieldViewControl view = new FieldViewControl
                    {
                        Field = field,
                        MainForm = this,
                        Dock = DockStyle.Fill
                    };

                    tabpage.Controls.Clear();
                    tabpage.Controls.Add(view);
                    return;
                }
            }
        }

        /// <summary>
        /// Adds a hook to the current project
        /// </summary>
        /// <param name="hook"></param>
        public void AddHook(Hook hook, bool updateHistory = true, string manifestName = null)
        {
            Manifest manifest = GetPatchManifest(hook, manifestName);
            manifest.Hooks.Add(hook);
            //Project.Current.Save(CurrentProjectFilename);

            if (objectview.Nodes.IndexOfKey("Hooks") == -1)
            {
                return;
            }

            TreeNode hooknode = new TreeNode(hook.Name);
            if (hook.Flagged)
            {
                hooknode.ImageKey = "script_error.png";
                hooknode.SelectedImageKey = "script_error.png";
            }
            else
            {
                hooknode.ImageKey = "script_lightning.png";
                hooknode.SelectedImageKey = "script_lightning.png";
            }
            hooknode.Tag = hook;

            TreeNode manifestNode = GetOrCreateManifestNode(manifest, "Hooks");

            if (string.IsNullOrWhiteSpace(hook.HookCategory))
            {
                manifestNode.Nodes.Add(hooknode);
            }
            else
            {
                TreeNode category = null;
                foreach (TreeNode node in manifestNode.Nodes)
                {
                    if (node.Text == hook.HookCategory)
                    {
                        category = node;
                        break;
                    }
                }

                if (category is null)
                {
                    return;
                }

                category.Nodes.Add(hooknode);
            }

            if (updateHistory)
            {
                AddHistory(new HookChanged(null, hook.DeepClone()));
            }
        }

        private TreeNode GetManifestNode(Manifest manifest, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Manifest nodeManifest && manifest == nodeManifest)
                {
                    return node;
                }

                if (node.Tag.ToString() == "Category")
                {
                    continue;
                }

                if (node.Nodes.Count > 0)
                {
                    TreeNode result = GetManifestNode(manifest, node.Nodes);
                    if (result != null)
                    {
                        return result;
                    }
                }
            }

            return null;
        }

        private List<TreeNode> GetManifestNodes(Manifest manifest, TreeNodeCollection nodes = null, List<TreeNode> result = null)
        {
            result = result ??= new List<TreeNode>();
            if (nodes == null)
            {
                nodes = objectview.Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Manifest nodeManifest && manifest == nodeManifest)
                {
                    result.Add(node);
                }
                else if (node.Tag.ToString() == "Category")
                {
                    continue;
                }

                if (node.Nodes.Count > 0)
                {
                    GetManifestNodes(manifest, node.Nodes, result);
                }
            }

            return result;
        }

        private List<string> GetManifestNodeParents(Manifest manifest, TreeNodeCollection nodes = null, List<string> result = null)
        {
            result = result ??= new List<string>();
            if (nodes == null)
            {
                nodes = objectview.Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Manifest nodeManifest && manifest == nodeManifest)
                {
                    result.Add(node.Parent.Tag.ToString());
                }
                else if (node.Tag?.ToString() == "Category")
                {
                    continue;
                }

                if (node.Nodes.Count > 0)
                {
                    GetManifestNodeParents(manifest, node.Nodes, result);
                }
            }

            return result;
        }

        public void AddPatch(Patch patch, bool updateHistory = true, string manifestName = null)
        {
            if (patch is Hook hook)
            {
                AddHook(hook, updateHistory, manifestName);
            }
            else if (patch is Modifier modifier)
            {
                AddModifier(modifier, updateHistory, manifestName);
            }
            else if (patch is Field field)
            {
                AddField(field, updateHistory, manifestName);
            }
        }

        public void RemovePatch(Patch patch, bool updateHistory = true)
        {
            if (patch is Hook hook)
            {
                RemoveHook(hook, updateHistory);
            }
            else if (patch is Modifier modifier)
            {
                RemoveModifier(modifier, updateHistory);
            }
            else if (patch is Field field)
            {
                RemoveField(field, updateHistory);
            }
        }

        private Manifest GetPatchManifest(Patch patch, string manifestName = null)
        {
            Manifest result = null;
            if (!string.IsNullOrEmpty(manifestName) && Project.Current.HasManifest(manifestName))
            {
                result = Project.Current.GetManifest(manifestName);
            }

            if (result == null && Project.Current.HasMultipleAssemblyManifests(patch.AssemblyName))
            {
                List<string> potentialManifests = Project.Current.Manifests.Where(m => m.AssemblyName == patch.AssemblyName).Select(m => m.Name).ToList();
                result = AssemblyManifestPrompt(potentialManifests);
            }

            if (result == null)
            {
                result = Project.Current.GetPrimaryManifest();
            }

            return result;
        }

        private Manifest AssemblyManifestPrompt(List<string> potentialManifests)
        {
            string manifestResult = null;
            DialogResult result = FormUtility.ShowListInputDialog("Choose assembly manifest", potentialManifests, ref manifestResult);
            if (result == DialogResult.OK)
            {
                return Project.Current.GetManifest(manifestResult);
            }

            return null;
        }

        /// <summary>
        /// Adds a modifier to the current project
        /// </summary>
        /// <param name="modifier"></param>
        public void AddModifier(Modifier modifier, bool updateHistory = true, string manifestName = null)
        {
            Manifest manifest = GetPatchManifest(modifier, manifestName);
            manifest.Modifiers.Add(modifier);
            //Project.Current.Save(CurrentProjectFilename);

            if (objectview.Nodes.IndexOfKey("Modifiers") == -1)
            {
                return;
            }

            TreeNode manifestNode = GetOrCreateManifestNode(manifest, "Modifiers");
            TreeNode modifiernode = new TreeNode(modifier.Name);

            if (modifier.Flagged)
            {
                modifiernode.ImageKey = "script_error.png";
                modifiernode.SelectedImageKey = "script_error.png";
            }
            else
            {
                modifiernode.ImageKey = "script_lightning.png";
                modifiernode.SelectedImageKey = "script_lightning.png";
            }
            modifiernode.Tag = modifier;
            manifestNode.Nodes.Add(modifiernode);
            Sort(modifiernode.Nodes);

            if (updateHistory)
            {
                AddHistory(new ModifierChanged(null, modifier.DeepClone()));
            }
        }

        private TreeNode GetOrCreateManifestNode(Manifest manifest, string topNode)
        {
            TreeNode manifestNode = GetManifestNode(manifest, objectview.Nodes[topNode].Nodes);
            if (manifestNode == null)
            {
                manifestNode = new TreeNode(manifest.Name)
                {
                    Tag = manifest,
                };

                objectview.Nodes[topNode].Nodes.Insert(0, manifestNode);
            }

            return manifestNode;
        }

        /// <summary>
        /// Adds a modifier to the current project
        /// </summary>
        /// <param name="field"></param>
        public void AddField(Field field, bool updateHistory = true, string manifestName = null)
        {
            Manifest manifest = GetPatchManifest(field, manifestName);
            manifest.Fields.Add(field);
            //Project.Current.Save(CurrentProjectFilename);

            if (objectview.Nodes.IndexOfKey("Fields") == -1)
            {
                return;
            }

            TreeNode manifestNode = GetOrCreateManifestNode(manifest, "Fields");
            TreeNode fieldnode = new TreeNode($"{field.TypeName}::{field.Name}");

            if (field.Flagged)
            {
                fieldnode.ImageKey = "script_error.png";
                fieldnode.SelectedImageKey = "script_error.png";
            }
            else
            {
                fieldnode.ImageKey = "script_lightning.png";
                fieldnode.SelectedImageKey = "script_lightning.png";
            }
            fieldnode.Tag = field;
            manifestNode.Nodes.Add(fieldnode);
            Sort(fieldnode.Nodes);

            if (updateHistory)
            {
                AddHistory(new FieldChanged(null, field.DeepClone()));
            }
        }

        public void CloneHook(Hook hook, string manifestName = null)
        {
            Hook newhook = Activator.CreateInstance(hook.GetType()) as Hook;
            newhook.Name = hook.Name + "(Clone)";
            newhook.HookName = hook.HookName + "(Clone)";
            newhook.AssemblyName = hook.AssemblyName;
            newhook.TypeName = hook.TypeName;
            newhook.Signature = hook.Signature;
            newhook.Flagged = hook.Flagged;
            newhook.MSILHash = hook.MSILHash;
            newhook.HookCategory = hook.HookCategory;
            newhook.BaseHook = hook;
            AddHook(newhook, true, manifestName);
            GotoHook(newhook);
        }

        public void TryRemovePatch(Patch patch, bool updateHistory = true)
        {
            DialogResult result = MessageBox.Show(this, "Are you sure you want to remove this patch?", "uMod Patcher", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                RemovePatch(patch, updateHistory);
            }
        }

        /// <summary>
        /// Removes a hook from the current project
        /// </summary>
        /// <param name="hook"></param>
        public void RemoveHook(Hook hook, bool updateHistory = true)
        {
            Manifest manifest = GetPatchManifest(hook, true);
            manifest.Hooks.Remove(hook);

            List<Hook> childHooks = manifest.Hooks.Where(h => h.BaseHook == hook).ToList();
            foreach (Hook childHook in childHooks)
            {
                childHook.Flagged = true;
                childHook.BaseHook = null;
                UpdateHook(childHook, false);

                foreach (TabPage tabpage in tabview.TabPages)
                {
                    if (tabpage.Tag is HookViewControl control && control.Hook == childHook)
                    {
                        control.BaseHookSearchButton.Enabled = false;
                        control.BaseHookDropdown.Items.Clear();
                    }
                }
            }

            //Project.Current.Save(CurrentProjectFilename);

            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is HookViewControl control && control.Hook == hook)
                {
                    tabview.TabPages.Remove(tabpage);
                    break;
                }
            }

            currentSearchControl?.RemoveSearchItem(hook);
            RemoveHookNode(hook);

            if (updateHistory)
            {
                AddHistory(new HookChanged(hook.DeepClone(), null, manifest));
            }
        }

        private void RemoveHookNode(Hook hook, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Hooks"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag == hook)
                {
                    node.Remove();
                    break;
                }

                RemoveHookNode(hook, node.Nodes);

                string tag = node.Tag as string;
                if (string.IsNullOrEmpty(tag))
                {
                    continue;
                }

                if (tag != "Category")
                {
                    continue;
                }

                foreach (TreeNode subnode in node.Nodes)
                {
                    if (subnode.Tag != hook)
                    {
                        continue;
                    }

                    subnode.Remove();
                    break;
                }
            }
        }

        public void ReplacePatchNode(Patch patch, TreeNodeCollection nodes = null)
        {
            if (patch is Hook hook)
            {
                ReplaceHookNode(hook, nodes);
            }
            else if (patch is Modifier modifier)
            {
                ReplaceModifierNode(modifier, nodes);
            }
            else if (patch is Field field)
            {
                ReplaceFieldNode(field, nodes);
            }
        }

        private void ReplaceHookNode(Hook hook, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Hooks"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Hook && node.Tag.Equals(hook))
                {
                    node.Tag = hook;
                    break;
                }

                ReplaceHookNode(hook, node.Nodes);
            }
        }

        private TreeNode GetPatchNode(Patch patch, TreeNodeCollection nodes = null)
        {
            switch (patch)
            {
                case Hook hook:
                    return GetHookNode(hook, nodes);
                case Modifier modifier:
                    return GetModifierNode(modifier, nodes);
                case Field field:
                    return GetFieldNode(field, nodes);
            }

            return null;
        }

        private TreeNode GetHookNode(Hook hook, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Hooks"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag.Equals(hook))
                {
                    return node;
                }

                TreeNode childNode = GetHookNode(hook, node.Nodes);
                if (childNode != null)
                {
                    return childNode;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes a modifier from the current project
        /// </summary>
        /// <param name="modifier"></param>
        public void RemoveModifier(Modifier modifier, bool updateHistory = true)
        {
            Manifest manifest = GetPatchManifest(modifier, true);
            manifest.Modifiers.Remove(modifier);
            //Project.Current.Save(CurrentProjectFilename);

            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is ModifierViewControl control && control.Modifier == modifier)
                {
                    tabview.TabPages.Remove(tabpage);
                    break;
                }
            }

            currentSearchControl?.RemoveSearchItem(modifier);
            RemoveModifierNode(modifier);

            if (updateHistory)
            {
                AddHistory(new ModifierChanged(modifier.DeepClone(), null, manifest));
            }
        }

        private void RemoveModifierNode(Modifier modifier, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Modifiers"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag == modifier)
                {
                    node.Remove();
                    break;
                }

                RemoveModifierNode(modifier, node.Nodes);
            }
        }

        private void ReplaceModifierNode(Modifier modifier, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Modifiers"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Modifier && node.Tag.Equals(modifier))
                {
                    node.Tag = modifier;
                    break;
                }

                ReplaceModifierNode(modifier, node.Nodes);
            }
        }

        private TreeNode GetModifierNode(Modifier modifier, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Modifiers"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag.Equals(modifier))
                {
                    return node;
                }

                TreeNode childNode = GetModifierNode(modifier, node.Nodes);
                if (childNode != null)
                {
                    return childNode;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes a field from the current project
        /// </summary>
        /// <param name="field"></param>
        public void RemoveField(Field field, bool updateHistory = true)
        {
            Manifest manifest = GetPatchManifest(field, true);
            manifest.Fields.Remove(field);
            //Project.Current.Save(CurrentProjectFilename);

            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is FieldViewControl control && control.Field == field)
                {
                    tabview.TabPages.Remove(tabpage);
                    break;
                }
            }

            currentSearchControl?.RemoveSearchItem(field);
            RemoveFieldNode(field);

            if (updateHistory)
            {
                AddHistory(new FieldChanged(field.DeepClone(), null, manifest));
            }
        }

        private void RemoveFieldNode(Field field, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Fields"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag == field)
                {
                    node.Remove();
                    break;
                }

                RemoveFieldNode(field, node.Nodes);
            }
        }

        private void ReplaceFieldNode(Field field, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Fields"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag is Field && node.Tag.Equals(field))
                {
                    node.Tag = field;
                    break;
                }

                ReplaceFieldNode(field, node.Nodes);
            }
        }

        private TreeNode GetFieldNode(Field field, TreeNodeCollection nodes = null)
        {
            if (nodes == null)
            {
                nodes = objectview.Nodes["Fields"].Nodes;
            }

            foreach (TreeNode node in nodes)
            {
                if (node.Tag.Equals(field))
                {
                    return node;
                }

                TreeNode childNode = GetFieldNode(field, node.Nodes);
                if (childNode != null)
                {
                    return childNode;
                }
            }

            return null;
        }

        public void UpdatePatch(Patch patch, bool updateHistory = true)
        {
            if (patch is Hook hook)
            {
                UpdateHook(hook, updateHistory);
            }
            else if (patch is Modifier modifier)
            {
                UpdateModifier(modifier, updateHistory);
            }
            else if (patch is Field field)
            {
                UpdateField(field, updateHistory);
            }
        }

        /// <summary>
        /// Updates the UI for a hook
        /// </summary>
        /// <param name="hook"></param>
        public void UpdateHook(Hook hook, bool updateHistory = true)
        {
            Manifest manifest = GetPatchManifest(hook, true);
            List<Hook> leafHooks = manifest.Hooks.Where(h => h.BaseHook != null).ToList();

            if (hook.Flagged)
            {
                List<Hook> childHooks = leafHooks.Where(h => h.BaseHook == hook).ToList();
                foreach (Hook childHook in childHooks)
                {
                    childHook.Flagged = true;
                    UpdateHook(childHook, false);
                }
            }

            if (hook.BaseHook != null)
            {
                if (hook.BaseHook.Flagged && !hook.Flagged)
                {
                    hook.Flagged = true;
                    SetStatus($"Cannot unflag {hook.Name} because its base hook {hook.BaseHook.Name} is flagged", Color.OrangeRed);
                    return;
                }

                List<Hook> matchingBaseHooks = leafHooks.Where(h => h.BaseHook == hook.BaseHook).ToList();
                if (matchingBaseHooks.Count > 1 && !hook.Flagged)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (Hook matchingBaseHook in matchingBaseHooks)
                        sb.Append($", {matchingBaseHook.Name}");

                    hook.Flagged = true;
                    SetStatus($"Cannot unflag {hook.Name} because there are multiple hooks referencing the same base hook {hook.BaseHook.Name}: {sb}", Color.OrangeRed);
                    return;
                }
            }
            else
            {
                List<Hook> duplicateHooks = manifest.Hooks.Where(h => h.Signature.Equals(hook.Signature) && h.TypeName.Equals(hook.TypeName) && h.BaseHook == null).ToList();
                if (duplicateHooks.Count > 1 && !hook.Flagged)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (Hook duplicateHook in duplicateHooks)
                        sb.Append($", {duplicateHook.Name}");

                    hook.Flagged = true;
                    SetStatus($"Cannot unflag {hook.Name} because there are multiple base hooks referencing the same method: {sb}", Color.OrangeRed);
                    return;
                }
            }

            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is HookViewControl control)
                {
                    if (control.Hook == hook)
                    {
                        tabpage.Text = hook.Name;
                        if (hook.Flagged)
                        {
                            control.UnflagButton.Enabled = true;
                            control.FlagButton.Enabled = false;
                        }
                        else
                        {
                            control.UnflagButton.Enabled = false;
                            control.FlagButton.Enabled = true;
                        }
                    }

                    if (control.Hook.BaseHook == hook)
                    {
                        control.HandlePatch();
                    }
                }
            }

            if (updateHistory)
            {
                Hook clone = hook.DeepClone();
                if (LastPatches.TryGetValue(hook, out Patch oldpatch) && oldpatch is Hook oldhook)
                {
                    AddHistory(new HookChanged(oldhook.DeepClone(), clone, manifest));
                    LastPatches.Remove(hook);
                    LastPatches.Add(hook, clone);
                }
                else
                {
                    LastPatches.Add(hook, clone);
                }
            }
            else
            {
                if (LastPatches.TryGetValue(hook, out Patch oldpatch) && oldpatch is Hook oldhook)
                {
                    LastPatches.Remove(hook);
                    LastPatches.Add(hook, hook.DeepClone());
                }
                else
                {
                    LastPatches.Add(hook, hook.DeepClone());
                }
            }

            if (objectview.Nodes.IndexOfKey("Hooks") == -1)
            {
                return;
            }

            UpdateHookNodes(hook, objectview.Nodes["Hooks"].Nodes);
        }

        private void UpdateHookNodes(Hook hook, TreeNodeCollection hooks)
        {
            foreach (TreeNode node in hooks)
            {
                if (node.Tag is string tag && tag == "Category")
                {
                    TreeNode category = node as TreeNode;
                    bool flagged = false;
                    foreach (object subnode in category.Nodes)
                    {
                        if ((subnode as TreeNode)?.Tag == hook)
                        {
                            TreeNode treenode = subnode as TreeNode;

                            treenode.Text = hook.Name;
                            if (hook.Flagged)
                            {
                                treenode.ImageKey = "script_error.png";
                                treenode.SelectedImageKey = "script_error.png";
                            }
                            else
                            {
                                treenode.ImageKey = "script_lightning.png";
                                treenode.SelectedImageKey = "script_lightning.png";
                            }
                        }
                        if (((subnode as TreeNode)?.Tag as Hook).Flagged)
                        {
                            flagged = true;
                        }
                    }

                    if (flagged)
                    {
                        category.ImageKey = "folder_flagged.png";
                        category.SelectedImageKey = "folder_flagged.png";
                    }
                    else
                    {
                        category.ImageKey = "folder.png";
                        category.SelectedImageKey = "folder.png";
                    }

                    Sort(category.Nodes);
                }
                else if ((node as TreeNode)?.Tag == hook)
                {
                    TreeNode treenode = node as TreeNode;

                    treenode.Text = hook.Name;
                    if (hook.Flagged)
                    {
                        treenode.ImageKey = "script_error.png";
                        treenode.SelectedImageKey = "script_error.png";
                    }
                    else
                    {
                        treenode.ImageKey = "script_lightning.png";
                        treenode.SelectedImageKey = "script_lightning.png";
                    }
                    Sort(hooks);
                    break;
                }
                else if (node.Tag is Manifest)
                {
                    UpdateHookNodes(hook, node.Nodes);
                }
            }
        }

        /// <summary>
        /// Updates the UI for a modifier
        /// </summary>
        /// <param name="modifier"></param>
        /// <param name="batchUpdate"></param>
        public void UpdateModifier(Modifier modifier, bool updateHistory = true)
        {
            Manifest manifest = GetModifierManifest(modifier, true);
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is ModifierViewControl control && control?.Modifier == modifier)
                {
                    tabpage.Text = modifier.Name;
                    if (modifier.Flagged)
                    {
                        control.UnflagButton.Enabled = true;
                        control.FlagButton.Enabled = false;
                    }
                    else
                    {
                        control.UnflagButton.Enabled = false;
                        control.FlagButton.Enabled = true;
                    }
                }
            }

            if (updateHistory)
            {
                Modifier clone = modifier.DeepClone();
                if (LastPatches.TryGetValue(modifier, out Patch oldpatch) && oldpatch is Modifier oldmodifier)
                {
                    AddHistory(new ModifierChanged(oldmodifier, clone, manifest));
                    LastPatches.Remove(modifier);
                    LastPatches.Add(modifier, clone);
                }
                else
                {
                    LastPatches.Add(modifier, clone);
                }
            }
            else
            {
                if (LastPatches.TryGetValue(modifier, out Patch oldpatch) && oldpatch is Modifier oldmodifier)
                {
                    LastPatches.Remove(modifier);
                    LastPatches.Add(modifier, modifier.DeepClone());
                }
                else
                {
                    LastPatches.Add(modifier, modifier.DeepClone());
                }
            }

            if (objectview.Nodes.IndexOfKey("Modifiers") == -1)
            {
                return;
            }

            UpdateModifierNodes(modifier, objectview.Nodes["Modifiers"].Nodes);
        }

        private void UpdateModifierNodes(Modifier modifier, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Tag == modifier)
                {
                    TreeNode treenode = node as TreeNode;

                    treenode.Text = modifier.Name;
                    if (modifier.Flagged)
                    {
                        treenode.ImageKey = "script_error.png";
                        treenode.SelectedImageKey = "script_error.png";
                    }
                    else
                    {
                        treenode.ImageKey = "script_lightning.png";
                        treenode.SelectedImageKey = "script_lightning.png";
                    }
                    Sort(nodes);
                    break;
                }
                else if (node.Tag is Manifest)
                {
                    UpdateModifierNodes(modifier, node.Nodes);
                }
            }
        }

        /// <summary>
        /// Updates the UI for a modifier
        /// </summary>
        /// <param name="field"></param>
        /// <param name="batchUpdate"></param>
        public void UpdateField(Field field, bool updateHistory = true)
        {
            Manifest manifest = GetFieldManifest(field, true);
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if (tabpage.Tag is FieldViewControl control && control.Field == field)
                {
                    tabpage.Text = $"{field.TypeName}::{field.Name}";
                    if (field.Flagged)
                    {
                        control.UnflagButton.Enabled = true;
                        control.FlagButton.Enabled = false;
                    }
                    else
                    {
                        control.UnflagButton.Enabled = false;
                        control.FlagButton.Enabled = true;
                    }
                }
            }

            if (updateHistory)
            {
                Field clone = field.DeepClone();
                if (LastPatches.TryGetValue(field, out Patch oldpatch) && oldpatch is Field oldfield)
                {
                    AddHistory(new FieldChanged(oldfield, clone, manifest));
                    LastPatches.Remove(field);
                    LastPatches.Add(field, clone);
                }
                else
                {
                    LastPatches.Add(field, clone);
                }
            }
            else
            {
                if (LastPatches.TryGetValue(field, out Patch oldpatch) && oldpatch is Field oldfield)
                {
                    LastPatches.Remove(field);
                    LastPatches.Add(field, field.DeepClone());
                }
                else
                {
                    LastPatches.Add(field, field.DeepClone());
                }
            }

            if (objectview.Nodes.IndexOfKey("Fields") == -1)
            {
                return;
            }

            UpdateFieldNodes(field, objectview.Nodes["Fields"].Nodes);
        }

        private void UpdateFieldNodes(Field field, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Tag == field)
                {
                    node.Text = $"{field.TypeName}::{field.Name}";
                    if (field.Flagged)
                    {
                        node.ImageKey = "script_error.png";
                        node.SelectedImageKey = "script_error.png";
                    }
                    else
                    {
                        node.ImageKey = "script_lightning.png";
                        node.SelectedImageKey = "script_lightning.png";
                    }
                    Sort(nodes);
                    break;
                }
                else if (node.Tag is Manifest)
                {
                    UpdateFieldNodes(field, node.Nodes);
                }
            }
        }

        private bool BatchingHistory = false;
        private Queue<HistoryItem> LastHistoryItemBatch = new Queue<HistoryItem>();

        private void BeginHistoryBatch()
        {
            LastHistoryItemBatch.Clear();
            BatchingHistory = true;
        }

        private void EndHistoryBatch()
        {
            BatchingHistory = false;
            AddHistory(new HistoryItemBatch()
            {
                History = new Queue<HistoryItem>(LastHistoryItemBatch)
            });
        }

        public void UpdateAllHooks()
        {
            if (Project.Current != null)
            {
                BeginHistoryBatch();
                foreach (Hook hook in Project.Current.Manifests.SelectMany(m => m.Hooks))
                {
                    UpdateHook(hook, true);
                }
                EndHistoryBatch();
            }
        }

        public MethodDefinition GetMethod(string assemblyname, string typename, MethodSignature signature)
        {
            return GetMethod(assemblyname, typename, signature, out _);
        }

        /// <summary>
        /// Gets the method associated with the specified signature
        /// </summary>
        /// <param name="assemblyname"></param>
        /// <param name="typename"></param>
        /// <param name="signature"></param>
        public MethodDefinition GetMethod(string assemblyname, string typename, MethodSignature signature, out bool shouldFlag)
        {
            shouldFlag = false;
            if (!assemblydict.TryGetValue(assemblyname, out AssemblyDefinition assemblyDefinition))
            {
                return null;
            }

            try
            {
                Mono.Cecil.TypeDefinition type = assemblyDefinition.Modules.SelectMany(m => m.GetTypes()).Single(t => t.FullName == typename);

                MethodDefinition method = type.Methods.SingleOrDefault(m => Utility.GetMethodSignature(m).Equals(signature));
                if (method == null)
                {
                    method = GetClosestMethodDefinitions(type, signature);
                    shouldFlag = true;
                }

                return method;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private MethodDefinition GetClosestMethodDefinitions(Mono.Cecil.TypeDefinition typeDefinition, MethodSignature methodSignature)
        {
            List<MethodDefinition> results = new List<MethodDefinition>();
            // get methods that match name
            foreach (MethodDefinition method in typeDefinition.Methods)
            {
                if (method.Name.Equals(methodSignature.Name, StringComparison.InvariantCultureIgnoreCase))
                {
                    results.Add(method);
                }
            }

            // get methods that have the most number of matching parameters
            if (results.Count > 1)
            {
                results = results.OrderByDescending(x => MatchingSignature(x.Parameters, methodSignature)).ToList();
            }

            return results.FirstOrDefault();
        }

        private int MatchingSignature(IEnumerable<ParameterDefinition> parametersDefinitions, MethodSignature methodSignature)
        {
            int count = 0;

            foreach (ParameterDefinition paramDefinition in parametersDefinitions)
            {
                if (methodSignature.Parameters.Contains(paramDefinition.ParameterType.FullName))
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Gets the method associated with the specified signature
        /// </summary>
        /// <param name="assemblyname"></param>
        /// <param name="typename"></param>
        /// <param name="signature"></param>
        public MethodDefinition GetModifier(string assemblyname, string typename, ModifierSignature signature)
        {
            if (!assemblydict.TryGetValue(assemblyname, out AssemblyDefinition assdef))
            {
                return null;
            }

            try
            {
                Mono.Cecil.TypeDefinition type = assdef.Modules.SelectMany(m => m.GetTypes()).Single(t => t.FullName == typename);

                return type.Methods.Single(m => Utility.GetModifierSignature(m).Equals(signature));
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the field associated with the specified signature
        /// </summary>
        /// <param name="assemblyname"></param>
        /// <param name="typename"></param>
        /// <param name="name"></param>
        /// <param name="signature"></param>
        public FieldDefinition GetField(string assemblyname, string typename, string name, ModifierSignature signature)
        {
            if (!assemblydict.TryGetValue(assemblyname, out AssemblyDefinition assdef))
            {
                return null;
            }

            try
            {
                Mono.Cecil.TypeDefinition type = assdef.Modules.SelectMany(m => m.GetTypes()).Single(t => t.FullName == typename);

                return type.Fields.Single(m => Utility.GetModifierSignature(m).Equals(signature));
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the property associated with the specified signature
        /// </summary>
        /// <param name="assemblyname"></param>
        /// <param name="typename"></param>
        /// <param name="name"></param>
        /// <param name="signature"></param>
        public PropertyDefinition GetProperty(string assemblyname, string typename, string name, ModifierSignature signature)
        {
            if (!assemblydict.TryGetValue(assemblyname, out AssemblyDefinition assdef))
            {
                return null;
            }

            try
            {
                Mono.Cecil.TypeDefinition type = assdef.Modules.SelectMany(m => m.GetTypes()).Single(t => t.FullName == typename);

                return type.Properties.Single(m => Utility.GetModifierSignature(m).Equals(signature));
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the type associated with the specified signature
        /// </summary>
        /// <param name="assemblyname"></param>
        /// <param name="typename"></param>
        public Mono.Cecil.TypeDefinition GetType(string assemblyname, string typename)
        {
            if (!assemblydict.TryGetValue(assemblyname, out AssemblyDefinition assdef))
            {
                return null;
            }

            try
            {
                return assdef.Modules.SelectMany(m => m.GetTypes()).Single(t => t.FullName == typename);
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion Code Interface

        private void windowsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (windowsToolStripMenuItem.Checked)
            {
                Project.Current.Platform = HookOperatingSystem.All;
                windowsToolStripMenuItem.Checked = false;
            }
            else
            {
                Project.Current.Platform = HookOperatingSystem.Windows;
                windowsToolStripMenuItem.Checked = true;
            }
        }

        private void linuxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (linuxToolStripMenuItem.Checked)
            {
                Project.Current.Platform = HookOperatingSystem.All;
                linuxToolStripMenuItem.Checked = false;
            }
            else
            {
                Project.Current.Platform = HookOperatingSystem.Linux;
                linuxToolStripMenuItem.Checked = true;
            }
        }

        private void toolStripTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Search(searchTextBox.Text);
                e.SuppressKeyPress = true;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Search(searchTextBox.Text);
        }

        private SearchViewControl currentSearchControl = null;

        public void Search(string text)
        {
            SearchViewControl view = null;
            // Check if the tab is already open somewhere
            foreach (TabPage tabpage in tabview.TabPages)
            {
                if ((tabpage.Tag is SearchViewControl svControl))
                {
                    tabpage.Text = $"Search: {text}";
                    currentSearchControl = view = svControl;
                    tabview.SelectedTab = tabpage;
                }
            }

            // Create
            if (view == null)
            {
                currentSearchControl = view = new SearchViewControl
                {
                    MainForm = this,
                    Dock = DockStyle.Fill
                };

                AddTab($"Search: {text}", view, view);
            }

            view.Search(text);
        }

        private void collapseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (objectview.SelectedNode == null) return;
            foreach (TreeNode node in objectview.SelectedNode.Nodes)
            {
                node.Collapse(false);
            }
        }

        private void expandAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            objectview.SelectedNode?.ExpandAll();
        }

        private void newManifestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode node = objectview.SelectedNode;
            string str = node.Tag.ToString();
            if (node != null && (str == "Hooks" || str == "Fields" || str == "Modifiers"))
            {
                string newName = string.Empty;
                var result = FormUtility.ShowInputDialog($"Choose manifest name", ref newName);

                if (ManifestExists(newName))
                {
                    Manifest manifest = Project.Current.GetManifest(newName);
                    if (manifest != null)
                    {
                        GetOrCreateManifestNode(node, manifest);
                        node.Expand();
                    }
                    /*MessageBox.Show("A manifest with this name already exists!", "Invalid Manifest",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);*/

                    return;
                }

                if (result == DialogResult.OK)
                {
                    string dir = Project.Current.Manifests.FirstOrDefault()?.OriginalFile;
                    if (!string.IsNullOrEmpty(dir))
                    {
                        Manifest manifest = new Manifest();
                        manifest.OriginalFile = Path.Combine(Path.GetDirectoryName(dir), $"{newName}.opj");
                        Project.Current.Manifests.Add(manifest);
                        TreeNode manifestNode = new TreeNode(newName)
                        {
                            Tag = manifest,
                        };

                        node.Nodes.Insert(0, manifestNode);
                        node.Expand();
                        AddHistory(new ManifestChanged(null, manifest.DeepClone(), null, manifest.Name));
                        //objectview.Nodes["Modifiers"].Nodes.Insert(0, manifestNode1);
                        //objectview.Nodes["Fields"].Nodes.Insert(0, manifestNode2);
                    }
                }
            }
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (HistoryIndex < 0)
            {
                return;
            }

            if (HistoryIndex > (History.Count - 1))
            {
                HistoryIndex = History.Count - 1;
            }

            MoveToHistoryItem(History[HistoryIndex]);
            HistoryIndex--;
        }

        private void MoveToHistoryItem(HistoryItem item, bool undo = true)
        {
            Manifest manifest = null;
            int index = 0;
            switch (item)
            {
                case ManifestChanged manifestChanged:
                    Manifest before = manifestChanged.Before;
                    Manifest after = manifestChanged.After;
                    string beforeName = manifestChanged.BeforeName;
                    string afterName = manifestChanged.AfterName;
                    if (undo)
                    {
                        if (before == null && after != null) // created
                        {
                            // delete manifest
                            Manifest existingManifest = Project.Current.GetManifest(after.AssemblyName, afterName);
                            if (existingManifest != null)
                            {
                                Project.Current.DeleteManifest(existingManifest);
                                manifestChanged.HookTypes = GetManifestNodeParents(existingManifest);
                                RemoveManifestNodes(existingManifest);
                            }
                        }
                        else if (before != null && after == null) // deleted
                        {
                            // create manifest
                            Manifest newManifest = before.DeepClone();
                            newManifest.OriginalFile = Path.Combine(CurrentProjectFilename, $"{beforeName}.opj");
                            Project.Current.Manifests.Add(newManifest);
                            if (manifestChanged.HookTypes?.Count > 0)
                            {
                                foreach (string hookType in manifestChanged.HookTypes)
                                {
                                    GetOrCreateManifestNode(objectview.Nodes[hookType], newManifest);
                                }
                            }
                            else
                            {
                                GetOrCreateManifestNode(objectview.Nodes["Hooks"], newManifest);
                            }

                            PopulateManifestNodes(newManifest);
                        }
                        else if (before != null && after != null) // modified
                        {
                            // revert manifest
                            Manifest existingManifest = Project.Current.GetManifest(after.AssemblyName, afterName);
                            if (existingManifest != null)
                            {
                                Project.Current.DeleteManifest(existingManifest);
                            }
                            Manifest newManifest = before.DeepClone();
                            newManifest.OriginalFile = Path.Combine(CurrentProjectFilename, $"{beforeName}.opj");
                            Project.Current.Manifests.Add(newManifest);
                            ReplaceManifestNodes(newManifest);
                        }
                    }
                    else
                    {
                        if (before == null && after != null) // created
                        {
                            // re-create
                            Manifest newManifest = after.DeepClone();
                            newManifest.OriginalFile = Path.Combine(CurrentProjectFilename, $"{afterName}.opj");
                            Project.Current.Manifests.Add(newManifest);
                            if (manifestChanged.HookTypes?.Count > 0)
                            {
                                foreach (string hookType in manifestChanged.HookTypes)
                                {
                                    GetOrCreateManifestNode(objectview.Nodes[hookType], newManifest);
                                }
                            }
                            else
                            {
                                GetOrCreateManifestNode(objectview.Nodes["Hooks"], newManifest);
                            }
                            PopulateManifestNodes(newManifest);
                        }
                        else if (before != null && after == null) // deleted
                        {
                            // re-delete
                            Manifest existingManifest = Project.Current.GetManifest(before.AssemblyName, beforeName);
                            if (existingManifest != null)
                            {
                                Project.Current.DeleteManifest(existingManifest);
                                manifestChanged.HookTypes = GetManifestNodeParents(existingManifest);
                                RemoveManifestNodes(existingManifest);
                            }
                        }
                        else if (before != null && after != null) // modified
                        {
                            // revert undo
                            Manifest existingManifest = Project.Current.GetManifest(before.AssemblyName, beforeName);
                            if (existingManifest != null)
                            {
                                Project.Current.DeleteManifest(existingManifest);
                            }

                            Manifest newManifest = after.DeepClone();
                            newManifest.OriginalFile = Path.Combine(CurrentProjectFilename, $"{afterName}.opj");
                            Project.Current.Manifests.Add(newManifest);
                            ReplaceManifestNodes(newManifest);
                        }
                    }
                    break;
                case ProjectChanged projectChanged:
                    if (undo)
                    {
                        Project.Current.Name = projectChanged.BeforeName;
                        Project.Current.TargetDirectory = projectChanged.BeforeTargetDirectory;
                    }
                    else
                    {
                        Project.Current.Name = projectChanged.AfterName;
                        Project.Current.TargetDirectory = projectChanged.AfterTargetDirectory;
                    }
                    break;
                case HistoryItemBatch batch:
                    // when doing mass actions like flag/unflag all
                    foreach (HistoryItem childItem in batch.History)
                    {
                        MoveToHistoryItem(childItem, undo);
                    }
                    break;
                case PatchCategoryChanged patchCategoryChanged:
                    // manifest changed
                    if (patchCategoryChanged is PatchManifestChanged patchManifestChanged)
                    {
                        Manifest newManifest, oldManifest;
                        if (undo)
                        {
                            newManifest = patchManifestChanged.AfterManifest;
                            oldManifest = patchManifestChanged.BeforeManifest;
                        }
                        else
                        {
                            newManifest = patchManifestChanged.BeforeManifest;
                            oldManifest = patchManifestChanged.AfterManifest;
                        }

                        if (patchManifestChanged.Before is Hook hook)
                        {
                            Patch oldHook = newManifest.GetMatchingPatch(hook);

                            newManifest.Hooks.RemoveAt(index);
                            oldManifest.Hooks.Add(hook);

                            if (oldHook != null)
                            {
                                RemovePatch(oldHook, false);
                            }
                            AddPatch(hook, false, oldManifest.Name);
                        }
                        else if (patchManifestChanged.Before is Modifier modifier)
                        {
                            Patch oldModifier = newManifest.GetMatchingPatch(modifier);

                            newManifest.Modifiers.RemoveAt(index);
                            oldManifest.Modifiers.Add(modifier);

                            if (oldModifier != null)
                            {
                                RemovePatch(oldModifier, false);
                            }
                            AddModifier(modifier, false, oldManifest.Name);
                        }
                        else if (patchManifestChanged.Before is Field field)
                        {
                            Patch oldField = newManifest.GetMatchingPatch(field);

                            newManifest.Fields.RemoveAt(index);
                            oldManifest.Fields.Add(field);

                            if (oldField != null)
                            {
                                RemovePatch(oldField, false);
                            }
                            AddField(field, false, oldManifest.Name);
                        }
                    }

                    manifest = GetPatchManifest(patchCategoryChanged.Before, patchCategoryChanged?.Manifest?.Name);

                    if (manifest == null)
                    {
                        MessageBox.Show($"Could not find manifest for patch {patchCategoryChanged.Before.Name}", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    RemovePatch(manifest.GetMatchingPatch(patchCategoryChanged.Before), false);

                    // category changed
                    switch (patchCategoryChanged.Before)
                    {
                        case Hook hook1:
                            hook1.HookCategory = undo ? patchCategoryChanged.BeforeCategory : patchCategoryChanged.AfterCategory;
                            break;
                        case Modifier modifier:
                            //modifier.ModifierCategory = back ? patchCategoryChanged.BeforeCategory : patchCategoryChanged.AfterCategory;
                            break;
                        case Field field:
                            //field.FieldCategory = back ? patchCategoryChanged.BeforeCategory : patchCategoryChanged.AfterCategory;
                            break;
                    }

                    AddPatch(patchCategoryChanged.Before, false, manifest.Name);

                    break;
                case HookChanged hookChanged:
                    MovePatchHistory(hookChanged.Before, hookChanged.After, hookChanged.Manifest, undo);
                    break;
                case ModifierChanged modifierChanged:
                    MovePatchHistory(modifierChanged.Before, modifierChanged.After, modifierChanged.Manifest, undo);
                    break;
                case FieldChanged fieldChanged:
                    MovePatchHistory(fieldChanged.Before, fieldChanged.After, fieldChanged.Manifest, undo);
                    break;
            }
        }

        private void MovePatchHistory(Patch before, Patch after, Manifest manifest = null, bool undo = true)
        {
            if (undo)
            {
                if (before == null && after != null) // created
                {
                    manifest ??= GetPatchManifest(after, null);
                    RemovePatch(manifest.GetMatchingPatch(after), false);
                }
                else if (before != null && after == null) // deleted
                {
                    manifest ??= GetPatchManifest(before, null);
                    if (manifest != null)
                    {
                        AddPatch(before, false, manifest.Name);
                    }
                }
                else if (before != null && after != null) // modified
                {
                    manifest ??= GetPatchManifest(after, null);
                    if (manifest != null)
                    {
                        manifest.ReplacePatch(before);
                        RefreshPatch(before);
                        ReplacePatchNode(before);
                    }
                }
            }
            else
            {
                if (before == null && after != null) // created
                {
                    AddPatch(after, false, manifest?.Name);
                }
                else if (before != null && after == null) // deleted
                {
                    manifest ??= GetPatchManifest(before, null);
                    RemovePatch(manifest.GetMatchingPatch(before), false);
                }
                else if (before != null && after != null) // modified
                {
                    manifest ??= GetPatchManifest(after, null);
                    manifest.ReplacePatch(after);
                    RefreshPatch(after);
                    ReplacePatchNode(after);
                }
            }
        }

        private Manifest GetPatchManifest(Patch patch, bool exactReference = false)
        {
            if (patch is Hook hook)
            {
                return GetHookManifest(hook, exactReference);
            }
            else if (patch is Modifier modifier)
            {
                return GetModifierManifest(modifier, exactReference);
            }
            else if (patch is Field field)
            {
                return GetFieldManifest(field, exactReference);
            }

            return null;
        }

        private Manifest GetHookManifest(Hook hook, bool exactReference = false)
        {
            if (exactReference)
            {
                return Project.Current.Manifests.FirstOrDefault(x => x.Hooks.Any(h => h == hook));
            }
            return Project.Current.Manifests.FirstOrDefault(x => x.Hooks.Any(h => h.Equals(hook)));
        }

        private Manifest GetModifierManifest(Modifier modifier, bool exactReference = false)
        {
            if (exactReference)
            {
                return Project.Current.Manifests.FirstOrDefault(x => x.Modifiers.Any(h => h == modifier));
            }
            return Project.Current.Manifests.FirstOrDefault(x => x.Modifiers.Any(h => h.Equals(modifier)));
        }

        private Manifest GetFieldManifest(Field field, bool exactReference = false)
        {
            if (exactReference)
            {
                return Project.Current.Manifests.FirstOrDefault(x => x.Fields.Any(h => h == field));
            }
            return Project.Current.Manifests.FirstOrDefault(x => x.Fields.Any(h => h.Equals(field)));
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoryIndex++;
            if (HistoryIndex > (History.Count - 1))
            {
                return;
            }

            MoveToHistoryItem(History[HistoryIndex], false);
        }

        private void editToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (History.Count > -1 && HistoryIndex > -1)
            {
                undoToolStripMenuItem.Enabled = true;
            }
            else
            {
                undoToolStripMenuItem.Enabled = false;
            }

            if (HistoryIndex < (History.Count - 1))
            {
                redoToolStripMenuItem.Enabled = true;
            }
            else
            {
                redoToolStripMenuItem.Enabled = false;
            }
        }

        private void cloneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (objectview.SelectedNode?.Tag is Hook patch)
            {
                CloneHook(patch);
                RefreshHook(patch);
            }
        }

        private void removeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (objectview.SelectedNode?.Tag is Patch patch)
            {
                TryRemovePatch(patch);
            }
        }

        private void moveToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (objectview.SelectedNode?.Tag is Patch patch)
            {
                Manifest manifest = GetPatchManifest(patch, true);
                Manifest newManifest = AssemblyManifestPrompt(Project.Current.GetManifestNames().Where(x => x != manifest.Name).ToList());

                TreeNode rootNode = objectview.Nodes[GetPatchTypeRootName(patch)];
                TreeNode node = GetPatchNode(patch);
                TreeNode targetNode = GetManifestNode(newManifest, rootNode.Nodes);
                if (targetNode == null)
                {
                    targetNode = GetOrCreateManifestNode(rootNode, newManifest);
                }

                objectview.Nodes.Remove(node);
                targetNode.Nodes.Add(node);
                MovePatchNode(targetNode, patch, manifest, newManifest);
                objectview.SelectedNode = node;
                node.EnsureVisible();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveProject();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveprojectdialog.ShowDialog() == DialogResult.OK)
            {
                CurrentProjectFilename = Path.GetDirectoryName(saveprojectdialog.FileName);
                Project.Current.Name = Path.GetFileNameWithoutExtension(saveprojectdialog.FileName);
                SaveProject();
            }
        }

        private void filemenu_DropDownOpening(object sender, EventArgs e)
        {
            bool anyapply = false;
            foreach (TabPage tab in tabview.TabPages)
            {
                if (tab.Controls.Count > 0 && tab.Controls[0] is IPatchView patchView && patchView.IsChanged)
                {
                    anyapply = true;
                    break;
                }
            }

            bool alreadySaved = HistoryIndex == -1 || History[HistoryIndex].Saved;

            if (!anyapply && alreadySaved)
            {
                saveToolStripMenuItem.Enabled = false;
                saveAsToolStripMenuItem.Enabled = false;
            }
            else
            {
                saveToolStripMenuItem.Enabled = true;
                saveAsToolStripMenuItem.Enabled = true;
            }
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            searchTextBox.Focus();
            searchTextBox.SelectAll();
        }
    }

    public static class Extensions
    {
        /// <summary>
        /// Get the string slice between the two indexes.
        /// Inclusive for start index, exclusive for end index.
        /// </summary>
        public static string Slice(this string source, int start, int end)
        {
            if (end < 0) // Keep this for negative end support
            {
                end = source.Length + end;
            }
            int len = end - start; // Calculate length
            return source.Substring(start, len); // Return Substring of length
        }

        public static T DeepClone<T>(this T obj)
        {
            if (obj == null) return default(T);
            string objStr = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

            Type targetType;
            if (obj is Hook hook)
            {
                targetType = Hook.GetHookType(hook.HookTypeName);
            }
            else
            {
                targetType = typeof(T);
            }
            T result = (T)Activator.CreateInstance(targetType);
            Newtonsoft.Json.JsonConvert.PopulateObject(objStr, result);
            return result;
        }
    }

    public class DragDropHelper
    {
        [DllImport("comctl32.dll")]
        public static extern bool InitCommonControls();

        [DllImport("comctl32.dll", CharSet = CharSet.Auto)]
        public static extern bool ImageList_BeginDrag(IntPtr hWnd, int iTrack, int dxHotspot, int dyHotspot);

        [DllImport("comctl32.dll", CharSet = CharSet.Auto)]
        public static extern bool ImageList_DragMove(int x, int y);

        [DllImport("comctl32.dll", CharSet = CharSet.Auto)]
        public static extern void ImageList_EndDrag();

        [DllImport("comctl32.dll", CharSet = CharSet.Auto)]
        public static extern bool ImageList_DragEnter(IntPtr hWnd, int x, int y);

        [DllImport("comctl32.dll", CharSet = CharSet.Auto)]
        public static extern bool ImageList_DragLeave(IntPtr hWnd);

        [DllImport("comctl32.dll", CharSet = CharSet.Auto)]
        public static extern bool ImageList_DragShowNolock(bool fShow);

        static DragDropHelper()
        {
            InitCommonControls();
        }
    }

    public static class AutoScroll
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        internal static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        public static void Scroll(this Control control)
        {
            Point pt = control.PointToClient(Cursor.Position);
            if (pt.Y + 20 > control.Height)
            {
                DragDropHelper.ImageList_DragShowNolock(false);
                SendMessage(control.Handle, 277, (IntPtr)1, (IntPtr)0);
                DragDropHelper.ImageList_DragShowNolock(true);
            }
            else if (pt.Y < 20)
            {
                DragDropHelper.ImageList_DragShowNolock(false);
                SendMessage(control.Handle, 277, (IntPtr)0, (IntPtr)0);
                DragDropHelper.ImageList_DragShowNolock(true);
            }
        }
    }
}
