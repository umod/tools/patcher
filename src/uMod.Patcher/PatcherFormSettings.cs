﻿using Newtonsoft.Json;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace uMod.Patcher
{
    /// <summary>
    /// A set of persistent window settings
    /// </summary>
    public class PatcherFormSettings
    {
        /// <summary>
        /// Gets or sets the form position
        /// </summary>
        public Point FormPosition { get; set; }

        /// <summary>
        /// Gets or sets the form size
        /// </summary>
        public Size FormSize { get; set; }

        /// <summary>
        /// Gets or sets the window state
        /// </summary>
        public FormWindowState WindowState { get; set; }

        /// <summary>
        /// Gets or sets the last directory used to open or save a project
        /// </summary>
        public string LastProjectDirectory { get; set; }

        /// <summary>
        /// Gets or sets the last target directory used
        /// </summary>
        public string LastTargetDirectory { get; set; }

        // The settings filename
        private const string Filename = "formsettings.json";

        /// <summary>
        /// Initializes a new instance of the PatcherFormSettings class with sensible defaults
        /// </summary>
        public PatcherFormSettings()
        {
            // Fill in defaults
            Rectangle workingArea = Screen.GetWorkingArea(new Point(0, 0));
            FormPosition = new Point(workingArea.Left + workingArea.Width / 5, workingArea.Top + workingArea.Height / 5);
            FormSize = new Size(workingArea.Width * 3 / 5, workingArea.Height * 3 / 5);
            WindowState = FormWindowState.Normal;
            LastProjectDirectory = "";
            LastTargetDirectory = "";
        }

        /// <summary>
        /// Saves these settings
        /// </summary>
        public void Save()
        {
            File.WriteAllText(Filename, JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
        }

        /// <summary>
        /// Loads the settings
        /// </summary>
        /// <returns></returns>
        public static PatcherFormSettings Load()
        {
            if (!File.Exists(Filename))
            {
                return new PatcherFormSettings();
            }

            string text = File.ReadAllText(Filename);
            return JsonConvert.DeserializeObject<PatcherFormSettings>(text);
        }
    }
}
